package com.akronline;

import static com.akronline.ProdList.filterData;
import static com.akronline.ProdList.productList;
import static com.akronline.ProdList.tempList;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.dialogs.EditProfile;
import com.akronline.dialogs.HelpUs;
import com.akronline.dialogs.OrderSample;
import com.akronline.dialogs.RepMeeting;
import com.akronline.dialogs.ReqWebinar;
import com.akronline.dialogs.RequestCatalog;
import com.akronline.dialogs.StateChooser;
import com.akronline.interfaces.AdsCallback;
import com.akronline.interfaces.DialogCallback;
import com.akronline.mob.HomeMob;
import com.commondata.CommonData;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.dataholder.DataHolderClass;
import com.datamodel.Product;
import com.helper.Utils;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class ProductScreen extends FragmentActivity implements OnClickListener,
		AdsCallback, DialogCallback {

	public static boolean fromYT = false;
	public static boolean fromScan = false;

	SlidingMenu drawer;
	SharedPreferences prefs;
	ActionBar actionBar;
	Button filter;
	SliderLayout slider;
	// for POPUPs
	PopupWindow popup;
	TextView pr1, pr2, pr3, pr4;
	float min, mid1, mid2, mid3, max;
	LinearLayout catItems;
	LinearLayout subItems;
	ImageView imgCatArrow;
	ImageView imgSubArrow;
	TextView txtCurrntCat;
	TextView txtCurretSub;
	HashSet<String> catList = new HashSet<String>();
	// ScrollView suv_layout;
	// Button catagory_header;
	// ListView lsy;

	String userID;
	String repID;

	// ActionBar
	TextView txtProdCount;
	TextView titleCurrentCategory;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		Log.e("tag", "ProductScreen");
//		if (getString(R.string.screen_type).equals("Tablet")) {
//			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//		} else {
//			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//		}
		prefs = getApplicationContext().getSharedPreferences("LoginPref", 0);
		userID = prefs.getString("USER-ID", "0");
		setContentView(R.layout.prod_screen_new);
		initDrawer();
		initPopup();
		
	}

	@Override
	public void onBackPressed() {
		if (drawer.isMenuShowing()) {
			drawer.toggle(true);
		} else if (popup.isShowing()) {
			popup.dismiss();
		} else
			super.onBackPressed();
	}

	private void initDrawer() {
		// Set up sliding drawer and Custom ActionBar
		Button moodProd, newProd, bestProd, snacks, os, rc, ep, hshu, rrm, rw, logout;
		drawer = new SlidingMenu(this);
		drawer.setMode(SlidingMenu.LEFT);
		drawer.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		drawer.setShadowDrawable(R.drawable.shadow);
		drawer.setShadowWidthRes(R.dimen.shadowWidth);
		drawer.setBehindWidthRes(R.dimen.drawerWidth);
		drawer.setFadeDegree(0.35f);
		drawer.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		drawer.setMenu(R.layout.drawer_layout);

		// setup ActionBar
		Button openDrawer, shoot, search;
		actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.custom_actionbar);
		openDrawer = (Button) actionBar.getCustomView().findViewById(
				R.id.drawer_btn);
		shoot = (Button) actionBar.getCustomView().findViewById(R.id.shoot);
		search = (Button) actionBar.getCustomView().findViewById(R.id.search);
		filter = (Button) actionBar.getCustomView().findViewById(R.id.filter);
		titleCurrentCategory = (TextView) actionBar.getCustomView()
				.findViewById(R.id.catagory_name);
		txtProdCount = (TextView) actionBar.getCustomView().findViewById(
				R.id.catagory_count);
		openDrawer.setOnClickListener(this);
		search.setOnClickListener(this);
		shoot.setOnClickListener(this);
		filter.setOnClickListener(this);
		slider = (SliderLayout) drawer.findViewById(R.id.slider);
		slider.setDuration(6000);
		if (CommonData.adsList.size() == 0) {
			new GetAds(this, userID).execute();
		} else {
			onAdsLoaded();
		}

		// setup drawer buttons
		moodProd = (Button) drawer.findViewById(R.id.mood_products);
		newProd = (Button) drawer.findViewById(R.id.new_products);
		bestProd = (Button) drawer.findViewById(R.id.best_sellers);
		snacks = (Button) drawer.findViewById(R.id.snacks);
		os = (Button) drawer.findViewById(R.id.os);
		rc = (Button) drawer.findViewById(R.id.rc);
		ep = (Button) drawer.findViewById(R.id.ep);
		hshu = (Button) drawer.findViewById(R.id.hshu);
		rrm = (Button) drawer.findViewById(R.id.rrm);
		rw = (Button) drawer.findViewById(R.id.rw);
		logout = (Button) drawer.findViewById(R.id.logout);

		moodProd.setOnClickListener(this);
		newProd.setOnClickListener(this);
		bestProd.setOnClickListener(this);
		snacks.setOnClickListener(this);
		os.setOnClickListener(this);
		rc.setOnClickListener(this);
		ep.setOnClickListener(this);
		hshu.setOnClickListener(this);
		rrm.setOnClickListener(this);
		rw.setOnClickListener(this);
		logout.setOnClickListener(this);
	}

	@SuppressLint("InflateParams")
	private void initPopup() {
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.data_window, null);
		// popup = new PopupWindow(layout, 310, LayoutParams.WRAP_CONTENT,
		// true);
		popup = new PopupWindow(this);
		popup.setContentView(layout);
		popup.setTouchable(true);
		popup.setFocusable(true);
		popup.setOutsideTouchable(true);
		
		int popupWidth = (int) ((int)Utils.getDeviceWidth(getWindowManager())/3.2);
		popup.setWidth(popupWidth);
		// TODO POPUP height
		popup.setHeight(LayoutParams.WRAP_CONTENT);
		TextView txtAsc, txtDesc;
		LinearLayout catLayout, subLayout;
		catLayout = (LinearLayout) layout.findViewById(R.id.catLayout);
		subLayout = (LinearLayout) layout.findViewById(R.id.subLayout);
		catItems = (LinearLayout) layout.findViewById(R.id.catItems);
		subItems = (LinearLayout) layout.findViewById(R.id.subItems);
		imgCatArrow = (ImageView) layout.findViewById(R.id.imgCatArrow);
		imgSubArrow = (ImageView) layout.findViewById(R.id.imgSubArrow);
		txtCurrntCat = (TextView) layout
				.findViewById(R.id.set_current_catagory);
		txtCurretSub = (TextView) layout.findViewById(R.id.txtCurrentSub);
		txtAsc = (TextView) layout.findViewById(R.id.txtAscOrder);
		txtDesc = (TextView) layout.findViewById(R.id.txtDescOrder);
		pr1 = (TextView) layout.findViewById(R.id.priceRange1);
		pr2 = (TextView) layout.findViewById(R.id.priceRange2);
		pr3 = (TextView) layout.findViewById(R.id.priceRange3);
		pr4 = (TextView) layout.findViewById(R.id.priceRange4);

		catLayout.setOnClickListener(this);
		subLayout.setOnClickListener(this);
		txtCurrntCat.setOnClickListener(this);
		txtCurretSub.setOnClickListener(this);
		imgCatArrow.setOnClickListener(this);
		imgSubArrow.setOnClickListener(this);
		pr1.setOnClickListener(this);
		pr2.setOnClickListener(this);
		pr3.setOnClickListener(this);
		pr4.setOnClickListener(this);
		txtAsc.setOnClickListener(this);
		txtDesc.setOnClickListener(this);
		txtCurrntCat.setText(DataHolderClass.getInstance().get_titleCatagory());
	}

	private OnClickListener onCategoryClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String cat = v.getTag().toString();
			if (cat.equals("ALL")) {
				tempList = new ArrayList<Product>(productList);
			} else {
				tempList = new ArrayList<Product>();
				for (Product product : productList) {
					if (product.getCatagory().equals(cat))
						tempList.add(product);
				}
			}
			txtCurrntCat.setText(cat);
			filterData(ProductScreen.this, tempList);
			setProductCount();
			popup.dismiss();
			
		}
	};

	private OnClickListener onSubcategotyClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			String subcat = v.getTag().toString();
			if (subcat.equals("ALL")) {
				tempList = new ArrayList<Product>(productList);
			} else {
				tempList = new ArrayList<Product>();
				for (Product product : productList) {
					if (product.getSubCatagory().equals(subcat))
						tempList.add(product);
				}
			}
			txtCurretSub.setText(subcat);
			filterData(ProductScreen.this, tempList);
			setProductCount();
			popup.dismiss();
		}
	};

	@Override
	public void onClick(View v) {
		try {
			repID = prefs.getString("REP-ID", "0");
			switch (v.getId()) {
			case R.id.catLayout:
			case R.id.set_current_catagory:
			case R.id.imgCatArrow:
				if (catList.size() <= 1) {
					catItems.setVisibility(View.GONE);
					return;
				}
				if (catItems.getVisibility() == View.VISIBLE) {
					catItems.setVisibility(View.GONE);
					imgCatArrow.setImageResource(R.drawable.right_black);
				} else {
					catItems.setVisibility(View.VISIBLE);
					imgCatArrow.setImageResource(R.drawable.down_black);
				}
				break;
			case R.id.subLayout:
			case R.id.txtCurrentSub:
			case R.id.imgSubArrow:
				if (subItems.getVisibility() == View.VISIBLE) {
					subItems.setVisibility(View.GONE);
					imgSubArrow.setImageResource(R.drawable.right_black);
				} else {
					subItems.setVisibility(View.VISIBLE);
					imgSubArrow.setImageResource(R.drawable.down_black);
				}
				break;
			case R.id.priceRange1:
				ProdList.setRange(this, min, mid1);
				setProductCount();
				popup.dismiss();
				break;
			case R.id.priceRange2:
				ProdList.setRange(this, mid1, mid2);
				setProductCount();
				popup.dismiss();
				break;
			case R.id.priceRange3:
				ProdList.setRange(this, mid2, mid3);
				setProductCount();
				popup.dismiss();
				break;
			case R.id.priceRange4:
				ProdList.setRange(this, mid3, max);
				setProductCount();
				popup.dismiss();
				break;
			case R.id.txtAscOrder:
				ProdList.sortAsc(this);
				setProductCount();
				popup.dismiss();
				break;
			case R.id.txtDescOrder:
				ProdList.sortDesc(this);
				setProductCount();
				popup.dismiss();
				break;
			case R.id.drawer_btn:
				drawer.toggle(true);
				break;
			case R.id.shoot:
				startActivity(new Intent(this, SyncTab.class));
				break;
			case R.id.search:
				ProdList._checkView();
				break;
			case R.id.filter:
				if (popup.isShowing()) {
					popup.dismiss();
				} else {
					popup.showAtLocation(filter, Gravity.START | Gravity.TOP,
							15, 70);
				}
				break;
			case R.id.logout:
				prefs = getApplicationContext().getSharedPreferences(
						"LoginPref", 0);
				Editor editor = prefs.edit();
				editor.clear();
				editor.commit();
				
				
				Intent i;
				if (getString(R.string.screen_type).equals("Tablet")) {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
					i = new Intent(this, HomeTab.class);
				} else {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
					i = new Intent(this, HomeMob.class);
				}
				
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(i);
				overridePendingTransition(R.anim.enter_new_screen,
						R.anim.exit_old_screen);
				
				/*Intent logout = new Intent(this, HomeTab.class);
				logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(logout);
				overridePendingTransition(R.anim.enter_new_screen,
						R.anim.exit_old_screen);*/
				break;
			case R.id.mood_products:
				if (DataHolderClass.getInstance().get_titleCatagory()
						.equalsIgnoreCase("MOOD PRODUCTS")) {
					Toast.makeText(getApplicationContext(),
							"You are on same page", Toast.LENGTH_LONG).show();
				} else {
					DataHolderClass.getInstance().set_titleCatagory(
							"MOOD PRODUCTS");
					DataHolderClass.getInstance().set_catagoryName(
							"moodproducts");
					switchCategory();
				}
				break;
			case R.id.new_products:
				if (DataHolderClass.getInstance().get_titleCatagory()
						.equalsIgnoreCase("NEW PRODUCTS")) {
					Toast.makeText(getApplicationContext(),
							"You are on same page", Toast.LENGTH_LONG).show();
				} else {
					DataHolderClass.getInstance().set_titleCatagory(
							"NEW PRODUCTS");
					DataHolderClass.getInstance().set_catagoryName(
							"newproducts");
					switchCategory();
				}
				break;
			case R.id.best_sellers:
				if (DataHolderClass.getInstance().get_titleCatagory()
						.equalsIgnoreCase("BEST SELLERS")) {
					Toast.makeText(getApplicationContext(),
							"You are on same page", Toast.LENGTH_LONG).show();
				} else {
					DataHolderClass.getInstance().set_titleCatagory(
							"BEST SELLERS");
					DataHolderClass.getInstance().set_catagoryName(
							"bestsellers");
					switchCategory();
				}
				break;
			case R.id.snacks:
				if (DataHolderClass.getInstance().get_titleCatagory()
						.equalsIgnoreCase("SNACKS")) {
					Toast.makeText(getApplicationContext(),
							"You are on same page", Toast.LENGTH_LONG).show();
				} else {
					DataHolderClass.getInstance().set_titleCatagory("SNACKS");
					DataHolderClass.getInstance().set_catagoryName("snacks");
					switchCategory();
				}
				break;
			case R.id.ep:
				drawer.toggle(true);
				new EditProfile(this, userID).show();
				break;
			case R.id.rc:
				drawer.toggle(true);
				new RequestCatalog(this).show();
				break;
			case R.id.os:
				drawer.toggle(true);
				new OrderSample(this, userID).show();
				break;
			case R.id.rw:
				drawer.toggle(true);
				if (repID.equals("0")) {
					StateChooser stateDialoge = new StateChooser(this, userID, 2);
					stateDialoge.setOnDialogSubmitListner(this);
					stateDialoge.show();
					//new StateChooser(this, userID, 2).show();
				} else
					new ReqWebinar(this).show();
				break;
			case R.id.hshu:
				drawer.toggle(true);
				new HelpUs(this, userID).show();
				break;
			case R.id.rrm:
				drawer.toggle(true);
				if (repID.equals("0")) {
					StateChooser stateDialoge = new StateChooser(this, userID, 1);
					stateDialoge.setOnDialogSubmitListner(this);
					stateDialoge.show();
					//new StateChooser(this, userID, 1).show();
				} else
					new RepMeeting(this, userID, repID).show();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG)
					.show();
		}
	}

	public void setFilterData() {
		float[] arr = ProdList.getMinMax();
		final DecimalFormat df = new DecimalFormat();
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(3);
		final LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 1);
		final LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		min = arr[0];
		max = arr[1];
		if (min == max || max == 0) {
			// TODO hide ranges
		} else {
			mid2 = (min + max) / 2;
			mid1 = (min + mid2) / 2;
			mid3 = (mid2 + max) / 2;
			pr1.setText("$ " + df.format(min) + " - $ " + df.format(mid1));
			pr2.setText("$ " + df.format(mid1) + " - $ " + df.format(mid2));
			pr3.setText("$ " + df.format(mid2) + " - $ " + df.format(mid3));
			pr4.setText("$ " + df.format(mid3) + " - $ " + df.format(max));
		}
		catItems.removeAllViews();
		subItems.removeAllViews();
		int i = 0;
		catList.clear();
//		HashSet<String> subList = new HashSet<String>();
		ArrayList<String> subList = new ArrayList<String> ();
		for (Product product : ProdList.productList) {
			if(!subList.contains(product.getSubCatagory())){
				subList.add(product.getSubCatagory());	
			}
			catList.add(product.getCatagory());
		}
		Log.e("cats", "Total cats:" + catList.size());
		Log.e("subs", "Total subs:" + subList.size());
		if (catList.size() > 1) {
			TextView txtCatAll = new TextView(this);
			View line = new View(this);
			txtCatAll.setText("ALL (" + ProdList.productList.size() + ")");
			txtCatAll.setTag("ALL");
			txtCatAll.setLayoutParams(txtParams);
			txtCatAll.setPadding(10, 10, 0, 10);
			txtCatAll.setTextSize(18f);
			txtCatAll.setOnClickListener(onCategoryClick);
			line.setLayoutParams(lineParams);
			line.setBackgroundColor(Color.GRAY);
			catItems.addView(line, i++);
			catItems.addView(txtCatAll, i++);
			txtCurrntCat.setText("ALL");
			imgCatArrow.setVisibility(View.VISIBLE);
			for (String cat : catList) {
				View v = new View(this);
				v.setLayoutParams(lineParams);
				v.setBackgroundColor(Color.GRAY);
				v.setLayoutParams(lineParams);
				v.setBackgroundColor(Color.GRAY);
				int count = 0;
				for (Product p : ProdList.productList)
					if (p.getCatagory().equals(cat))
						count++;
				TextView tv = new TextView(this);
				tv.setText(cat + " (" + count + ")");
				tv.setTag(cat);
				tv.setLayoutParams(txtParams);
				tv.setPadding(10, 10, 0, 10);
				tv.setTextSize(18f);
				tv.setTag(cat);
				tv.setOnClickListener(onCategoryClick);
				catItems.addView(v, i++);
				catItems.addView(tv, i++);
			}
		} else {
			imgCatArrow.setVisibility(View.GONE);
			catItems.setVisibility(View.GONE);
			txtCurrntCat.setText(DataHolderClass.getInstance()
					.get_titleCatagory());
		}
		i = 0;
		if (subList.size() == 1) {
			txtCurretSub.setText("ALL");
			imgSubArrow.setVisibility(View.GONE);
		} else {
			imgSubArrow.setVisibility(View.VISIBLE);
			TextView txtSubAll = new TextView(this);
			txtSubAll.setText("ALL (" + ProdList.productList.size() + ")");
			txtSubAll.setTag("ALL");
			txtSubAll.setLayoutParams(txtParams);
			txtSubAll.setPadding(10, 10, 0, 10);
			txtSubAll.setTextSize(18f);
			txtSubAll.setOnClickListener(onSubcategotyClick);
			View line = new View(this);
			line.setLayoutParams(lineParams);
			line.setBackgroundColor(Color.GRAY);
			subItems.addView(line, i++);
			subItems.addView(txtSubAll, i++);
			
			java.util.Collections.sort(subList);
			for (String subcat : subList) {
				View v = new View(this);
				v.setLayoutParams(lineParams);
				v.setBackgroundColor(Color.GRAY);
				v.setLayoutParams(lineParams);
				v.setBackgroundColor(Color.GRAY);
				int count = 0;
				for (Product p : ProdList.productList)
					if (p.getSubCatagory().equals(subcat))
						count++;
				TextView tv = new TextView(this);
				tv.setText(subcat + " (" + count + ")");
				tv.setTag(subcat);
				tv.setLayoutParams(txtParams);
				tv.setPadding(10, 10, 0, 10);
				tv.setTextSize(18f);
				tv.setTag(subcat);
				tv.setOnClickListener(onSubcategotyClick);
				subItems.addView(v, i++);
				subItems.addView(tv, i++);
			}
		}

		setProductCount();
	}

	public void setProductCount() {
		if (tempList.size() > 0) {
			titleCurrentCategory.setText(tempList.get(0).getCatagory());
			txtProdCount.setText(tempList.size() + " Products");
		}
	}

	private void switchCategory() {
		drawer.toggle(true);
		Intent i = new Intent(this, ProductScreen.class);
		startActivity(i);
		overridePendingTransition(android.R.anim.fade_in,
				android.R.anim.fade_out);
		finish();
	}

	public void doTransaction(String tag, Fragment fragment) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		Fragment prevFrag = fragmentManager.findFragmentByTag(tag);
		if (prevFrag != null) {
			fragmentTransaction.remove(prevFrag);
		}
		fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, 0, 0,
				android.R.anim.fade_out);
		fragmentTransaction.replace(R.id.detail_fragment_container, fragment,
				tag);
		fragmentTransaction.commit();
	}

	@Override
	public void onAdsLoaded() {
		for (String ad : CommonData.adsList) {
			DefaultSliderView sv = new DefaultSliderView(this);
			sv.image(ad);
			slider.addSlider(sv);
		}
	}

	@Override
	public void onDialogSubmit(int requestCode, String result) {
		if(requestCode == 1){
			new RepMeeting(this, userID, repID).show();
		}else {
			new ReqWebinar(this).show();
		}		
	}

}
