package com.akronline.mob;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Spinner;
import android.widget.TextView;

import com.akronline.R;
import com.progressbar.GoogleProgressDialog;

public class MobileFreight extends Fragment {

	TextView note;
	Spinner spinCountry,spinState;
	private WebView web_view;
	public View txtNolink;
	public View progress;
	private static final String NAMESPACE = "http://tempuri.org/";
	private static final String METHOD_NAME = "SetSampleRequest";
	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
	private static String FRIEGHT_ESTIMATOR_METHOD = "GetFreightEstimator";
	private static String FRIEGHT_ESTIMATOR_SOAP_ACTION = "http://tempuri.org/GetFreightEstimator";

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.freight_estimator, container, false);
		Log.e("tag", "MobileFreight");
		web_view = (WebView) v.findViewById(R.id.web_view);
		txtNolink = (TextView)v.findViewById(R.id.noLink);
		progress = v.findViewById(R.id.progressBar);
		
		new Estimator().execute();
		return v;
	}
	private class Estimator extends AsyncTask<String, String, String> {
		GoogleProgressDialog dialog;
		
		

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				SoapObject request = new SoapObject(NAMESPACE,
						FRIEGHT_ESTIMATOR_METHOD);
				request.addProperty("ProductCode", getArguments().get("PRODUCT_CODE"));
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				try {
					HttpTransportSE androidHttpTransport = new HttpTransportSE(
							URL);
					androidHttpTransport.call(FRIEGHT_ESTIMATOR_SOAP_ACTION,
							envelope);
					SoapObject result = (SoapObject) envelope.bodyIn;
					if (result != null) {
						return result.getProperty(0).toString();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject _obj = new JSONObject(result);
				if (_obj != null) {
					JSONArray _jarray = _obj.optJSONArray("Freight");
					if (_jarray != null & _jarray.length() > 0) {
						String _link = _jarray.optJSONObject(0).optString(
								"Link");
						if (_link != null && _link.length() > 0) {
							web_view.setVisibility(VISIBLE);
							WebSettings webSettings = web_view.getSettings();
							webSettings.setJavaScriptEnabled(true);
							System.out.println(_link);
							web_view.loadUrl(_link);
						} else {
							web_view.setVisibility(View.INVISIBLE);
							txtNolink.setVisibility(VISIBLE);
							progress.setVisibility(GONE);
						}
					}
				}
			} catch (Exception e) {
				web_view.setVisibility(View.INVISIBLE);
				txtNolink.setVisibility(VISIBLE);
				progress.setVisibility(GONE);
				e.printStackTrace();
			}
			dialog.dismiss();
		}
	}
}
