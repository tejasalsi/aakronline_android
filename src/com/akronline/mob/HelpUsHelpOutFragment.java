package com.akronline.mob;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.R;
import com.akronline.dialogs.MarketChooser;
import com.akronline.dialogs.SocialChooser;
import com.akronline.interfaces.DialogCallback;
import com.progressbar.GoogleProgressDialog;

public class HelpUsHelpOutFragment extends BaseFragment implements OnClickListener, DialogCallback, OnItemSelectedListener{
	

	private static final String NAMESPACE = "http://tempuri.org/";
	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	Context ctx;
	String userID;
//	TextView abtMe, howIsell, myCust, whrIwrk;
	Button btnCancel, btnSubmit;
	Button btn_personel, btn_more_about_me;
	View viewAboutMe, viewWhereIWork, viewHowISell, viewMyCust;
	private static int tabPos = 0;
	// About Me
	EditText etPhyAddr, etState, etZip, etOfcPhn, etCell;
	TextView txtBday, txtSocial;
	Spinner spinPhone, spinEmail, spinText;
	Spinner spinAnnSalesPers, spinPP, spinCS, spinFS, spinDI, spinSNC, spinDSR;
	// How I sell
	private static final String[] sellingTools = { "Choose One", "Catalog",
			"Supplier Website", "App" };
	private static final String[] often = { "Select", "Daily",
			"1-3 times/ week", "1-3 times/ month", "1-3 times/ year", "Never" };
	private static final String[] PSAs = { "Select", "Monthly", "Quarterly",
			"Semi Annually", "Annually", "Never" };
	private static final String[] directMail = { "Select", "Always",
			"Sometimes", "Never" };
	
	private static final String [] catText = {"About Me", "How I Sell","My Customers", "Where I Work"};
	Spinner spinCat;
	Spinner spinInPerson, spinEmail2, spinDirect, spinPhone2;
	Spinner spinTools, spinSampAdv, spinPSAs, spinDirectMail;
	RadioButton rbKitsY, rbKitsN, rbWearY, rbWearN;
	TextView txtSocailBusiness;
	// My customers
	RadioButton rbCustArtY, rbCustArtN;
	EditText etMarketNotSelling;
	TextView txtMarketSelling;
	String resultOfMyCustomerDialog, resultOfSocialSiteDialog;
	// WhereIWork
	Spinner spinAnnSales, spinBuyDeci;
	EditText etOfcLoc, etNumReps, etVRM, etEmrCon;
	private static final String[] ann_sales = { "Select", "Less than $500,000",
			"$500,001 - $1,000,000", "$1,000,001 - $5,000,000",
			"$5,000,001 - $10,000,000", "Over $10,000,000" };
	private static final String[] buying_deci = { "Select", "Project",
			"Product", "Budget" };
	
	SharedPreferences prefs;
	boolean isLogin = false;
	String emaiID;
	String repID;
	
	int personalVisibility = 0;
	LinearLayout layoutPesonal, layoutMoreAboutMe;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View _view = inflater.inflate(R.layout.help_us_help_out_fragment, null);
		
		Log.e("tag", "HelpUsHelpOutFragment Mobile");
		
		prefs = getActivity().getSharedPreferences("LoginPref", 0);
		userID = prefs.getString("USER-ID", "");
		emaiID = prefs.getString("USER-EMAIL", "");
		repID = prefs.getString("REP-ID", "0");
		
		spinCat = (Spinner) _view.findViewById(R.id.spinCategory);
//		abtMe = (TextView) _view.findViewById(R.id.txtAbtMe);
//		howIsell = (TextView) _view.findViewById(R.id.txtHowIsell);
//		myCust = (TextView) _view.findViewById(R.id.txtMyCust);
//		whrIwrk = (TextView) _view.findViewById(R.id.txtWhrIwrk);
		btnCancel = (Button) _view.findViewById(R.id.btnCancel);
		btnSubmit = (Button) _view.findViewById(R.id.btnSubmit);
		viewAboutMe = _view.findViewById(R.id.viewAboutMe);
		viewWhereIWork = _view.findViewById(R.id.viewWhereIWork);
		viewHowISell = _view.findViewById(R.id.viewHowISell);
		viewMyCust = _view.findViewById(R.id.viewMyCustomers);
		// About Me
		etPhyAddr = (EditText) _view.findViewById(R.id.etPhyAdd);
		etState = (EditText) _view.findViewById(R.id.etState);
		etZip = (EditText) _view.findViewById(R.id.etZip);
		etOfcPhn = (EditText) _view.findViewById(R.id.etPhnOfc);
		etCell = (EditText) _view.findViewById(R.id.etCell);
		txtBday = (TextView) _view.findViewById(R.id.txtBday);
		spinPhone = (Spinner) _view.findViewById(R.id.spinPhone);
		spinEmail = (Spinner) _view.findViewById(R.id.spinEmail);
		spinText = (Spinner) _view.findViewById(R.id.spinText);
		txtSocial = (TextView) _view.findViewById(R.id.txtSocialPers);
		spinAnnSalesPers = (Spinner) _view.findViewById(R.id.spinAnnSalesPers);
		spinPP = (Spinner) _view.findViewById(R.id.spinPP);
		spinCS = (Spinner) _view.findViewById(R.id.spinCS);
		spinFS = (Spinner) _view.findViewById(R.id.spinFS);
		spinDI = (Spinner) _view.findViewById(R.id.spinDI);
		spinSNC = (Spinner) _view.findViewById(R.id.spinSNC);
		spinDSR = (Spinner) _view.findViewById(R.id.spinDSR);

		spinAnnSalesPers.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, ann_sales));
		// How I sell
		spinInPerson = (Spinner) _view.findViewById(R.id.spinInPerson);
		spinEmail2 = (Spinner) _view.findViewById(R.id.spinEmail2);
		spinDirect = (Spinner) _view.findViewById(R.id.spinDirect);
		spinPhone2 = (Spinner) _view.findViewById(R.id.spinPhone2);
		spinTools = (Spinner) _view.findViewById(R.id.spinSellTools);
		spinSampAdv = (Spinner) _view.findViewById(R.id.spinOften);
		spinPSAs = (Spinner) _view.findViewById(R.id.spinPSAs);
		spinDirectMail = (Spinner) _view.findViewById(R.id.spinDirectMail);
		rbKitsY = (RadioButton) _view.findViewById(R.id.rbKitsY);
		rbKitsN = (RadioButton) _view.findViewById(R.id.rbKitsN);
		rbWearY = (RadioButton) _view.findViewById(R.id.rbWearY);
		rbWearN = (RadioButton) _view.findViewById(R.id.rbWearN);
		txtSocailBusiness = (TextView) _view.findViewById(R.id.txtSocialBusiness);
		
		btn_personel = (Button) _view.findViewById(R.id.btn_personal);
		btn_more_about_me = (Button) _view.findViewById(R.id.btn_about_me);
		layoutPesonal = (LinearLayout) _view.findViewById(R.id.abtMeTop);
		layoutMoreAboutMe = (LinearLayout) _view.findViewById(R.id.layout_aboutme);

		spinCat.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, catText));
		spinTools.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, sellingTools));
		spinSampAdv.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, often));
		spinPSAs.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, PSAs));
		spinDirectMail.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, directMail));

		// My customers
		txtMarketSelling = (TextView) _view.findViewById(R.id.txtMarkets);
		etMarketNotSelling = (EditText) _view.findViewById(R.id.etWhatMarkets);
		rbCustArtY = (RadioButton) _view.findViewById(R.id.rbCustArtY);
		rbCustArtN = (RadioButton) _view.findViewById(R.id.rbCustArtN);
		// where I work
		spinAnnSales = (Spinner) _view.findViewById(R.id.spinAnnSales);
		spinBuyDeci = (Spinner) _view.findViewById(R.id.spinBuyDeci);
		etOfcLoc = (EditText) _view.findViewById(R.id.etOfcLoc);
		etNumReps = (EditText) _view.findViewById(R.id.etNumReps);
		etVRM = (EditText) _view.findViewById(R.id.etVRM);
		etEmrCon = (EditText) _view.findViewById(R.id.etEmerCon);

		spinAnnSales.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, ann_sales));
		spinBuyDeci.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, buying_deci));

		btnCancel.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
//		abtMe.setOnClickListener(this);
//		howIsell.setOnClickListener(this);
//		myCust.setOnClickListener(this);
//		whrIwrk.setOnClickListener(this);
		txtSocial.setOnClickListener(this);
		txtSocailBusiness.setOnClickListener(this);
		txtMarketSelling.setOnClickListener(this);
		txtBday.setOnClickListener(this);
		
		spinCat.setOnItemSelectedListener(this);
		
		btn_personel.setOnClickListener(this);
		btn_more_about_me.setOnClickListener(this);
		
		switchTab(0);
		setVisibilityOfLayouts(0);
		
		return _view;
	}
	
	private void showToast(String msg) {
		Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_LONG)
				.show();
	}
	
	public void setVisibilityOfLayouts(int i){
		if(i == 0){
			layoutPesonal.setVisibility(View.GONE);
			layoutMoreAboutMe.setVisibility(View.GONE);
		}else if(i == 1){
			if(layoutPesonal.getVisibility() == View.VISIBLE){
				layoutPesonal.setVisibility(View.GONE);
				btn_personel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_black, 0);
				btn_more_about_me.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_black, 0);
			}else{
				layoutPesonal.setVisibility(View.VISIBLE);
				btn_personel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_black, 0);
				btn_more_about_me.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_black, 0);
			}
			
			layoutMoreAboutMe.setVisibility(View.GONE);
			
		}else if(i == 2){
			if(layoutMoreAboutMe.getVisibility() == View.VISIBLE){
				layoutMoreAboutMe.setVisibility(View.GONE);
				btn_personel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_black, 0);
				btn_more_about_me.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_black, 0);
			}else{
				layoutMoreAboutMe.setVisibility(View.VISIBLE);
				btn_personel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_black, 0);
				btn_more_about_me.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_black, 0);
			}
			layoutPesonal.setVisibility(View.GONE);
		}
		personalVisibility = i;
	}
	
	
	

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
//			String title = DataHolderClass.getInstance().get_titleCatagory();
			((HomeMob) getActivity()).setTitle("Help Us Help You");
		} catch (Exception e) {
			e.printStackTrace();
		}
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.search.setVisibility(View.GONE);
	}

	private void switchTab(int position) {
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		tabPos = position;
		switch (position) {
		case 0: // About me
//			abtMe.setBackgroundResource(R.drawable.buttonhover);
//			howIsell.setBackgroundResource(R.drawable.black);
//			myCust.setBackgroundResource(R.drawable.black);
//			whrIwrk.setBackgroundResource(R.drawable.black);
			viewAboutMe.setVisibility(VISIBLE);
			viewHowISell.setVisibility(GONE);
			viewMyCust.setVisibility(GONE);
			viewWhereIWork.setVisibility(GONE);
			break;
		case 1: // How i sell
//			abtMe.setBackgroundResource(R.drawable.black);
//			howIsell.setBackgroundResource(R.drawable.buttonhover);
//			myCust.setBackgroundResource(R.drawable.black);
//			whrIwrk.setBackgroundResource(R.drawable.black);
			viewAboutMe.setVisibility(GONE);
			viewHowISell.setVisibility(VISIBLE);
			viewMyCust.setVisibility(GONE);
			viewWhereIWork.setVisibility(GONE);
			break;
		case 2: // My Customers
//			abtMe.setBackgroundResource(R.drawable.black);
//			howIsell.setBackgroundResource(R.drawable.black);
//			myCust.setBackgroundResource(R.drawable.buttonhover);
//			whrIwrk.setBackgroundResource(R.drawable.black);
			viewAboutMe.setVisibility(GONE);
			viewHowISell.setVisibility(GONE);
			viewMyCust.setVisibility(VISIBLE);
			viewWhereIWork.setVisibility(GONE);
			break;
		case 3: // Where I work
//			abtMe.setBackgroundResource(R.drawable.black);
//			howIsell.setBackgroundResource(R.drawable.black);
//			myCust.setBackgroundResource(R.drawable.black);
//			whrIwrk.setBackgroundResource(R.drawable.buttonhover);
			viewAboutMe.setVisibility(GONE);
			viewHowISell.setVisibility(GONE);
			viewMyCust.setVisibility(GONE);
			viewWhereIWork.setVisibility(VISIBLE);
			break;
		}
	}

	private static final DecimalFormat df = new DecimalFormat("00");
	private OnDateSetListener dateSet = new OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			txtBday.setText((df.format(++monthOfYear)) + "/"
					+ (df.format(dayOfMonth)) + "/" + year);
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txtAbtMe:
			switchTab(0);
			break;
		case R.id.txtHowIsell:
			switchTab(1);
			break;
		case R.id.txtMyCust:
			switchTab(2);
			break;
		case R.id.txtWhrIwrk:
			switchTab(3);
			break;
		case R.id.btnCancel:
			getActivity().onBackPressed();
			break;
		case R.id.btnSubmit:
			submitForm();
			break;
		case R.id.txtSocialPers:
			new SocialChooser(getActivity(), this, REQ_SOCIAL).show();
			break;
		case R.id.txtSocialBusiness:
			new SocialChooser(getActivity(), this, REQ_SOCIAL_BUSINESS).show();
			break;
		case R.id.txtMarkets:
			new MarketChooser(getActivity(), this, REQ_MARKET).show();
			break;
		case R.id.txtBday:
//			Calendar cal = Calendar.getInstance();
//	//		cal.setTimeInMillis(System.currentTimeMillis());
//			DatePickerDialog dpd = new DatePickerDialog(getActivity(), dateSet,
//					cal.get(Calendar.DAY_OF_WEEK), cal.get(Calendar.MONTH),
//					cal.get(Calendar.YEAR));
//			dpd.getDatePicker().setMaxDate(cal.getTimeInMillis());
//			
//			dpd.show();
			Calendar c = Calendar.getInstance();
			 int year  = c.get(Calendar.YEAR);
		        int month = c.get(Calendar.MONTH);
		        int day   = c.get(Calendar.DAY_OF_MONTH);
		        Log.v("year", "year " + year);
				 if (year == 2014) {
				 day = 1;
				 month = 0;
				 year = 2015;
				 }
				
				 
				 DatePickerDialog dialog = new DatePickerDialog(v.getContext(),
							dateSet, year, month, day);

				
			
//			 dialog.updateDate(year,month,day);
			 dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
			 dialog.updateDate(year,month,day);
			 dialog.getDatePicker().setCalendarViewShown(false);
			dialog.show();
			break;
			
		case R.id.btn_personal:
			setVisibilityOfLayouts(1);
			break;
			
		case R.id.btn_about_me:
			setVisibilityOfLayouts(2);
			break;
		}
	}

	private void submitForm() {
		switch (tabPos) {
		case 0:
			String PhysicalAddress = etPhyAddr.getText().toString(),
			State = etState.getText().toString(),
			ZipCode = etZip.getText().toString(),
			OfficePhone = etOfcPhn.getText().toString(),
			CellPhone = etCell.getText().toString(),
			Birthday = txtBday.getText().toString(),
			BestWayContactMe = "",
			SocialNetworkingg = txtSocial.getText().toString(),
			AnnualSale = "",
			SupplierAttributes = "";
			if (spinPhone.getSelectedItemPosition() > 0)
				BestWayContactMe = "Phone;"
						+ spinPhone.getSelectedItem().toString();
			if (spinEmail.getSelectedItemPosition() > 0)
				BestWayContactMe = BestWayContactMe + "|Email;"
						+ spinEmail.getSelectedItem().toString();
			if (spinText.getSelectedItemPosition() > 0)
				BestWayContactMe = BestWayContactMe + "|Text;"
						+ spinText.getSelectedItem().toString();
			if (BestWayContactMe.startsWith("|")) {
				BestWayContactMe = BestWayContactMe.substring(1);
			}

			if (spinAnnSalesPers.getSelectedItemPosition() > 0)
				AnnualSale = spinAnnSalesPers.getSelectedItem().toString();
			if (spinPP.getSelectedItemPosition() > 0)
				SupplierAttributes = "Product prices;"
						+ spinPP.getSelectedItem().toString();
			if (spinCS.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes + "|Customer service;"
						+ spinCS.getSelectedItem().toString();
			if (spinFS.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes + "|Fast shipping;"
						+ spinFS.getSelectedItem().toString();
			if (spinDI.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes + "|Deep inventory;"
						+ spinDI.getSelectedItem().toString();
			if (spinSNC.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes
						+ "|Safety & compliance;"
						+ spinSNC.getSelectedItem().toString();
			if (spinDSR.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes
						+ "|Dedicated sales representatives;"
						+ spinDSR.getSelectedItem().toString();

			if (SupplierAttributes.startsWith("|")) {
				SupplierAttributes = SupplierAttributes.substring(1);
			}
			final String[] aboutMeParams = { PhysicalAddress, State, ZipCode,
					OfficePhone, CellPhone, Birthday, BestWayContactMe,
					SocialNetworkingg, AnnualSale, SupplierAttributes };
			new AboutMe().execute(aboutMeParams);
			break;
		case 1:
			String sellingTool = "",
			freeVirtualSample = "",
			aakronSampleKit = "",
			sampleKit = "",
			PSA = "",
			SocialNetworking = "",
			CommunicationMethod = "",
			SendDirectMailClient = "";
			if (spinTools.getSelectedItemPosition() > 0) {
				sellingTool = spinTools.getSelectedItem().toString();
			}
			if (spinSampAdv.getSelectedItemPosition() > 0) {
				freeVirtualSample = spinSampAdv.getSelectedItem().toString();
			}
			if (rbKitsY.isChecked() || rbKitsN.isChecked()) {
				aakronSampleKit = rbKitsY.isChecked() ? "Yes" : "No";
			}
			if (rbWearY.isChecked() || rbWearN.isChecked()) {
				sampleKit = rbWearY.isChecked() ? "Yes" : "No";
			}
			if (spinPSAs.getSelectedItemPosition() > 0) {
				PSA = spinPSAs.getSelectedItem().toString();
			}
			SocialNetworking = txtSocailBusiness.getText().toString();
			// TODO pref comm fix CommunicationMethod
			
			if (spinInPerson.getSelectedItemPosition() > 0)
				CommunicationMethod = "In Person;"
						+ spinInPerson.getSelectedItem().toString();
			if (spinEmail2.getSelectedItemPosition() > 0)
				CommunicationMethod = CommunicationMethod + "|Email;"
						+ spinEmail2.getSelectedItem().toString();
			if (spinDirect.getSelectedItemPosition() > 0)
				CommunicationMethod = CommunicationMethod + "|Direct Mail;"
						+ spinDirect.getSelectedItem().toString();
			if (spinPhone2.getSelectedItemPosition() > 0)
				CommunicationMethod = CommunicationMethod + "|Phone;"
						+ spinPhone2.getSelectedItem().toString();

			if (CommunicationMethod.startsWith("|")) {
				CommunicationMethod = CommunicationMethod.substring(1);
			}

			if (spinDirectMail.getSelectedItemPosition() > 0) {
				SendDirectMailClient = spinDirectMail.getSelectedItem()
						.toString();
			}
			final String[] howISellParams = { sellingTool, freeVirtualSample,
					aakronSampleKit, sampleKit, PSA, SocialNetworking,
					CommunicationMethod, SendDirectMailClient };
			new HowISell().execute(howISellParams);
			break;
		case 2:
			String MarketSell = "",
			MarketNotSelling = "",
			FreeVirtualSample = "";
			MarketSell = txtMarketSelling.getText().toString();
			MarketNotSelling = etMarketNotSelling.getText().toString();
			if (rbCustArtY.isChecked() || rbCustArtN.isChecked()) {
				FreeVirtualSample = rbCustArtY.isChecked() ? "Yes" : "No";
			}
			final String[] myCustParams = { MarketSell, MarketNotSelling,
					FreeVirtualSample };
			new MyCustomers().execute(myCustParams);
			break;
		case 3:
			new WhereIWork().execute();
			break;
		}
	}

	private class AboutMe extends AsyncTask<String, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/HelpUsHelpYou_AboutMe";
		private static final String METHOD_NAME = "HelpUsHelpYou_AboutMe";

		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... p) {

			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("PhysicalAddress", p[0]);
			request.addProperty("State", p[1]);
			request.addProperty("ZipCode", p[2]);
			request.addProperty("OfficePhone", p[3]);
			request.addProperty("CellPhone", p[4]);
			request.addProperty("Birthday", p[5]);
			request.addProperty("BestWayContactMe", p[6]);
			request.addProperty("SocialNetworking", p[7]);
			request.addProperty("AnnualSale", p[8]);
			request.addProperty("SupplierAttributes", p[9]);
			request.addProperty("UserId", userID);
			
			Log.v("About request ", request.toString());
//			{"PhysicalAddress":"abc",
//			 "State":"maharstara",
//			 "ZipCode":"400093",
//			 "OfficePhone":"28953862",
//			 "CellPhone":"7894561235",
//			 "Birthday":"02/08/1986",
//			 "BestWayContactMe":"call me",
//			 "SocialNetworking":"fa|whatsapp|orkut",
//			 "AnnualSale":"5cr",
//			 "SupplierAttributes":"lool",
//			 "UserId":"5"}
//			
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				// HelpUs.this.dismiss();
				showToast("Feedback submitted successfully");
				return;
			} else
				showToast("Server error: Feedback not sent");
		}

	}

	private class HowISell extends AsyncTask<String, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/HelpUsHelpYou_HowISell";
		private static final String METHOD_NAME = "HelpUsHelpYou_HowISell";

		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... p) {

			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("SellingTool", p[0]);
			request.addProperty("FreeVirtualSample", p[1]);
			request.addProperty("AakronSampleKit", p[2]);
			request.addProperty("SampleKit", p[3]);
			request.addProperty("PSA", p[4]);
			request.addProperty("SocialNetworking", p[5]);
			request.addProperty("CommunicationMethod", p[6]);
			request.addProperty("SendDirectMailClient", p[7]);
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				// HelpUs.this.dismiss();
				showToast("Feedback submitted successfully");
				return;
			} else
				showToast("Server error: Feedback not sent");
		}

	}

	private class MyCustomers extends AsyncTask<String, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/HelpUsHelpYou_MyCustomers";
		private static final String METHOD_NAME = "HelpUsHelpYou_MyCustomers";

		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... p) {

			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("MarketSell", p[0]);
			request.addProperty("MarketNotSelling", p[1]);
			request.addProperty("FreeVirtualSample", p[2]);
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				// HelpUs.this.dismiss();
				showToast("Feedback submitted successfully");
				return;
			} else
				showToast("Server error: Feedback not sent");
		}

	}

	private class WhereIWork extends AsyncTask<Void, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/HelpUsHelpYou_WhereIWork";
		private static final String METHOD_NAME = "HelpUsHelpYou_WhereIWork";

		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {

			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("AnnualSale", spinAnnSales.getSelectedItem()
					.toString());
			request.addProperty("Decision", spinBuyDeci.getSelectedItem()
					.toString());
			request.addProperty("OfficeLocations", etOfcLoc.getText()
					.toString());
			request.addProperty("SalesRep", etNumReps.getText().toString());
			request.addProperty("VendorRelationManager", etVRM.getText()
					.toString());
			request.addProperty("Emergency", etEmrCon.getText().toString());
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				// HelpUs.this.dismiss();
				showToast("Feedback submitted successfully");
				return;
			} else
				showToast("Server error: Feedback not sent");
		}

	}

	private static final int REQ_SOCIAL = 100;
	private static final int REQ_SOCIAL_BUSINESS = 200;
	private static final int REQ_MARKET = 300;

	@Override
	public void onDialogSubmit(int requestCode, String result) {
		switch (requestCode) {
		case REQ_SOCIAL:
			txtSocial.setText(result);
			break;
		case REQ_MARKET:
			txtMarketSelling.setText(result);
			break;
		case REQ_SOCIAL_BUSINESS:
			txtSocailBusiness.setText(result);
			break;
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		
		switchTab(position);
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}


}
