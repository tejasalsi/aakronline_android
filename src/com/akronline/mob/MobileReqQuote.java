package com.akronline.mob;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.adapters.SampleColorAdapter;
import com.akronline.R;
import com.datamodel.Product;
import com.progressbar.GoogleProgressDialog;

public class MobileReqQuote extends Fragment implements OnCheckedChangeListener, OnClickListener
{
	private static final String NAMESPACE = "http://tempuri.org/";
	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
	CheckBox cbSpotQ, cbFCDQ;

	

	Button btnSubmit;

	EditText etQuantity, etInfo;

	private TextView txtChoosecolorQ;

	private TextView txtQuantQ;

	private RelativeLayout pricingLayoutQ;

	private Spinner spinColorsQ;

	private Button btnReqQuote;

	private EditText etQuantQ;

	private EditText etQuantQ2;

	private EditText etQuantQ3;

	private EditText etQuantQ4;

	private EditText etQuantQ5;

	private TextView txtPricingQ;

	private TextView txtStandardQ;

	private TextView txtExpiresQ;

	private ImageView ivCAQ;

	private ImageView ivUSQ;

	private ImageView arrowCAQ;

	private ImageView arrowUSQ;

	private TextView noteQ;

	private LinearLayout noteslayoutQ;

	private LinearLayout spclLineQ;

	private LinearLayout newPriceLayoutQ;

	private TextView Qq1;

	private TextView Qq2;

	private TextView Qq3;

	private TextView Qq4;

	private TextView Qq5;

	private TextView Qst1;

	private TextView Qst2;

	private TextView Qst3;

	private TextView Qst4;

	private TextView Qst5;

	private TextView Qdc1;

	private TextView Qsp1;

	private TextView Qsp2;

	private TextView Qsp3;

	private TextView Qsp4;

	private TextView Qsp5;

	private TextView Qdc2;

	private TextView Qsq1;

	private TextView Qsq2;

	private TextView Qsq3;

	private TextView Qsq4;

	private TextView Qsq5;

	private TextView Qp1;

	private TextView Qp2;

	private TextView Qp3;

	private TextView Qp4;

	private TextView Qp5;

	private TextView Qdc3;

	String[] stdQuantsQ;

	String[] stdUSQ;

	String[] stdCAQ;

	private String stdCodeQ;

	private String prodNoteQ;

	private String[] splQuantsQ;

	private String[] splUSQ;

	private String[] splCAQ;

	private String splCodeQ;

	private String splExpDateQ;

	private String[] newQuantsQ;

	private String[] newUSQ;

	private String[] newCAQ;
	
	private String[] spinnerValues;

	private String newCodeQ;

	DecimalFormat df = new DecimalFormat();
	private String userID;
	private Product product;

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		Log.e("tag", "MobileReqQuote");
		userID = getActivity().getApplicationContext()
				.getSharedPreferences("LoginPref", 0)
				.getString("USER-ID", null);
		View v = inflater.inflate(R.layout.request_quote_layout, container, false);
		// Request Quote
		txtChoosecolorQ = (TextView) v.findViewById(R.id.txtChooseColorQ);
		txtChoosecolorQ.setText(Html.fromHtml("Available Color <font color=red>*</font>"));
		txtQuantQ = (TextView) v.findViewById(R.id.txtQuantityQ);
		txtQuantQ.setText(Html.fromHtml("Quantity <font color=red>*</font>"));
		pricingLayoutQ = (RelativeLayout) v.findViewById(R.id.pricingQ);
		cbSpotQ = (CheckBox) v.findViewById(R.id.spot_checkboxQ);
		cbFCDQ = (CheckBox) v.findViewById(R.id.fcd_checkboxQ);
		spinColorsQ = (Spinner) v.findViewById(R.id.spinColorsQ);
		btnReqQuote = (Button) v.findViewById(R.id.btnReqQuote);
		etQuantQ = (EditText) v.findViewById(R.id.etQuantityQ);
		etQuantQ2 = (EditText) v.findViewById(R.id.etQuantityQ2);
		etQuantQ3 = (EditText) v.findViewById(R.id.etQuantityQ3);
		etQuantQ4 = (EditText) v.findViewById(R.id.etQuantityQ4);
		etQuantQ5 = (EditText) v.findViewById(R.id.etQuantityQ5);
		etInfo = (EditText) v.findViewById(R.id.etInfoQ);
		txtPricingQ = (TextView) v.findViewById(R.id.txtPricingQ);
		txtStandardQ = (TextView) v.findViewById(R.id.txtStandardQ);
		txtExpiresQ = (TextView) v.findViewById(R.id.txtExpiresQ);
		ivCAQ = (ImageView) v.findViewById(R.id.imgCAQ);
		ivUSQ = (ImageView) v.findViewById(R.id.imgUSAQ);
		arrowCAQ = (ImageView) v.findViewById(R.id.arrowCAQ);
		arrowUSQ = (ImageView) v.findViewById(R.id.arrowUSQ);
		noteQ = (TextView) v.findViewById(R.id.txtNoteQ);
		noteslayoutQ = (LinearLayout) v.findViewById(R.id.noteLayoutQ);
		spclLineQ = (LinearLayout) v.findViewById(R.id.specialPricesQ);
		newPriceLayoutQ = (LinearLayout) v.findViewById(R.id.newPricesQ);
		Qq1 = (TextView) v.findViewById(R.id.Qquant1);
		Qq2 = (TextView) v.findViewById(R.id.Qquant2);
		Qq3 = (TextView) v.findViewById(R.id.Qquant3);
		Qq4 = (TextView) v.findViewById(R.id.Qquant4);
		Qq5 = (TextView) v.findViewById(R.id.Qquant5);
		Qst1 = (TextView) v.findViewById(R.id.Qstd1);
		Qst2 = (TextView) v.findViewById(R.id.Qstd2);
		Qst3 = (TextView) v.findViewById(R.id.Qstd3);
		Qst4 = (TextView) v.findViewById(R.id.Qstd4);
		Qst5 = (TextView) v.findViewById(R.id.Qstd5);
		Qdc1 = (TextView) v.findViewById(R.id.Qdc1);
		Qsp1 = (TextView) v.findViewById(R.id.Qspcl1);
		Qsp2 = (TextView) v.findViewById(R.id.Qspcl2);
		Qsp3 = (TextView) v.findViewById(R.id.Qspcl3);
		Qsp4 = (TextView) v.findViewById(R.id.Qspcl4);
		Qsp5 = (TextView) v.findViewById(R.id.Qspcl5);
		Qdc2 = (TextView) v.findViewById(R.id.Qdc2);
		Qsq1 = (TextView) v.findViewById(R.id.Qpq1);
		Qsq2 = (TextView) v.findViewById(R.id.Qpq2);
		Qsq3 = (TextView) v.findViewById(R.id.Qpq3);
		Qsq4 = (TextView) v.findViewById(R.id.Qpq4);
		Qsq5 = (TextView) v.findViewById(R.id.Qpq5);
		Qp1 = (TextView) v.findViewById(R.id.Qp1);
		Qp2 = (TextView) v.findViewById(R.id.Qp2);
		Qp3 = (TextView) v.findViewById(R.id.Qp3);
		Qp4 = (TextView) v.findViewById(R.id.Qp4);
		Qp5 = (TextView) v.findViewById(R.id.Qp5);
		Qdc3 = (TextView) v.findViewById(R.id.Qdc3);

		ivCAQ.setOnClickListener(this);
		ivUSQ.setOnClickListener(this);

		cbSpotQ = (CheckBox) v.findViewById(R.id.cbSpotQ);
		cbFCDQ = (CheckBox) v.findViewById(R.id.cbFCDQ);
		spinColorsQ = (Spinner) v.findViewById(R.id.spinColorsQ);
		btnSubmit = (Button) v.findViewById(R.id.btnSubmitQ);
		etQuantity = (EditText) v.findViewById(R.id.etQuantity);
		

		cbSpotQ.setOnCheckedChangeListener(this);
		cbFCDQ.setOnCheckedChangeListener(this);

		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(3);
		btnSubmit.setOnClickListener(this);

		spinnerValues = getArguments().getStringArray("SPINNER_VALUE");
		SampleColorAdapter ca = new SampleColorAdapter(getActivity(), R.layout.request_sample_color_dropdown_inflator,spinnerValues);
		spinColorsQ.setAdapter(ca);

		try
		{
			JSONObject response = new JSONObject(getArguments().getString("RESPONSE_DATA"));
			JSONArray arr = response.optJSONArray("ProductData");
			if (arr != null && arr.length() > 0) {
				product = new Product();
				product.setID(arr.optJSONObject(0).optString(
						"productCode"));
			}
			// PRICING DATA
			int stdPrices = 0, splPrices = 0, newPrices = 0;
			JSONArray prices = response.optJSONArray("Pricing");
			if (prices != null && prices.length() > 0)
			{
				stdPrices = Integer.valueOf(prices.getJSONObject(0).getString("RowCount1"));
				splPrices = Integer.valueOf(prices.getJSONObject(0).getString("RowCount2"));
				newPrices = Integer.valueOf(prices.getJSONObject(0).getString("RowCount3"));
				System.out.println(stdPrices + " " + splPrices + " " + newPrices);
			}
			if (stdPrices > 0)
			{
				stdQuantsQ = new String[stdPrices];
				stdUSQ = new String[stdPrices];
				stdCAQ = new String[stdPrices];
				JSONArray stdPrice = response.optJSONArray("PriceData1");
				for (int i = 0; i < stdPrices; i++)
				{
					JSONObject ob = stdPrice.optJSONObject(i);
					stdCodeQ = ob.getString("discountCode");
					stdQuantsQ[i] = ob.getString("Quantity");
					stdUSQ[i] = ob.getString("USPrice");
					stdCAQ[i] = ob.getString("CanadianPrice");
					prodNoteQ = ob.getString("noteText");
				}
				if (splPrices > 0)
				{
					splQuantsQ = new String[splPrices];
					splUSQ = new String[splPrices];
					splCAQ = new String[splPrices];
					JSONArray splPrice = response.getJSONArray("PriceData2");
					for (int i = 0; i < splPrices; i++)
					{
						JSONObject ob = splPrice.optJSONObject(i);
						splCodeQ = ob.getString("discountCode");
						splQuantsQ[i] = ob.getString("Quantity");
						splUSQ[i] = ob.getString("USPrice");
						splCAQ[i] = ob.getString("CanadianPrice");
						splExpDateQ = ob.getString("expirationDate").split(" ")[0];
					}
				}
			}
			if (newPrices > 0)
			{
				newQuantsQ = new String[newPrices];
				newUSQ = new String[newPrices];
				newCAQ = new String[newPrices];
				JSONArray newPrice = response.optJSONArray("PriceData3");
				for (int i = 0; i < newPrices; i++)
				{
					JSONObject ob = newPrice.optJSONObject(i);
					newCodeQ = ob.getString("discountCode");
					newQuantsQ[i] = ob.getString("Quantity");
					newUSQ[i] = ob.getString("USPrice");
					newCAQ[i] = ob.getString("CanadianPrice");
				}
			}
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		showQuotePrices(1);
		return v;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
		switch (buttonView.getId())
		{
			case R.id.cbSpotQ:
				if (isChecked)
				{
					//cbFCDQ.setOnCheckedChangeListener(null);
					cbFCDQ.setChecked(false);
					//cbFCDQ.setOnCheckedChangeListener(this);
				}
				break;
			case R.id.cbFCDQ:
				if (isChecked)
				{
					//cbSpotQ.setOnCheckedChangeListener(null);
					cbSpotQ.setChecked(false);
					//cbSpotQ.setOnCheckedChangeListener(this);
				}
				break;
			default:
				break;
		}
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			
			case R.id.imgUSAQ:
				showQuotePrices(1);
				break;
			case R.id.imgCAQ:
				showQuotePrices(2);
				break;
			case R.id.btnSubmitQ:
				if (spinnerValues == null || spinnerValues.length == 0) {
					showToast("No color available for this product");
					return;
				}
				if (!cbFCDQ.isChecked() && !cbSpotQ.isChecked()) {
					showToast("Please select Imprint Method");
					return;
				}
				if (etQuantQ.getText().toString().isEmpty()
						&& etQuantQ2.getText().toString().isEmpty()
						&& etQuantQ3.getText().toString().isEmpty()
						&& etQuantQ4.getText().toString().isEmpty()
						&& etQuantQ5.getText().toString().isEmpty()) {
					showToast("Please enter at least 1 quantity");
					etQuantQ.requestFocus();
					return;
				}
				
				String quants = "";
				if (!etQuantQ.getText().toString().isEmpty()){
					if(Integer.parseInt(etQuantQ.getText().toString()) < 1){
						showToast("Please enter quantity greater than 0 or leave blank");
						return;
					}
					quants = etQuantQ.getText().toString();
					
				}
				if (!etQuantQ2.getText().toString().isEmpty()) {
					if (quants.equals(""))
						quants = etQuantQ2.getText().toString();
					else
						if(Integer.parseInt(etQuantQ2.getText().toString()) < 1){
							showToast("Please enter quantity greater than 0 or leave blank");
							return;
						}
						quants = quants + "|" + etQuantQ2.getText().toString();
				}
				if (!etQuantQ3.getText().toString().isEmpty()) {
					if (quants.equals(""))
						quants = etQuantQ2.getText().toString();
					else
						if(Integer.parseInt(etQuantQ3.getText().toString()) < 1){
							showToast("Please enter quantity greater than 0 or leave blank");
							return;
						}
						quants = quants + "|" + etQuantQ3.getText().toString();
				}
				if (!etQuantQ4.getText().toString().isEmpty()) {
					if (quants.equals(""))
						quants = etQuantQ4.getText().toString();
					else
						if(Integer.parseInt(etQuantQ4.getText().toString()) < 1){
							showToast("Please enter quantity greater than 0 or leave blank");
							return;
						}
						quants = quants + "|" + etQuantQ2.getText().toString();
				}
				if (!etQuantQ5.getText().toString().isEmpty()) {
					if (quants.equals(""))
						quants = etQuantQ5.getText().toString();
					else
						if(Integer.parseInt(etQuantQ5.getText().toString()) < 1){
							showToast("Please enter quantity greater than 0 or leave blank");
							return;
						}
						quants = quants + "|" + etQuantQ5.getText().toString();
				}
				Log.d("quants", quants);
				new RequestQuote().execute(quants);
				break;
			default:
				break;
		}

	}
	private void showToast(String msg) {
		Toast.makeText(getActivity().getApplicationContext(), msg,
				Toast.LENGTH_LONG).show();
	}
	public class RequestQuote extends AsyncTask<String, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/SetRequestQuote";
		private static final String METHOD_NAME = "SetRequestQuote";
		GoogleProgressDialog dialog;
		JSONObject obj;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("Productcode", product.getProductCode());
			request.addProperty("ImprintMethod",
					cbFCDQ.isChecked() ? cbFCDQ.getText() : cbSpotQ.getText());
			request.addProperty("Colors", spinColorsQ.getSelectedItem()
					.toString());
			request.addProperty("Quantity", params[0]);
			request.addProperty("Info", etInfo.getText().toString());
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (result != null) {
					showToast(result);
					return;
				}
				if (obj.getJSONArray("Data").getJSONObject(0)
						.getString("responseCode").equals("0")) {
					// success
					showToast("Quote Request posted successfully");
					etQuantQ.setText("");
					etQuantQ2.setText("");
					etQuantQ3.setText("");
					etQuantQ4.setText("");
					etQuantQ5.setText("");
				} else {
					showToast("Error Occured: Request not submitted");
				}
			} catch (Exception e) {
				e.printStackTrace();
				showToast("Error Occured: Request not submitted");
			}

		}

	}

	private void showQuotePrices(int i)
	{
		if (prodNoteQ.equals(""))
		{
			noteslayoutQ.setVisibility(GONE);
		}
		else
		{
			noteQ.setText(prodNoteQ);
			noteslayoutQ.setVisibility(VISIBLE);
		}
		switch (stdQuantsQ.length)
		{
			case 5:
				Qq5.setText(stdQuantsQ[4]);
			case 4:
				if (Qq5.getText().toString().equalsIgnoreCase("")) {
					Qq5.setVisibility(View.GONE);
				}
				Qq4.setText(stdQuantsQ[3]);
			case 3:
				Qq3.setText(stdQuantsQ[2]);
			case 2:
				Qq2.setText(stdQuantsQ[1]);
			case 1:
				Qq1.setText(stdQuantsQ[0]);
		}
		if (splUSQ != null)
		{
			spclLineQ.setVisibility(VISIBLE);
			txtExpiresQ.setVisibility(VISIBLE);
			Qst1.setPaintFlags(Qst1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			Qst2.setPaintFlags(Qst1.getPaintFlags());
			Qst3.setPaintFlags(Qst1.getPaintFlags());
			Qst4.setPaintFlags(Qst1.getPaintFlags());
			Qst5.setPaintFlags(Qst1.getPaintFlags());
			txtStandardQ.setPaintFlags(Qst1.getPaintFlags());
			txtExpiresQ.setText("Expires: " + splExpDateQ);
		}
		else
		{
			spclLineQ.setVisibility(GONE);
			txtExpiresQ.setVisibility(GONE);
		}

		if (i == 1)
		{ // US pricing
			arrowUSQ.setVisibility(VISIBLE);
			arrowCAQ.setVisibility(GONE);
			txtPricingQ.setText("Pricing, US Dollars");
			Qdc1.setText(stdCodeQ);
			switch (stdUSQ.length)
			{
				case 5:
					Qst5.setText("$"+df.format(Float.valueOf(stdUSQ[4])));
				case 4:
					if (Qst5.getText().toString().equalsIgnoreCase("")) {
						Qst5.setVisibility(View.GONE);
					}
					Qst4.setText("$"+df.format(Float.valueOf(stdUSQ[3])));
				case 3:
					Qst3.setText("$"+df.format(Float.valueOf(stdUSQ[2])));
				case 2:
					Qst2.setText("$"+df.format(Float.valueOf(stdUSQ[1])));
				case 1:
					Qst1.setText("$"+df.format(Float.valueOf(stdUSQ[0])));
			}
			if (splUSQ != null)
			{
				Qdc2.setText(splCodeQ);
				switch (splUSQ.length)
				{
					case 5:
						Qsp5.setText("$"+df.format(Float.valueOf(splUSQ[4])));
					case 4:
						if (Qsp5.getText().toString().equalsIgnoreCase("")) {
							Qsp5.setVisibility(View.GONE);
						}
						Qsp4.setText("$"+df.format(Float.valueOf(splUSQ[3])));
					case 3:
						Qsp3.setText("$"+df.format(Float.valueOf(splUSQ[2])));
					case 2:
						Qsp2.setText("$"+df.format(Float.valueOf(splUSQ[1])));
					case 1:
						Qsp1.setText("$"+df.format(Float.valueOf(splUSQ[0])));
				}
			}

		}
		else if (i == 2)
		{ // CA pricing
			arrowUSQ.setVisibility(GONE);
			arrowCAQ.setVisibility(VISIBLE);
			txtPricingQ.setText("Pricing, Canadian Dollars");
			Qdc1.setText(stdCodeQ);
			switch (stdCAQ.length)
			{
				case 5:
					Qst5.setText("$"+df.format(Float.valueOf(stdCAQ[4])));
				case 4:
					if (Qst5.getText().toString().equalsIgnoreCase("")) {
						Qst5.setVisibility(View.GONE);
					}
					Qst4.setText("$"+df.format(Float.valueOf(stdCAQ[3])));
				case 3:
					Qst3.setText("$"+df.format(Float.valueOf(stdCAQ[2])));
				case 2:
					Qst2.setText("$"+df.format(Float.valueOf(stdCAQ[1])));
				case 1:
					Qst1.setText("$"+df.format(Float.valueOf(stdCAQ[0])));
			}
			if (splCAQ != null)
			{
				Qdc2.setText(splCodeQ);
				switch (splCAQ.length)
				{
					case 5:
						Qsp5.setText("$"+df.format(Float.valueOf(splCAQ[4])));
					case 4:
						if (Qsp5.getText().toString().equalsIgnoreCase("")) {
							Qsp5.setVisibility(View.GONE);
						}
						Qsp4.setText("$"+df.format(Float.valueOf(splCAQ[3])));
					case 3:
						Qsp3.setText("$"+df.format(Float.valueOf(splCAQ[2])));
					case 2:
						Qsp2.setText("$"+df.format(Float.valueOf(splCAQ[1])));
					case 1:
						Qsp1.setText("$"+df.format(Float.valueOf(splCAQ[0])));
				}
			}

		}
	}
}
