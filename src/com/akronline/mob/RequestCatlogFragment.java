package com.akronline.mob;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.akronline.R;
import com.progressbar.GoogleProgressDialog;

public class RequestCatlogFragment extends BaseFragment implements OnClickListener{
	
	Context ctx;
	String userID;
	Spinner spinCatalogs, spinFlyers;
	EditText notes;
	Button btnSubmit, btnCancel;
	private static final String[] items = {"0", "1", "2", "3" };
	

	SharedPreferences prefs;
	boolean isLogin = false;
	String emaiID;
	String repID;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View _view = inflater.inflate(R.layout.request_catlog_fragment, null);
		
		Log.e("tag", "RequestCatlogFragment");
		prefs = getActivity().getSharedPreferences("LoginPref", 0);
		userID = prefs.getString("USER-ID", "");
		emaiID = prefs.getString("USER-EMAIL", "");
		repID = prefs.getString("REP-ID", "0");
		
		btnCancel = (Button) _view.findViewById(R.id.btnCancel);
		btnSubmit = (Button) _view.findViewById(R.id.btnSubmit);
		notes = (EditText) _view.findViewById(R.id.etNotes);
		spinCatalogs = (Spinner) _view.findViewById(R.id.spinCatalogs);
		spinFlyers = (Spinner) _view.findViewById(R.id.spinFlyers);

		spinCatalogs
				.setAdapter(new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1, items));
		spinFlyers
				.setAdapter(new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1, items));

		spinCatalogs.setSelection(0);
		spinFlyers.setSelection(0);

		btnSubmit.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		
		final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(_view.getWindowToken(), 0);

		return _view;
	}

	@Override
	public void onStart() {
		super.onStart();
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnCancel:
				getActivity().onBackPressed();
				break;
			case R.id.btnSubmit:
//				if (spinCatalogs.getSelectedItemPosition() == 0
//						&& spinFlyers.getSelectedItemPosition() == 0) {
//					showToast("Please select at least one item");
//					return;
//				}
				new Submit().execute();
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception: " + e.getMessage());
		}

	}

	private void showToast(String msg) {
		Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_LONG)
				.show();
	}

	private class Submit extends AsyncTask<Void, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/RequestACatalog";
		private static final String NAMESPACE = "http://tempuri.org/";
		private static final String METHOD_NAME = "RequestACatalog";
		private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("NumberOfCatalogs", spinCatalogs
					.getSelectedItem().toString());
			request.addProperty("NumberOfFlyers", spinFlyers.getSelectedItem()
					.toString());
			request.addProperty("AdditionalInfo", notes.getText().toString()
					.trim());
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				showToast("Request sent successfully");
				getActivity().onBackPressed();
			} else
				showToast("Server Error: Request not sent");

		}
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
//			String title = DataHolderClass.getInstance().get_titleCatagory();
			((HomeMob) getActivity()).setTitle("Request Catalog");
		} catch (Exception e) {
			e.printStackTrace();
		}
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.search.setVisibility(View.GONE);
	}

}
