package com.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akronline.R;
import com.datamodel.InventoryModel;

public class InventoryAdapter extends BaseAdapter {
	Context _ctx;
	ArrayList<InventoryModel> _list;
	HolderClass holder = null;

	public InventoryAdapter(Context ctx, ArrayList<InventoryModel> list) {
		this._ctx = ctx;
		this._list = list;
	}

	@Override
	public int getCount() {
		return _list.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if (row == null) {
			LayoutInflater inflater = ((Activity) _ctx).getLayoutInflater();
			row = inflater.inflate(R.layout.inventory_inflator, parent, false);
			holder = new HolderClass();
			holder._cname = (TextView) row.findViewById(R.id.colorname);
			holder._cvalue = (TextView) row.findViewById(R.id.colorvalue);

			row.setTag(holder);
		} else {
			holder = (HolderClass) row.getTag();
		}
		try {
			InventoryModel _model = _list.get(position);
			holder._cname.setText(_model.getColorName());
			holder._cvalue.setText(_model.getInventoryAvailable());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return row;
	}

	class HolderClass {
		TextView _cname;
		TextView _cvalue;
	}
}
