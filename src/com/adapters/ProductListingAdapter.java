package com.adapters;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.akronline.R;
import com.androidquery.AQuery;
import com.datamodel.Product;

public class ProductListingAdapter extends BaseAdapter {

	Context context;
	ArrayList<Product> products;
	ViewHolder holder = null;
	private static final DecimalFormat df = new DecimalFormat();

	public ProductListingAdapter(Context ctx, ArrayList<Product> _list) {
		this.context = ctx;
		this.products = _list;
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(3);
	}

	@Override
	public int getCount() {
		return products.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {

		if (v == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			v = inflater.inflate(R.layout.product_list_inflator, parent, false);
			holder = new ViewHolder();
			holder.productImage = (ImageView) v
					.findViewById(R.id._productImage);
			holder.productName = (TextView) v.findViewById(R.id._productName);
			holder.productPrice = (TextView) v.findViewById(R.id._productPrice);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		try {
			Product p = products.get(position);
			AQuery aq = new AQuery(context);
			aq.id(holder.productImage).image(p.getImageURL());
			holder.productName.setText(p.getName());
			float price = 0f;
			try {
				price = Float.valueOf(p.getPrice().substring(1));
			} catch (Exception e) {
				price = 0f;
			} finally {
				holder.productPrice.setText("$" + df.format(price));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return v;
	}

	private static class ViewHolder {
		TextView productName;
		TextView productPrice;
		ImageView productImage;
	}
}
