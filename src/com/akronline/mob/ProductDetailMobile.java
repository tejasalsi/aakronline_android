package com.akronline.mob;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.R.menu;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.adapters.ProductDetailAdapter;
import com.akronline.PlayVideoScreen;
import com.akronline.ProductScreen;
import com.akronline.R;
import com.akronline.dialogs.SendEmail;
import com.androidquery.AQuery;
import com.commondata.CommonData;
import com.dataholder.DataHolderClass;
import com.datamodel.ProductDetailModel;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.progressbar.GoogleProgressDialog;

public class ProductDetailMobile extends Fragment implements OnClickListener {

	@Override
	public void onResume() {
		super.onResume();
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.search.setVisibility(View.GONE);
		HomeMob.title.setVisibility(View.VISIBLE);
	}

	private static String SOAP_ACTION = "http://tempuri.org/GetProductData";

	private static String NAMESPACE = "http://tempuri.org/";

	private static String METHOD_NAME = "GetProductData";

	private static String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	String _ProductCode, _ProductName, _productDescription, _AsLowAsPrice,
			_MultimediaContent, _ItemColors, _ImageUrl;

	public static String productId;

	public static String _result;

	GoogleProgressDialog _dialog;

	com.akronline.mob.CustomListView infoList;
	

	SharedPreferences _AakronlineLoginPref;

	String _getUserId = "";

	ImageView prodImg;

	TextView tvProductCode, tvProductName, tvPrice, tvDetails, tvCharges,
			tvOptions, tvFeatures;

	LinearLayout two, three, four, five;

	ArrayList<ProductDetailModel> _chargeList;

	ArrayList<ProductDetailModel> _DetailsList;

	//ArrayList<InventoryModel> _inventoryList;

	ArrayList<ProductDetailModel> _OptionsList;

	AQuery aq;

	DecimalFormat df = new DecimalFormat();

	LinearLayout noteslayout, spclLine;

	LinearLayout newPriceLayout;

	RelativeLayout pricingLayout;

	String[] stdQuants;

	String[] stdUS;

	String[] stdCA;

	String stdCode;

	String prodNote;

	String[] splQuants;

	String[] splUS;

	String[] splCA;

	String splCode;

	String splExpDate;

	String[] newQuants;

	String[] newUS;

	String[] newCA;

	JSONArray prices;

	String newCode;

	String[] spinnerValues;

	private TextView txtProdDesc;

	TextView note;

	TextView q1;

	TextView q2;

	TextView q3;

	TextView q4;

	TextView q5;

	TextView st1;

	TextView st2;

	TextView st3;

	TextView st4;

	TextView st5;

	TextView dc1;

	TextView sp1;

	TextView sp2;

	TextView sp3;

	TextView sp4;

	TextView sp5;

	TextView txtPricing;

	TextView txtStandard;

	TextView txtExpires;

	ImageView ivCA;

	ImageView ivUS;

	ImageView arrowCA;

	ImageView arrowUS;

	TextView dc2;

	public ImageView btnVideo, btnEmail;

	ScrollView scrollView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Log.e("tag", "ProductDetailMobile");
		
		HomeMob.search.setVisibility(View.GONE);
		HomeMob.searchField.setVisibility(View.GONE);

		View v = inflater.inflate(R.layout.product_details_mobile, null);
		df.setMinimumFractionDigits(2);
		scrollView = (ScrollView) v.findViewById(R.id.scroll);
		pricingLayout = (RelativeLayout) v.findViewById(R.id.pricing);
		tvPrice = (TextView) v.findViewById(R.id.set_price);
		tvProductCode = (TextView) v.findViewById(R.id.set_product_id);
		tvProductName = (TextView) v.findViewById(R.id.set_product_name);
		txtProdDesc = (TextView) v.findViewById(R.id.txtProdDesc);
		prodImg = (ImageView) v.findViewById(R.id.product_image);
		tvDetails = (TextView) v.findViewById(R.id.txtDetails);
		tvOptions = (TextView) v.findViewById(R.id.txtOptinns);
		tvCharges = (TextView) v.findViewById(R.id.txtCharges);
		tvFeatures = (TextView) v.findViewById(R.id.txtFeatures);
		noteslayout = (LinearLayout) v.findViewById(R.id.noteLayout);
		infoList = (com.akronline.mob.CustomListView) v
				.findViewById(R.id.detailList);
		two = (LinearLayout) v.findViewById(R.id.two);
		three = (LinearLayout) v.findViewById(R.id.three);
		four = (LinearLayout) v.findViewById(R.id.four);
		five = (LinearLayout) v.findViewById(R.id.five);

		tvDetails.setOnClickListener(this);
		tvOptions.setOnClickListener(this);
		tvCharges.setOnClickListener(this);
		tvFeatures.setOnClickListener(this);
		two.setOnClickListener(this);
		three.setOnClickListener(this);
		four.setOnClickListener(this);
		five.setOnClickListener(this);
		note = (TextView) v.findViewById(R.id.txtNote);
		spclLine = (LinearLayout) v.findViewById(R.id.specialPrices);
		newPriceLayout = (LinearLayout) v.findViewById(R.id.newPrices);
		q1 = (TextView) v.findViewById(R.id.quant1);
		q2 = (TextView) v.findViewById(R.id.quant2);
		q3 = (TextView) v.findViewById(R.id.quant3);
		q4 = (TextView) v.findViewById(R.id.quant4);
		q5 = (TextView) v.findViewById(R.id.quant5);
		st1 = (TextView) v.findViewById(R.id.std1);
		st2 = (TextView) v.findViewById(R.id.std2);
		st3 = (TextView) v.findViewById(R.id.std3);
		st4 = (TextView) v.findViewById(R.id.std4);
		st5 = (TextView) v.findViewById(R.id.std5);
		dc1 = (TextView) v.findViewById(R.id.dc1);
		sp1 = (TextView) v.findViewById(R.id.spcl1);
		sp2 = (TextView) v.findViewById(R.id.spcl2);
		sp3 = (TextView) v.findViewById(R.id.spcl3);
		sp4 = (TextView) v.findViewById(R.id.spcl4);
		sp5 = (TextView) v.findViewById(R.id.spcl5);
		dc2 = (TextView) v.findViewById(R.id.dc2);
		txtPricing = (TextView) v.findViewById(R.id.txtPricing);
		txtStandard = (TextView) v.findViewById(R.id.txtStandard);
		txtExpires = (TextView) v.findViewById(R.id.txtExpires);
		ivCA = (ImageView) v.findViewById(R.id.imgCA);
		ivUS = (ImageView) v.findViewById(R.id.imgUSA);
		arrowCA = (ImageView) v.findViewById(R.id.arrowCA);
		arrowUS = (ImageView) v.findViewById(R.id.arrowUS);
		btnEmail = (ImageView) v.findViewById(R.id.sendEmail);
		btnVideo = (ImageView) v.findViewById(R.id.playVideo);

		ivCA.setOnClickListener(this);
		ivUS.setOnClickListener(this);
		btnEmail.setOnClickListener(this);
		btnVideo.setOnClickListener(this);

		
		  /*infoList.setOnTouchListener(new ListView.OnTouchListener() {
		  
		  @Override public boolean onTouch(View v, MotionEvent event) {
		  
		  int action = event.getAction(); switch (action) { case
		  MotionEvent.ACTION_DOWN:
		  v.getParent().requestDisallowInterceptTouchEvent(true); break;
		  
		  case MotionEvent.ACTION_UP:
		  v.getParent().requestDisallowInterceptTouchEvent(false); break; }
		 v.onTouchEvent(event); return true; } });*/
		 

		aq = new AQuery(getActivity());
		try {
			_AakronlineLoginPref = getActivity().getApplicationContext()
					.getSharedPreferences("LoginPref", 0);
			_getUserId = _AakronlineLoginPref.getString("USER-ID", null);
			new ProductDetails(DataHolderClass.getInstance().get_pId())
					.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return v;

	}

	/****
	 * Method for Setting the Height of the ListView dynamically. Hack to fix
	 * the issue of not showing all the items of the ListView when placed inside
	 * a ScrollView
	 ****/
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));

			view.measure(MeasureSpec.makeMeasureSpec(desiredWidth,
					MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(0,
					MeasureSpec.UNSPECIFIED));
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	@Override
	public void onPause() {
		super.onPause();
		_dialog.dismiss();
	}
	@Override
	public void onDetach() {
		super.onDetach();
		HomeMob.search.setVisibility(View.GONE);
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.title.setVisibility(View.VISIBLE);
	}

	/******************************** fetch Product Details *********************************************/
	private class ProductDetails extends AsyncTask<String, String, String> {

		public ProductDetails(String id) {
			productId = id;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			_dialog = new GoogleProgressDialog(getActivity());
			_dialog.setCancelable(false);
			_dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				//_chargeList.clear();
				//_inventoryList.clear();
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				request.addProperty("ProductCode", productId);
				if (_getUserId != null && _getUserId.length() > 0) {
					request.addProperty("userid", _getUserId);
				} else {
					request.addProperty("userid", "0");
				}
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null && result.getPropertyCount() > 0) {
					try {
						Log.v("response", result.toString());
						Log.e("ProductDetail", "" + result.toString());
						_result = result.getProperty(0).toString();
						JSONObject _obj = new JSONObject(_result);
						if (_obj != null) {
							JSONArray _array = _obj.optJSONArray("ProductData");
							if (_array != null && _array.length() > 0) {
								_ProductCode = _array.optJSONObject(0)
										.optString("productCode");
								_ProductName = _array.optJSONObject(0)
										.optString("ProductName");
								_productDescription = _array.optJSONObject(0)
										.optString("productDescription");
								_AsLowAsPrice = _array.optJSONObject(0)
										.optString("AsLowAsPrice");
								_MultimediaContent = _array.optJSONObject(0)
										.optString("MultimediaContent");
								_ItemColors = _array.optJSONObject(0)
										.optString("ItemColors");
								_ImageUrl = _array.optJSONObject(0).optString(
										"ImageUrl");
								spinnerValues = _array.optJSONObject(0)
										.optString("ItemColors").split("\\|");

							}
							JSONArray _ChargesArray = _obj
									.optJSONArray("Charges");
							if (_ChargesArray != null
									&& _ChargesArray.length() > 0) {
								_chargeList = new ArrayList<ProductDetailModel>();
								for (int i = 0; i < _ChargesArray.length(); i++) {
									JSONObject _chargeObj = _ChargesArray
											.optJSONObject(i);
									ProductDetailModel _model = new ProductDetailModel();
									_model.setName(_chargeObj
											.optString("chargeName"));
									_model.setText(_chargeObj
											.optString("chargeText"));
									_chargeList.add(_model);
								}
							}
							JSONArray _DetailsArray = _obj
									.optJSONArray("Details");
							if (_DetailsArray != null
									&& _DetailsArray.length() > 0) {
								_DetailsList = new ArrayList<ProductDetailModel>();
								for (int j = 0; j < _DetailsArray.length(); j++) {
									JSONObject _detailsObj = _DetailsArray
											.optJSONObject(j);
									ProductDetailModel _model1 = new ProductDetailModel();
									String _Name = _detailsObj
											.optString("detailName");
									String _Text = _detailsObj
											.optString("detailText");
									_model1.setName(_Name);
									_model1.setText(_Text);
									_DetailsList.add(_model1);
								}
							}
							JSONArray _OptionsArray = _obj
									.optJSONArray("Options");
							if (_OptionsArray != null
									&& _OptionsArray.length() > 0) {
								_OptionsList = new ArrayList<ProductDetailModel>();
								for (int k = 0; k < _OptionsArray.length(); k++) {
									JSONObject _optionObj = _OptionsArray
											.optJSONObject(k);
									ProductDetailModel _model2 = new ProductDetailModel();
									String _optionName = _optionObj
											.optString("optionName");
									String _optiontext = _optionObj
											.optString("optiontext");
									_model2.setName(_optionName);
									_model2.setText(_optiontext);
									_OptionsList.add(_model2);
								}
							}

							// PRICING DATA
							int stdPrices = 0, splPrices = 0, newPrices = 0;
							prices = _obj.optJSONArray("Pricing");
							if (prices != null && prices.length() > 0) {
								stdPrices = Integer.valueOf(prices
										.getJSONObject(0)
										.getString("RowCount1"));
								splPrices = Integer.valueOf(prices
										.getJSONObject(0)
										.getString("RowCount2"));
								newPrices = Integer.valueOf(prices
										.getJSONObject(0)
										.getString("RowCount3"));
								System.out.println(stdPrices + " " + splPrices
										+ " " + newPrices);
							}
							if (stdPrices > 0) {
								stdQuants = new String[stdPrices];
								stdUS = new String[stdPrices];
								stdCA = new String[stdPrices];
								JSONArray stdPrice = _obj
										.optJSONArray("PriceData1");
								for (int i = 0; i < stdPrices; i++) {
									JSONObject ob = stdPrice.optJSONObject(i);
									stdCode = ob.getString("discountCode");
									stdQuants[i] = ob.getString("Quantity");
									stdUS[i] = ob.getString("USPrice");
									stdCA[i] = ob.getString("CanadianPrice");
									prodNote = ob.getString("noteText");
								}
								if (splPrices > 0) {
									splQuants = new String[splPrices];
									splUS = new String[splPrices];
									splCA = new String[splPrices];
									JSONArray splPrice = _obj
											.getJSONArray("PriceData2");
									for (int i = 0; i < splPrices; i++) {
										JSONObject ob = splPrice
												.optJSONObject(i);
										splCode = ob.getString("discountCode");
										splQuants[i] = ob.getString("Quantity");
										splUS[i] = ob.getString("USPrice");
										splCA[i] = ob
												.getString("CanadianPrice");
										splExpDate = ob.getString(
												"expirationDate").split(" ")[0];
									}
								}
							}
							if (newPrices > 0) {
								newQuants = new String[newPrices];
								newUS = new String[newPrices];
								newCA = new String[newPrices];
								JSONArray newPrice = _obj
										.optJSONArray("PriceData3");
								for (int i = 0; i < newPrices; i++) {
									JSONObject ob = newPrice.optJSONObject(i);
									newCode = ob.getString("discountCode");
									newQuants[i] = ob.getString("Quantity");
									newUS[i] = ob.getString("USPrice");
									newCA[i] = ob.getString("CanadianPrice");
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						_dialog.dismiss();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				_dialog.dismiss();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (_MultimediaContent.equals("")) {
					btnVideo.setVisibility(View.GONE);
				} else {
					btnVideo.setVisibility(View.VISIBLE);
				}

				tvProductCode.setText(_ProductCode);
				tvProductName.setText(_ProductName);
				//tvPrice.setText(_AsLowAsPrice);
				final DecimalFormat df = new DecimalFormat();
				df.setMinimumFractionDigits(2);
				tvPrice.setText("$"+df.format(Float.valueOf(_AsLowAsPrice.substring(1))));
				df.setMaximumFractionDigits(3);
				if (stdQuants != null) {
					showDetailsPrices(1);
				} else {
					pricingLayout.setVisibility(GONE);
					// pricingLayoutQ.setVisibility(GONE);
				}
				if (_productDescription.equals("")) {
					tvFeatures.setVisibility(GONE);
				} else {
					tvFeatures.setVisibility(VISIBLE);
				}
				aq.id(prodImg).image(_ImageUrl);
				_setInfoData(_DetailsList);
				
				// setListViewHeightBasedOnChildren(infoList);
			} catch (Exception e) {
				e.printStackTrace();
			}
			_dialog.dismiss();
			//scrollView.scrollTo(0, 0);
			//scrollView.pageScroll(View.FOCUS_UP);
			scrollView.smoothScrollBy(0, 0);    
			
		}
	}

	/******************************* sets data on the listview ******************************************/
	private void _setInfoData(ArrayList<ProductDetailModel> _data) {
		ProductDetailAdapter detailAdapter = new ProductDetailAdapter(getActivity(), _data);
		infoList.setAdapter(detailAdapter);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txtDetails:
			infoList.setVisibility(View.VISIBLE);
			txtProdDesc.setVisibility(View.GONE);
			_setInfoData(_DetailsList);
			break;
		case R.id.txtOptinns:
			infoList.setVisibility(View.VISIBLE);
			txtProdDesc.setVisibility(View.GONE);
			_setInfoData(_OptionsList);
			break;
		case R.id.txtCharges:
			infoList.setVisibility(View.VISIBLE);
			txtProdDesc.setVisibility(View.GONE);
			_setInfoData(_chargeList);
			break;
		case R.id.txtFeatures:
			infoList.setVisibility(View.GONE);
			txtProdDesc.setVisibility(View.VISIBLE);
			txtProdDesc.setText(Html.fromHtml(_productDescription));
			break;
		case R.id.two:
			DataHolderClass.getInstance().set_pId(productId);
			Bundle samplebundle = new Bundle();
			samplebundle.putStringArray("SPINNER_VALUE", spinnerValues);
			samplebundle.putString("PRODUCT_CODE", productId);
			((HomeMob) getActivity()).letsHaveSomeTransactions(R.id.container,
					new MobileReqSample(), "RequestSample", samplebundle);
			break;
		case R.id.three:
			DataHolderClass.getInstance().set_pId(productId);
			Bundle quotebundle = new Bundle();
			quotebundle.putStringArray("SPINNER_VALUE", spinnerValues);
			quotebundle.putString("RESPONSE_DATA", _result);
			((HomeMob) getActivity()).letsHaveSomeTransactions(R.id.container,
					new MobileReqQuote(), "RequestQuote", quotebundle);
			break;
		case R.id.four:
			DataHolderClass.getInstance().set_pId(productId);
			((HomeMob) getActivity()).letsHaveSomeTransactions(R.id.container,
					new MobileInventory(), "", null);
			break;
		case R.id.five:
			DataHolderClass.getInstance().set_pId(productId);
			Bundle frieghtbundle = new Bundle();
			frieghtbundle.putString("PRODUCT_CODE", productId);
			((HomeMob) getActivity()).letsHaveSomeTransactions(R.id.container,
					new MobileFreight(), "", frieghtbundle);
			break;
		case R.id.imgUSA:
			showDetailsPrices(1);
			break;
		case R.id.imgCA:
			showDetailsPrices(2);
			break;
		case R.id.playVideo:
			try {
				DataHolderClass.getInstance().set_videoId(_MultimediaContent);
				ProductScreen.fromScan = false;

				// TODO Update this code if the new YouTubeAPI.jar is available
				// with fixed issue for implicit intent.
				if (android.os.Build.VERSION.SDK_INT >= 21) {
					Intent intent = YouTubeStandalonePlayer.createVideoIntent(
							getActivity(), CommonData.YOUTUBE_API_KEY,
							_MultimediaContent);
					startActivity(intent);
				} else {

					startActivity(new Intent(getActivity(),
							PlayVideoScreen.class));
				}
				getActivity().overridePendingTransition(
						R.anim.enter_new_screen, R.anim.exit_old_screen);

			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(), "Error! Can't Play this video",
						Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.sendEmail:
			new SendEmail(getActivity(), productId).show();
			break;
		default:
			break;
		}
	}

	private void showDetailsPrices(int i) {
		if (prodNote.equals("")) {
			noteslayout.setVisibility(GONE);
		} else {
			note.setText(prodNote);
			noteslayout.setVisibility(VISIBLE);
		}
		switch (stdQuants.length) {
		case 5:
			q5.setText(stdQuants[4]);
			
		case 4:
			if (q5.getText().toString().equalsIgnoreCase("")) {
				q5.setVisibility(View.GONE);
			}
			q4.setText(stdQuants[3]);
		case 3:
			q3.setText(stdQuants[2]);
		case 2:
			q2.setText(stdQuants[1]);
		case 1:
			q1.setText(stdQuants[0]);
		}
		if (splUS != null) {
			spclLine.setVisibility(VISIBLE);
			txtExpires.setVisibility(VISIBLE);
			st1.setPaintFlags(st1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			st2.setPaintFlags(st1.getPaintFlags());
			st3.setPaintFlags(st1.getPaintFlags());
			st4.setPaintFlags(st1.getPaintFlags());
			st5.setPaintFlags(st1.getPaintFlags());
			txtStandard.setPaintFlags(st1.getPaintFlags());
			txtExpires.setText("Expires: " + splExpDate);
		} else {
			spclLine.setVisibility(GONE);
			txtExpires.setVisibility(GONE);
		}
		if (i == 1) { // US pricing
			arrowUS.setVisibility(VISIBLE);
			arrowCA.setVisibility(GONE);
			txtPricing.setText("Pricing, US Dollars");
			dc1.setText(stdCode);
			switch (stdUS.length) {
			case 5:
				st5.setText("$"+df.format(Float.valueOf(stdUS[4])));
				
			case 4:
				if (st5.getText().toString().equalsIgnoreCase("")) {
					st5.setVisibility(View.GONE);
				}
				st4.setText("$"+df.format(Float.valueOf(stdUS[3])));
			case 3:
				st3.setText("$"+df.format(Float.valueOf(stdUS[2])));
			case 2:
				st2.setText("$"+df.format(Float.valueOf(stdUS[1])));
			case 1:
				st1.setText("$"+df.format(Float.valueOf(stdUS[0])));
			}
			if (splUS != null) {
				dc2.setText(splCode);
				switch (splUS.length) {
				case 5:
					sp5.setText("$"+df.format(Float.valueOf(splUS[4])));
					
				case 4:
					if (sp5.getText().toString().equalsIgnoreCase("")) {
						sp5.setVisibility(View.GONE);
					}
					sp4.setText("$"+df.format(Float.valueOf(splUS[3])));
				case 3:
					sp3.setText("$"+df.format(Float.valueOf(splUS[2])));
				case 2:
					sp2.setText("$"+df.format(Float.valueOf(splUS[1])));
				case 1:
					sp1.setText("$"+df.format(Float.valueOf(splUS[0])));
				}
			}

		} else if (i == 2) { // CA pricing
			arrowUS.setVisibility(GONE);
			arrowCA.setVisibility(VISIBLE);
			txtPricing.setText("Pricing, Canadian Dollars");
			switch (stdCA.length) {
			case 5:
				st5.setText("$"+df.format(Float.valueOf(stdCA[4])));
				
			case 4:
				if (st5.getText().toString().equalsIgnoreCase("")) {
					st5.setVisibility(View.GONE);
				}
				st4.setText("$"+df.format(Float.valueOf(stdCA[3])));
			case 3:
				st3.setText("$"+df.format(Float.valueOf(stdCA[2])));
			case 2:
				st2.setText("$"+df.format(Float.valueOf(stdCA[1])));
			case 1:
				st1.setText("$"+df.format(Float.valueOf(stdCA[0])));
			}
			if (splCA != null) {
				switch (splCA.length) {
				case 5:
					sp5.setText("$"+df.format(Float.valueOf(splCA[4])));
					
				case 4:
					if (sp5.getText().toString().equalsIgnoreCase("")) {
						sp5.setVisibility(View.GONE);
					}
					sp4.setText("$"+df.format(Float.valueOf(splCA[3])));
				case 3:
					sp3.setText("$"+df.format(Float.valueOf(splCA[2])));
				case 2:
					sp2.setText("$"+df.format(Float.valueOf(splCA[1])));
				case 1:
					sp1.setText("$"+df.format(Float.valueOf(splCA[0])));
				}
			}

		}
	}
}
