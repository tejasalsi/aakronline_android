package com.datamodel;

public class RepData {

	String SalesRepId, stateId, stateName, RegionId, RegionName, RFName,
			RLName, IFName, ILName, REmail, IEmail, RPhoneNo, IPhoneNo,
			RDesignation, IDesignation, Rimage, Iimage;

	public String getSalesRepId() {
		return SalesRepId;
	}

	public void setSalesRepId(String salesRepId) {
		SalesRepId = salesRepId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getRegionId() {
		return RegionId;
	}

	public void setRegionId(String regionId) {
		RegionId = regionId;
	}

	public String getRegionName() {
		return RegionName;
	}

	public void setRegionName(String regionName) {
		RegionName = regionName;
	}

	public String getRFName() {
		return RFName;
	}

	public void setRFName(String rFName) {
		RFName = rFName;
	}

	public String getRLName() {
		return RLName;
	}

	public void setRLName(String rLName) {
		RLName = rLName;
	}

	public String getIFName() {
		return IFName;
	}

	public void setIFName(String iFName) {
		IFName = iFName;
	}

	public String getILName() {
		return ILName;
	}

	public void setILName(String iLName) {
		ILName = iLName;
	}

	public String getREmail() {
		return REmail;
	}

	public void setREmail(String rEmail) {
		REmail = rEmail;
	}

	public String getIEmail() {
		return IEmail;
	}

	public void setIEmail(String iEmail) {
		IEmail = iEmail;
	}

	public String getRPhoneNo() {
		return RPhoneNo;
	}

	public void setRPhoneNo(String rPhoneNo) {
		RPhoneNo = rPhoneNo;
	}

	public String getIPhoneNo() {
		return IPhoneNo;
	}

	public void setIPhoneNo(String iPhoneNo) {
		IPhoneNo = iPhoneNo;
	}

	public String getRDesignation() {
		return RDesignation;
	}

	public void setRDesignation(String rDesignation) {
		RDesignation = rDesignation;
	}

	public String getIDesignation() {
		return IDesignation;
	}

	public void setIDesignation(String iDesignation) {
		IDesignation = iDesignation;
	}

	public String getRimage() {
		return Rimage;
	}

	public void setRimage(String rimage) {
		Rimage = rimage;
	}

	public String getIimage() {
		return Iimage;
	}

	public void setIimage(String iimage) {
		Iimage = iimage;
	}

}
