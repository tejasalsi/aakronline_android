package com.akronline.mob;

import static com.akronline.dialogs.RepMeeting.salesRepData;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.R;
import com.datamodel.RepData;
import com.progressbar.GoogleProgressDialog;

/*
 *  Fragment for Request webinar 
 */
public class RequestWebinarFragment extends BaseFragment implements OnClickListener {
	
	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
	private static final String NAMESPACE = "http://tempuri.org/";
	private Context ctx;
	String userID, repID;
	EditText etNotes;
	TextView txtName1, txtName2, txtDetails1, txtDetails2;
	Button btnSubmit, btnCancel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View _view = inflater.inflate(R.layout.req_webinar, null);
		Log.e("tag", "RequestWebinarFragment");
	
		SharedPreferences prefs = getActivity().getApplicationContext()
				.getSharedPreferences("LoginPref", 0);
		userID = prefs.getString("USER-ID", "0");
		repID = prefs.getString("REP-ID", "0");

		etNotes = (EditText) _view.findViewById(R.id.etNotes);
		txtName1 = (TextView) _view.findViewById(R.id.txtName1);
		txtName2 = (TextView) _view.findViewById(R.id.txtName2);
		txtDetails1 = (TextView) _view.findViewById(R.id.txtDetails1);
		txtDetails2 = (TextView) _view.findViewById(R.id.txtDetails2);
		btnSubmit = (Button) _view.findViewById(R.id.btnSubmit);
		btnCancel = (Button) _view.findViewById(R.id.btnCancel);

		btnCancel.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
	
		return _view;
	}


	@Override
	public void onStart() {
		super.onStart();
		// TODO Auto-generated method stub
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		if (salesRepData.isEmpty()) {
			new GetRepData().execute();
		} else {
			setData();
		}
	}
	
	private void setData() {
		try {
			for (RepData repData : salesRepData)
				if (repData.getSalesRepId().equals(repID)) {
					txtName1.setText(repData.getRFName() + " "
							+ repData.getRLName());
					txtDetails1.setText(repData.getRDesignation() + "\n"
							+ repData.getREmail() + "\nPh: "
							+ repData.getRPhoneNo());
					txtName2.setText(repData.getIFName() + " "
							+ repData.getILName());
					txtDetails2.setText(repData.getIDesignation() + "\n"
							+ repData.getIEmail() + "\nPh: "
							+ repData.getIPhoneNo());
					break;
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
//			String title = DataHolderClass.getInstance().get_titleCatagory();
			((HomeMob) getActivity()).setTitle("Request Webinar");
		} catch (Exception e) {
			e.printStackTrace();
		}
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.search.setVisibility(View.GONE);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnCancel:
				getActivity().onBackPressed();
				break;
			case R.id.btnSubmit:
				new SubmitWebinar().execute();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception: " + e.getMessage());
		}
	}
	
	private void showToast(String msg) {
		Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG)
				.show();
	}

	private class GetRepData extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "GetSalesRepData";
		private static final String SOAP_ACTION = "http://tempuri.org/GetSalesRepData";
		JSONObject ob;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			txtName1.setText(Html
					.fromHtml("<font color=red>Please wait...</font>"));
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					ob = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result != null) {
				showToast(result);
				getActivity().onBackPressed();
				return;
			}
			try {
				JSONArray arr = ob.getJSONArray("Data");
				salesRepData.clear();
				for (int i = 0; i < arr.length(); i++) {
					JSONObject rep = arr.getJSONObject(i);
					RepData repData = new RepData();
					repData.setSalesRepId(rep.getString("SalesRepId"));
					repData.setRegionId(rep.getString("RegionId"));
					repData.setRFName(rep.getString("RFName"));
					repData.setRLName(rep.getString("RLName"));
					repData.setIFName(rep.getString("IFName"));
					repData.setILName(rep.getString("ILName"));
					repData.setREmail(rep.getString("REmail"));
					repData.setIEmail(rep.getString("IEmail"));
					repData.setRPhoneNo(rep.getString("RPhoneNo"));
					repData.setIPhoneNo(rep.getString("IPhoneNo"));
					repData.setRDesignation(rep.getString("RDesignation"));
					repData.setIDesignation(rep.getString("IDesignation"));
					repData.setRimage(rep.getString("Rimage"));
					repData.setIimage(rep.getString("Iimage"));
					repData.setRegionName(rep.getString("RegionName"));
					repData.setStateId(rep.getString("stateId"));
					repData.setStateName(rep.getString("stateName"));
					salesRepData.add(repData);
				}
				setData();

			} catch (Exception e) {
				e.printStackTrace();
				showToast("Exception: " + e.getMessage());
				getActivity().onBackPressed();
			}
		}

	}

	private class SubmitWebinar extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "RequestWebinar";
		private static final String SOAP_ACTION = "http://tempuri.org/RequestWebinar";
		GoogleProgressDialog dialog;
		String response = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("Notes", etNotes.getText().toString());
			request.addProperty("SalesRepId", repID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					response = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null) {
				showToast(result);
			} else if (response.equals("0")) {
				// success try to close dialog
				showToast("Request submitted successfully");
				getActivity().onBackPressed();
			} else {
				showToast("Request not submitted");
			}
		}

	}

}
