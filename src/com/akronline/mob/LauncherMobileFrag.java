package com.akronline.mob;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.ProductScreen;
import com.akronline.R;
import com.dataholder.DataHolderClass;
import com.internet.ConnectionDetector;

@SuppressLint("InflateParams")
public class LauncherMobileFrag extends Fragment implements OnClickListener {
	ImageView shooot_mobile, snacks_mobile, new_products_mobile,
			mood_products_mobile, best_seller_mobile;

	Boolean _isInternetPresent = false;
	ConnectionDetector _cd;
	SharedPreferences prefs;
	String emailId;
	String userId;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View _view = inflater.inflate(R.layout.launcher_fragmnet, null);
		Log.e("tag", "LauncherMobileFrag");
		try {
			prefs = getActivity().getSharedPreferences("LoginPref", 0);
			emailId = prefs.getString("USER-EMAIL", "");
			userId = prefs.getString("USER-ID", "");

			shooot_mobile = (ImageView) _view.findViewById(R.id.pointnshoot);
			shooot_mobile.setOnClickListener(this);

			snacks_mobile = (ImageView) _view.findViewById(R.id.snacks_mobile);
			snacks_mobile.setOnClickListener(this);

			new_products_mobile = (ImageView) _view
					.findViewById(R.id.new_products_mobile);
			new_products_mobile.setOnClickListener(this);

			mood_products_mobile = (ImageView) _view
					.findViewById(R.id.mood_products_mobile);
			mood_products_mobile.setOnClickListener(this);

			best_seller_mobile = (ImageView) _view
					.findViewById(R.id.best_seller_mobile);
			best_seller_mobile.setOnClickListener(this);

			_cd = new ConnectionDetector(getActivity());
			_isInternetPresent = _cd.isConnectingToInternet();
			
			
			if (userId.equals("")) {
				startActivity(new Intent(getActivity(),
						LoginMobile.class));
				getActivity().overridePendingTransition(
						R.anim.push_out_to_bottom, 0);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return _view;

	}

	@Override
	public void onClick(View v) {
		ProductScreen.fromScan = false;
		switch (v.getId()) {
		case R.id.pointnshoot:
			try {
				if (userId.equals("")) {
					showToast("Please Login first");
					return;
				}
				if (_isInternetPresent) {
					SharedPreferences preferences = getActivity().getSharedPreferences("app",
							android.content.Context.MODE_PRIVATE);
					boolean isFromMobileProductListing = preferences.getBoolean(
							"isFromMobileProductList", false);
					
					if (isFromMobileProductListing) {
						SharedPreferences.Editor _editor = preferences.edit();
						_editor.putBoolean("isFromMobileProductList", false);
						_editor.commit();
						getActivity().finish();
						
					} else {
						startActivity(new Intent(getActivity(), SyncMob.class));
						getActivity().overridePendingTransition(
								R.anim.enter_new_screen, R.anim.exit_old_screen);
					}
					
//					startActivity(new Intent(getActivity(), SyncMob.class));
//					getActivity().overridePendingTransition(
//							R.anim.enter_new_screen, R.anim.exit_old_screen);
				} else {
					showToast("no internet");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		case R.id.snacks_mobile:
			try {
				if (_isInternetPresent) {
					if (emailId != null && userId != null
							&& emailId.length() > 0 && userId.length() > 0) {
						DataHolderClass.getInstance().set_filterBy("Snacks");
						DataHolderClass.getInstance().set_titleCatagory(
								"SNACKS");
						DataHolderClass.getInstance()
								.set_catagoryName("snacks");
						isFromScanToMobileProductList(false);
						MobileProductListing _listing = new MobileProductListing(false);
						((HomeMob) getActivity()).letsHaveSomeTransactions(
								R.id.container, _listing, "Listing",null);
					} else {
						startActivity(new Intent(getActivity(),
								LoginMobile.class));
						getActivity().overridePendingTransition(
								R.anim.push_out_to_bottom, 0);
					}
				} else {

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		case R.id.new_products_mobile:
			try {
				if (_isInternetPresent) {
					if (emailId != null && userId != null
							&& emailId.length() > 0 && userId.length() > 0) {
						DataHolderClass.getInstance().set_filterBy(
								"New Products");
						DataHolderClass.getInstance().set_titleCatagory(
								"NEW PRODUCTS");
						DataHolderClass.getInstance().set_catagoryName(
								"newproducts");
						isFromScanToMobileProductList(false);
						MobileProductListing _listing = new MobileProductListing(false);
						((HomeMob) getActivity()).letsHaveSomeTransactions(
								R.id.container, _listing, "Listing",null);
					} else {
						startActivity(new Intent(getActivity(),
								LoginMobile.class));
						getActivity().overridePendingTransition(
								R.anim.push_out_to_bottom, 0);
					}
				} else {

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		case R.id.mood_products_mobile:
			try {
				if (_isInternetPresent) {
					if (emailId != null && userId != null
							&& emailId.length() > 0 && userId.length() > 0) {
						DataHolderClass.getInstance().set_filterBy(
								"Mood Products");
						DataHolderClass.getInstance().set_titleCatagory(
								"MOOD PRODUCTS");
						DataHolderClass.getInstance().set_catagoryName(
								"moodproducts");
						isFromScanToMobileProductList(false);
						MobileProductListing _listing = new MobileProductListing(false);
						((HomeMob) getActivity()).letsHaveSomeTransactions(
								R.id.container, _listing, "Listing",null);
					} else {
						startActivity(new Intent(getActivity(),
								LoginMobile.class));
						getActivity().overridePendingTransition(
								R.anim.push_out_to_bottom, 0);
					}
				} else {

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		case R.id.best_seller_mobile:
			try {
				if (_isInternetPresent) {
					if (emailId != null && userId != null
							&& emailId.length() > 0 && userId.length() > 0) {
						DataHolderClass.getInstance().set_filterBy(
								"Best Seller");
						DataHolderClass.getInstance().set_titleCatagory(
								"BEST SELLERS");
						DataHolderClass.getInstance().set_catagoryName(
								"bestsellers");
						isFromScanToMobileProductList(false);
						MobileProductListing _listing = new MobileProductListing(false);
						((HomeMob) getActivity()).letsHaveSomeTransactions(
								R.id.container, _listing, "Listing",null);
					} else {
						startActivity(new Intent(getActivity(),
								LoginMobile.class));
						getActivity().overridePendingTransition(
								R.anim.push_out_to_bottom, 0);
					}
				} else {

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		default:
			break;
		}
	}

	private void isFromScanToMobileProductList(boolean b) {
		SharedPreferences preferences = getActivity().getSharedPreferences(
				"app", android.content.Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean("isFromMobileProductList", b);
		editor.commit();		
	}

	@Override
	public void onResume() {
		super.onResume();
		try {
			prefs = getActivity().getSharedPreferences("LoginPref", 0);
			emailId = prefs.getString("USER-EMAIL", emailId);
			userId = prefs.getString("USER-ID", userId);
			ImageView logo = (ImageView) getActivity().getActionBar()
					.getCustomView().findViewById(R.id.app_icon);
			TextView txtTitle = (TextView) getActivity().getActionBar()
					.getCustomView().findViewById(R.id.title);
			Button btnBack = (Button) getActivity().getActionBar()
					.getCustomView().findViewById(R.id.back_mobile);
			txtTitle.setVisibility(View.GONE);
			btnBack.setVisibility(View.GONE);
			logo.setVisibility(View.VISIBLE);

		} catch (Exception e) {
			e.printStackTrace();
		}
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.search.setVisibility(View.GONE);
	}

	private void showToast(String msg) {
		Toast.makeText(getActivity().getApplicationContext(), msg,
				Toast.LENGTH_LONG).show();
	}
}
