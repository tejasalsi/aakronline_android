package com.akronline.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.akronline.R;
import com.akronline.interfaces.DialogCallback;

public class MarketChooser extends Dialog {

	Button btnDone;
	CheckBox cbFB, cbTWT, cbIG, cbPI, cbCS, cbGP, cbLI, cbCon, cbFNB;
	EditText etOther;

	public MarketChooser(Context ctx, final DialogCallback callback,
			final int reqCode) {
		super(ctx);
		final StringBuilder sb = new StringBuilder();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.market_choose);
		getWindow().getDecorView().setBackgroundResource(R.drawable.white_bg);
		cbFB = (CheckBox) findViewById(R.id.cbFB);
		cbTWT = (CheckBox) findViewById(R.id.cbTWT);
		cbIG = (CheckBox) findViewById(R.id.cbIG);
		cbPI = (CheckBox) findViewById(R.id.cbPI);
		cbCS = (CheckBox) findViewById(R.id.cbCS);
		cbGP = (CheckBox) findViewById(R.id.cbGP);
		cbLI = (CheckBox) findViewById(R.id.cbLI);
		cbCon = (CheckBox) findViewById(R.id.cbCon);
		cbFNB = (CheckBox) findViewById(R.id.cbFNB);
		etOther = (EditText) findViewById(R.id.etOther);
		btnDone = (Button) findViewById(R.id.btnDone);

		btnDone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (cbFB.isChecked())
					sb.append("Healthcare,");
				if (cbTWT.isChecked())
					sb.append("Education,");
				if (cbIG.isChecked())
					sb.append("Police/Fire/Rescue,");
				if (cbPI.isChecked())
					sb.append("Charitable Organizations,");
				if (cbCS.isChecked())
					sb.append("Entertainment,");
				if (cbGP.isChecked())
					sb.append("Financial Services,");
				if (cbLI.isChecked())
					sb.append("Manufacturing,");
				if (cbCon.isChecked())
					sb.append("Construction,");
				if (cbFNB.isChecked())
					sb.append("Food & Beverage,");
				if (!etOther.getText().toString().isEmpty()) {
					sb.append(etOther.getText().toString());
				}

				String result = sb.toString();
				if (result.endsWith(","))
					result = result.substring(0, result.length() - 1);
				callback.onDialogSubmit(reqCode, result);
				MarketChooser.this.dismiss();
			}
		});

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

}
