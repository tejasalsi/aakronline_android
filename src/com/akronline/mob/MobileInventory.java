package com.akronline.mob;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.adapters.InventoryAdapter;
import com.akronline.R;
import com.dataholder.DataHolderClass;
import com.datamodel.InventoryModel;
import com.progressbar.GoogleProgressDialog;

public class MobileInventory extends Fragment {

	private static String NAMESPACE = "http://tempuri.org/";
	private static String CHECK_INVENTORY_METHOD = "GetInventoryDetails";
	private static String CHECK_INVENTORY_SOAP_ACTION = "http://tempuri.org/GetInventoryDetails";
	private static String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	ListView inventoryList;
	TextView txtPart;
	ArrayList<InventoryModel> invenList;
	LinearLayout inventoryTableHolder;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.check_inventory_screen, container,
				false);
		Log.e("tag", "MobileInventory");
		inventoryTableHolder = (LinearLayout) v.findViewById(R.id.inventoryTable);
		inventoryList = (ListView) v.findViewById(R.id.inventoryList);
		txtPart = (TextView) v.findViewById(R.id.txtPart);
		new CheckInventory(DataHolderClass.getInstance().get_pId()).execute();
		return v;
	}

	/************************ CHECK INVENTORY WORK *************************************/
	private class CheckInventory extends AsyncTask<String, String, String> {
		String _code;
		String _part;
		String errorMessage = "";
		GoogleProgressDialog _dialog;

		public CheckInventory(String code) {
			this._code = code;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			_dialog = new GoogleProgressDialog(getActivity());
			_dialog.setCancelable(false);
			_dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				invenList = new ArrayList<InventoryModel>();
				SoapObject request = new SoapObject(NAMESPACE,
						CHECK_INVENTORY_METHOD);
				request.addProperty("ProductCode", _code);
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				try {
					HttpTransportSE androidHttpTransport = new HttpTransportSE(
							URL);
					androidHttpTransport.call(CHECK_INVENTORY_SOAP_ACTION,
							envelope);
					SoapObject result = (SoapObject) envelope.bodyIn;
					if (result != null) {
						String _data = result.getProperty(0).toString();
						JSONObject _obj = new JSONObject(_data);
						if (_obj != null) {
							JSONArray _jarray = _obj.optJSONArray("Response");
							if (_jarray != null & _jarray.length() > 0) {
								String _responseCode = _jarray.optJSONObject(0)
										.optString("ResponseCode");
								if (_responseCode.equalsIgnoreCase("0")) {
									JSONArray _array = _obj
											.optJSONArray("Details");
									for (int i = 0; i < _array.length(); i++) {
										JSONObject obj = _array
												.optJSONObject(i);
										InventoryModel _model = new InventoryModel();
										_model.setColorName(obj
												.optString("ColorName"));
										_model.setInventoryAvailable(obj
												.optString("InventoryAvailable"));
										invenList.add(_model);
									}
									JSONArray _jrray = _obj
											.optJSONArray("Inventory");
									_part = _jrray.optJSONObject(0).optString(
											"Part");

								} else if (_responseCode.equalsIgnoreCase("1")) {

								} else if (_responseCode.equalsIgnoreCase("2")) {

									_part = _jarray.optJSONObject(0).optString(
											"msg");

								}

							}
						}
					} else {
						System.out.println("error");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				txtPart.setText((Html.fromHtml("<b>Part:</b>" + _part)));
				if(invenList.size()>0){
				
				InventoryAdapter _adap = new InventoryAdapter(getActivity(),
						invenList);
				_adap.notifyDataSetChanged();
				inventoryList.setAdapter(_adap);
				}else{
					
					inventoryTableHolder.setVisibility(View.GONE);
					
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			_dialog.dismiss();
		}
	}
}
