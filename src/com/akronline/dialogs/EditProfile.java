package com.akronline.dialogs;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.R;
import com.progressbar.GoogleProgressDialog;

public class EditProfile extends Dialog implements
		android.view.View.OnClickListener {

	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
	private static final String NAMESPACE = "http://tempuri.org/";

	Context ctx;
	Button btnSubmit, btnCancel;
	CheckBox cbMail;
	TextView email, fname, last, comp, phone, addr, city, state, cntry, zip;
	EditText etEmail, etFName, etMName, etLName, etComp, etPhone, etFax,
			etAddress, etCity, etState, etZip, etASI, etUPIC, etPPI, etSAGE;
	Spinner spinCountry, spinState;
	FrameLayout stateContainer;
	String userID, repID;
	ScrollView scrollView;
	TextView txtLoading;
	private ArrayList<String> countries = new ArrayList<String>();
	private ArrayList<String> countryIds = new ArrayList<String>();
	private Map<String, String> countryIdMap = new HashMap<String, String> ();
	
	private ArrayList<String> states = new ArrayList<String>();
	private ArrayList<String> stateIds = new ArrayList<String>();
	private Map<String, String> stateIdMap = new HashMap<String, String> ();

	public EditProfile(Context ctx, String userID) {
		super(ctx);
		this.ctx = ctx;
		this.userID = userID;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.edit_profile);
		getWindow().getDecorView().setBackgroundResource(R.drawable.white_bg);

		repID = ctx.getApplicationContext()
				.getSharedPreferences("LoginPref", 0).getString("REP-ID", "0");
		scrollView = (ScrollView) findViewById(R.id.scroll);
		email = (TextView) findViewById(R.id.email);
		fname = (TextView) findViewById(R.id.fName);
		last = (TextView) findViewById(R.id.lName);
		comp = (TextView) findViewById(R.id.compName);
		phone = (TextView) findViewById(R.id.phone);
		addr = (TextView) findViewById(R.id.address);
		city = (TextView) findViewById(R.id.city);
		state = (TextView) findViewById(R.id.state);
		cntry = (TextView) findViewById(R.id.country);
		zip = (TextView) findViewById(R.id.zip);
		txtLoading = (TextView) findViewById(R.id.txtWait);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		cbMail = (CheckBox) findViewById(R.id.cbMail);
		etEmail = (EditText) findViewById(R.id.etEmail);
		etFName = (EditText) findViewById(R.id.etFName);
		etLName = (EditText) findViewById(R.id.etLName);
		etMName = (EditText) findViewById(R.id.etMname);
		etComp = (EditText) findViewById(R.id.etComp);
		etPhone = (EditText) findViewById(R.id.etPhone);
		etFax = (EditText) findViewById(R.id.etFax);
		etAddress = (EditText) findViewById(R.id.etAddress);
		etCity = (EditText) findViewById(R.id.etCity);
		etState = (EditText) findViewById(R.id.etState);
		spinState = (Spinner) findViewById(R.id.spinState);
		stateContainer = (FrameLayout) findViewById(R.id.stateContainer);
		etZip = (EditText) findViewById(R.id.etZip);
		etASI = (EditText) findViewById(R.id.etASI);
		etUPIC = (EditText) findViewById(R.id.etUPIC);
		etPPI = (EditText) findViewById(R.id.etPPI);
		etSAGE = (EditText) findViewById(R.id.etSAGE);
		spinCountry = (Spinner) findViewById(R.id.spinCountry);

		email.setText(Html.fromHtml("Email Address <font color=red>*</font>"));
		fname.setText(Html.fromHtml("First Name <font color=red>*</font>"));
		last.setText(Html.fromHtml("Last Name <font color=red>*</font>"));
		comp.setText(Html.fromHtml("Company Name <font color=red>*</font>"));
		phone.setText(Html.fromHtml("Phone Number <font color=red>*</font>"));
		addr.setText(Html.fromHtml("Address <font color=red>*</font>"));
		city.setText(Html.fromHtml("City <font color=red>*</font>"));
		state.setText(Html.fromHtml("State <font color=red>*</font>"));
		cntry.setText(Html.fromHtml("Country <font color=red>*</font>"));
		zip.setText(Html.fromHtml("Zip Code <font color=red>*</font>"));

		spinCountry.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if (position == 0) {
					etState.setVisibility(View.GONE);
					stateContainer.setVisibility(View.VISIBLE);
				} else {
					etState.setVisibility(View.VISIBLE);
					stateContainer.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		btnSubmit.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		new GetProfileData().execute();
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnSubmit:
				if (!etEmail.getText().toString()
						.matches(Patterns.EMAIL_ADDRESS.pattern())) {
					showToast("Please enter valid Email");
					return;
				}
				if (etFName.getText().toString().isEmpty()) {
					showToast("First Name is required");
					return;
				}
				if (etLName.getText().toString().isEmpty()) {
					showToast("Last Name is required");
					return;
				}
				if (etComp.getText().toString().isEmpty()) {
					showToast("Company Name is required");
					return;
				}
				if (etPhone.getText().toString().isEmpty()) {
					showToast("Phone number is required");
					return;
				}
				if (!etPhone.getText().toString()
						.matches(Patterns.PHONE.pattern())) {
					showToast("Phone number is invalid");
					return;
				}
				if (!etFax.getText().toString().isEmpty()
						&& !etFax.getText().toString()
								.matches(Patterns.PHONE.pattern())) {
					showToast("Fax number is invalid");
					return;
				}
				if (etAddress.getText().toString().isEmpty()) {
					showToast("Address is required");
					return;
				}
				if (etCity.getText().toString().isEmpty()) {
					showToast("City is required");
					return;
				}
				if (etState.getVisibility() == View.VISIBLE
						&& etState.getText().toString().isEmpty()) {
					showToast("State is required");
					return;
				}
				if (etZip.getText().toString().isEmpty()) {
					showToast("Zip code is required");
					return;
				}
				if (etZip.getText().toString().length() < 5) {
					etZip.requestFocus();
					showToast("Zip code is invalid");
					return;
				}
				// all OK call service
				new StoreData().execute();

				break;
			case R.id.btnCancel:
				this.dismiss();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception :" + e.getLocalizedMessage());
		}
	}

	// private void scrollTo(EditText editText) {
	// // TODO fix scrolling
	// this.getWindow().setSoftInputMode(
	// WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	// // scrollView.scroll
	// }

	private void showToast(String msg) {
		Toast.makeText(ctx.getApplicationContext(), msg, Toast.LENGTH_LONG)
				.show();
	}

	private class GetProfileData1 extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "FetchProfileData";
		private static final String SOAP_ACTION = "http://tempuri.org/FetchProfileData";
		JSONObject obj;

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.d("result", result.toString());
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (result != null) {
				} else {
					JSONObject profile = obj.getJSONArray("ProfileData")
							.getJSONObject(0);
					if (profile.getString("ResponseCode").equals("0")) {
						repID = profile.getString("SalesRepId");
						SharedPreferences.Editor editor = ctx.getApplicationContext()
								.getSharedPreferences("LoginPref", 0).edit();
						editor.putString("REP-ID", repID);
						editor.commit();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				showToast("Error Occured: Unable to fetch details");
				txtLoading.setText("Error: Unable to fetch details!");
			}
			EditProfile.this.onBackPressed();
		}
	}
		
	
	private class GetProfileData extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "FetchProfileData";
		private static final String SOAP_ACTION = "http://tempuri.org/FetchProfileData";
		JSONObject obj;

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					obj = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
 			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (result != null) {
					showToast(result);
					txtLoading.setText(result);
				} else {
					JSONArray arr = obj.getJSONArray("Country");
					countries.clear();
					countryIds.clear();
					countryIdMap.clear();
					for (int i = 0; i < arr.length(); i++) {
						countries.add(arr.getJSONObject(i).getString("countryName"));
						countryIds.add(arr.getJSONObject(i).getString("countryId"));
						
					}
					Collections.reverse(countries);
					Collections.reverse(countryIds);
					int temp = 0;
					for(String countryID : countryIds){
						countryIdMap.put(countryID, temp+"");
						temp++;
						
					}
					
					arr = obj.getJSONArray("State");
					states.clear();
					stateIds.clear();
					stateIdMap.clear();
					
					for (int i = 0; i < arr.length(); i++) {
						states.add(arr.getJSONObject(i).getString("stateName"));
						stateIds.add(arr.getJSONObject(i).getString("stateId"));
						stateIdMap.put(arr.getJSONObject(i).getString("stateId"), i+"");
					}
					spinCountry.setAdapter(new ArrayAdapter<String>(ctx,
							android.R.layout.simple_list_item_1,
							android.R.id.text1, countries));
					spinState.setAdapter(new ArrayAdapter<String>(ctx,
							android.R.layout.simple_list_item_1,
							android.R.id.text1, states));

					JSONObject profile = obj.getJSONArray("ProfileData")
							.getJSONObject(0);
					if (profile.getString("ResponseCode").equals("0")) {
						etEmail.setText(profile.getString("email"));
						etFName.setText(profile.getString("firstName"));
						etMName.setText(profile.getString("middleName"));
						etLName.setText(profile.getString("lastName"));
						etComp.setText(profile.getString("companyName"));
						etPhone.setText(profile.getString("phone"));
						etFax.setText(profile.getString("fax"));
						etAddress.setText(profile.getString("address"));
						etCity.setText(profile.getString("city"));
						etState.setText(profile.getString("state"));
						etZip.setText(profile.getString("zip"));
						etASI.setText(profile.getString("asi"));
						etUPIC.setText(profile.getString("upic"));
						etPPI.setText(profile.getString("ppai"));
						etSAGE.setText(profile.getString("sage"));
						cbMail.setChecked(profile.getString("onlySubscription").equals("1")); 
						String stateID = "0";
						try {
							String _countryId = profile.getString("countryId");
							spinCountry.setSelection(Integer.parseInt(countryIdMap.get(_countryId)));
							stateID = profile.getString("StateId");
							
						} catch (NumberFormatException e) {
							e.printStackTrace();
							spinCountry.setSelection(0);
							stateID = "0";
						}
						
						if (Integer.parseInt(stateID) != 0) {
							etState.setVisibility(View.GONE);
							stateContainer.setVisibility(View.VISIBLE);
							spinState.setSelection(Integer.parseInt(stateIdMap.get(stateID)));
							
						} else {
							etState.setVisibility(View.VISIBLE);
							stateContainer.setVisibility(View.GONE);
						}

						repID = profile.getString("SalesRepId");
						SharedPreferences.Editor editor = ctx
								.getApplicationContext()
								.getSharedPreferences("LoginPref", 0).edit();
						editor.putString("REP-ID", repID);
						editor.commit();

						txtLoading.setVisibility(View.GONE);

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				showToast("Error Occured: Unable to fetch details");
				txtLoading.setText("Error: Unable to fetch details!");
			}
		}
	}

	private class StoreData extends AsyncTask<String, String, String> {

		private static final String METHOD_NAME = "SaveProfileData";
		private static final String SOAP_ACTION = "http://tempuri.org/SaveProfileData";
		GoogleProgressDialog dialog;
		String response;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(ctx);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... p) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("EmailId", etEmail.getText().toString());
			request.addProperty("FirstName", etFName.getText().toString()
					.trim());
			request.addProperty("MiddleName", etMName.getText().toString()
					.trim());
			request.addProperty("LastName", etLName.getText().toString().trim());
			request.addProperty("CompanyName", etComp.getText().toString()
					.trim());
			request.addProperty("Phone", etPhone.getText().toString().trim());
			request.addProperty("fax", etFax.getText().toString().trim());
			request.addProperty("address", etAddress.getText().toString()
					.trim());
			request.addProperty("city", etCity.getText().toString().trim());
			request.addProperty("countryId", countryIds.get(spinCountry.getSelectedItemPosition()));
			
			if (spinCountry.getSelectedItemPosition() == 0) {
				request.addProperty("state", spinState.getSelectedItem().toString());
				request.addProperty("stateid", stateIds.get(spinState.getSelectedItemPosition()));
				
			} else {
				request.addProperty("state", etState.getText().toString());
				request.addProperty("stateid", "0");
			}
			request.addProperty("zip", etZip.getText().toString().trim());
			request.addProperty("asi", etASI.getText().toString().trim());
			request.addProperty("upic", etUPIC.getText().toString().trim());
			request.addProperty("ppai", etPPI.getText().toString().trim());
			request.addProperty("sage", etSAGE.getText().toString().trim());
			request.addProperty("onlySubscription", cbMail.isChecked() ? "1": "0");

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					JSONObject obj = new JSONObject(data);
					if (obj.getJSONArray("ProfileData").getJSONObject(0)
							.getString("ResponseCode").equals("0")) {
						response = "0";
					} else {
						return "Error: Changes not saved";
					}
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Server responded an error";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null) {
				showToast(result);
			} else if (response.equals("0")) {
				// success try to close dialog
				new GetProfileData1().execute();
				showToast("Changes saved successfully");
				
			}
		}
	}

}
