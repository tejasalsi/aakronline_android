package com.akronline.mob;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.GetAds;
import com.akronline.R;
import com.akronline.dialogs.StateChooser;
import com.akronline.interfaces.AdsCallback;
import com.akronline.interfaces.DialogCallback;
import com.akronline.views.MyEditText;
import com.commondata.CommonData;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.dataholder.DataHolderClass;
import com.internet.ConnectionDetector;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class HomeMob extends FragmentActivity implements OnClickListener,
		AdsCallback, DialogCallback {

	SlidingMenu drawer;

	String emailId;

	String userId;
	// <<<<<<< HEAD
	Button menu, shoot, back, helpUs, reqWebinar, reqMeeting, requestCatalog,
			orderSample, editProfile, moodProducts, newProducts, bestSellers,
			snacksAkron, logout, searchByOrder;
	// =======
	//
	// Button menu, shoot, back, helpUs, reqWebinar, reqMeeting, requestCatalog,
	// orderSample, logout;
	//
	public static Button search;
	//
	// >>>>>>> 08c30060d0beed0afbb74dd084619cf8212a2226
	ImageView logo;

	public static TextView title;

	ActionBar actionBar;

	public static com.akronline.views.MyEditText searchField;

	boolean isInternet;

	ConnectionDetector connectionDetector;

	SharedPreferences prefs;

	boolean isLogin = false;

	String userID;

	String emaiID;

	String repID;

	SliderLayout slider;
	FragmentManager fragmentManager;
	Fragment launcherFrag;
	// Fragment frags;
	int i;

	public static Activity homeMob;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.launcher_mobile);
		Log.e("tag", "HomeMob ");
		homeMob = this;
		if (CommonData.scannedProduct == null) {
			launcherFrag = new LauncherMobileFrag();
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, launcherFrag, "launcher").commit();
		} else {
			if (CommonData.scannedProduct.getCatagory().equalsIgnoreCase(
					"New Products")) {

				DataHolderClass.getInstance().set_filterBy("New Products");
				DataHolderClass.getInstance().set_titleCatagory("NEW PRODUCTS");
				DataHolderClass.getInstance().set_catagoryName("newproducts");

			} else if (CommonData.scannedProduct.getCatagory()
					.equalsIgnoreCase("Snacks")) {
				DataHolderClass.getInstance().set_filterBy("Snacks");
				DataHolderClass.getInstance().set_titleCatagory("SNACKS");
				DataHolderClass.getInstance().set_catagoryName("snacks");

			} else if (CommonData.scannedProduct.getCatagory()
					.equalsIgnoreCase("Mood Products")) {
				DataHolderClass.getInstance().set_filterBy("Mood Products");
				DataHolderClass.getInstance()
						.set_titleCatagory("MOOD PRODUCTS");
				DataHolderClass.getInstance().set_catagoryName("moodproducts");
			} else if (CommonData.scannedProduct.getCatagory()
					.equalsIgnoreCase("Best Seller")) {
				DataHolderClass.getInstance().set_filterBy("Best Seller");
				DataHolderClass.getInstance().set_titleCatagory("BEST SELLERS");
				DataHolderClass.getInstance().set_catagoryName("bestsellers");

			}
			// ProductScreen.fromScan = false;
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, new MobileProductListing(false))
					.commit();
		}
		prefs = getApplicationContext().getSharedPreferences("LoginPref", 0);
		userID = prefs.getString("USER-ID", "");
		emaiID = prefs.getString("USER-EMAIL", "");
		repID = prefs.getString("REP-ID", "0");

		connectionDetector = new ConnectionDetector(this);

		if (userID.equals("") || emaiID.equals(""))
			isLogin = false;
		else
			isLogin = true;

		initDrawer();

		if (CommonData.adsList.size() == 0) {
			new GetAds(HomeMob.this, userID).execute();
		} else {
			onAdsLoaded();
		}
	}

	private void initDrawer() {
		Log.e("tag", "HomeMob Sliding menu line 150");
		drawer = new SlidingMenu(this);
		drawer.setMode(SlidingMenu.RIGHT);
		drawer.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		drawer.setShadowDrawable(R.drawable.shadow_mob);
		drawer.setShadowWidthRes(R.dimen.shadowWidth);
		drawer.setBehindWidthRes(R.dimen.drawerWidth);
		drawer.setFadeDegree(0.35f);
		drawer.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		drawer.setMenu(R.layout.drawer_layout);

		// actionbar
		actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.mobile_actionbar);
		logo = (ImageView) actionBar.getCustomView()
				.findViewById(R.id.app_icon);
		title = (TextView) actionBar.getCustomView().findViewById(R.id.title);

		title.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		menu = (Button) actionBar.getCustomView()
				.findViewById(R.id.menu_mobile);
		shoot = (Button) actionBar.getCustomView().findViewById(
				R.id.shoot_mobile);
		search = (Button) actionBar.getCustomView().findViewById(
				R.id.search_mobile);
		searchField = (MyEditText) actionBar.getCustomView().findViewById(
				R.id.searchText);

		back = (Button) actionBar.getCustomView()
				.findViewById(R.id.back_mobile);

		reqWebinar = (Button) drawer.findViewById(R.id.rw);
		reqMeeting = (Button) drawer.findViewById(R.id.rrm);
		requestCatalog = (Button) drawer.findViewById(R.id.rc);
		orderSample = (Button) drawer.findViewById(R.id.os);
		slider = (SliderLayout) drawer.findViewById(R.id.slider);
		helpUs = (Button) drawer.findViewById(R.id.hshu);
		editProfile = (Button) drawer.findViewById(R.id.ep);
		moodProducts = (Button) drawer.findViewById(R.id.mood_products);
		newProducts = (Button) drawer.findViewById(R.id.new_products);
		bestSellers = (Button) drawer.findViewById(R.id.best_sellers);
		snacksAkron = (Button) drawer.findViewById(R.id.snacks);
		searchByOrder = (Button) drawer.findViewById(R.id.searchByOrder);
		
		slider.setDuration(6000);

		reqWebinar.setOnClickListener(this);
		reqMeeting.setOnClickListener(this);
		requestCatalog.setOnClickListener(this);

		moodProducts.setOnClickListener(this);
		newProducts.setOnClickListener(this);
		bestSellers.setOnClickListener(this);
		snacksAkron.setOnClickListener(this);
		searchByOrder.setOnClickListener(this);

		menu.setOnClickListener(this);
		search.setOnClickListener(this);
		shoot.setOnClickListener(this);
		back.setOnClickListener(this);

		logout = (Button) drawer.findViewById(R.id.logout);
		logout.setOnClickListener(this);
		orderSample.setOnClickListener(this);
		editProfile.setOnClickListener(this);
		helpUs.setOnClickListener(this);
	}

	public void setTitle() {
		logo.setVisibility(View.VISIBLE);
		back.setVisibility(View.GONE);
		title.setVisibility(View.GONE);
	}

	public void setTitle(String text) {
		logo.setVisibility(View.GONE);
		back.setVisibility(View.VISIBLE);
		title.setVisibility(View.VISIBLE);

		title.setText(text);
	}

	private void showToast(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}

	// <<<<<<< HEAD
	// @Override
	// =======
	// public void onClick(View v) {
	// switch (v.getId()) {
	//
	// case R.id.shoot_mobile:
	// try {
	// if (userId == null || userId.equals("")) {
	// showToast("Please login first");
	// return;
	// }
	// overridePendingTransition(R.anim.enter_new_screen,
	// R.anim.exit_old_screen);
	// startActivity(new Intent(HomeMob.this, SyncMob.class));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// break;
	//
	// case R.id.menu_mobile:
	// try {
	// if (userId == null || userId.equals("")) {
	// showToast("Please login first");
	// return;
	// }
	// drawer.toggle();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// break;
	//
	// case R.id.back_mobile:
	// onBackPressed();
	// break;
	// case R.id.search_mobile:
	// // TODO
	// showToast("Pending");
	// break;
	// >>>>>>> 08c30060d0beed0afbb74dd084619cf8212a2226
	public void onAdsLoaded() {
		for (String ad : CommonData.adsList) {
			DefaultSliderView sv = new DefaultSliderView(this);
			sv.image(ad);
			slider.addSlider(sv);
		}
	}

	public void onClick(View v) {
		repID = prefs.getString("REP-ID", "0");
		View target = this.getWindow().getDecorView()
				.findViewById(android.R.id.content).findFocus();
		if (target != null) {
			InputMethodManager imm = (InputMethodManager) target.getContext()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(target.getWindowToken(), 0);
		}
		switch (v.getId()) {
		case R.id.logout:
			prefs = getApplicationContext()
					.getSharedPreferences("LoginPref", 0);
			Editor editor = prefs.edit();
			editor.clear();
			editor.commit();

			Intent i;
			i = new Intent(this, HomeMob.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(i);
			overridePendingTransition(R.anim.enter_new_screen,
					R.anim.exit_old_screen);

			/*
			 * Intent logout = new Intent(this, HomeTab.class);
			 * logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
			 * Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			 * startActivity(logout);
			 * overridePendingTransition(R.anim.enter_new_screen,
			 * R.anim.exit_old_screen);
			 */
			break;

		case R.id.shoot_mobile:
			try {
				if (userId == null || userId.equals("")) {
					showToast("Please login first");
					return;
				}
				overridePendingTransition(R.anim.enter_new_screen,
						R.anim.exit_old_screen);

				SharedPreferences preferences = getSharedPreferences("app",
						android.content.Context.MODE_PRIVATE);

				boolean isFromMobileProductListing = preferences.getBoolean(
						"isFromMobileProductList", false);
				
				System.out.println(" ================================");
				System.out.println(MobileProductListing.isFromScan);
				System.out.println(isFromMobileProductListing);
				if (isFromMobileProductListing) {
					SharedPreferences.Editor _editor = preferences.edit();
					_editor.putBoolean("isFromMobileProductList", false);
					_editor.commit();
					finish();
				} else {
					startActivity(new Intent(HomeMob.this, SyncMob.class));
				} 
			}

			catch (Exception e) {
				e.printStackTrace();

				overridePendingTransition(R.anim.enter_new_screen,
						R.anim.exit_old_screen);
				startActivity(new Intent(HomeMob.this, SyncMob.class));
			}
			break;

		case R.id.menu_mobile:
			drawer.toggle();
			break;

		case R.id.back_mobile:
			onBackPressed();
			break;
		case R.id.search_mobile:

			break;

		case R.id.rw:
			if (connectionDetector.isConnectingToInternet()) {
				if (repID.equals("0")) {
					StateChooser stateDialoge = new StateChooser(this, userID, 2);
					stateDialoge.setOnDialogSubmitListner(this);
					stateDialoge.show();
					//new StateChooser(this, userID, 2).show();
				} else {
					RequestWebinarFragment _fragment = new RequestWebinarFragment();
					// <<<<<<< HEAD
					transactionsWithDrawer(R.id.container, _fragment,
							"RequestWebinarFragment");
					// =======
					// letsHaveSomeTransactions(R.id.container, _fragment,
					// "RequestWebinarFragment", null);
					// >>>>>>> 08c30060d0beed0afbb74dd084619cf8212a2226
					drawer.toggle();
				}
			} else {
				showToast("No Internet");
			}
			break;

		case R.id.rrm:
			if (connectionDetector.isConnectingToInternet()) {
				if (repID.equals("0")) {
					StateChooser stateDialoge = new StateChooser(this, userID, 1);
					stateDialoge.setOnDialogSubmitListner(this);
					stateDialoge.show();
					//new StateChooser(this, userID, 1).show();
				} else {
					RequestMeetingFragment meetingfragment = new RequestMeetingFragment();
					// <<<<<<< HEAD
					transactionsWithDrawer(R.id.container, meetingfragment,
							"RequestMeetingFragment");
					// =======
					// letsHaveSomeTransactions(R.id.container, meetingfragment,
					// "RequestMeetingFragment", null);
					// >>>>>>> 08c30060d0beed0afbb74dd084619cf8212a2226
					drawer.toggle();
				}
			} else {
				showToast("No Internet");
			}
			break;

		case R.id.rc:
			if (connectionDetector.isConnectingToInternet()) {

				RequestCatlogFragment reqCatlogFragment = new RequestCatlogFragment();
				// <<<<<<< HEAD
				transactionsWithDrawer(R.id.container, reqCatlogFragment,
						"RequestWebinarFragment");
				// =======
				// letsHaveSomeTransactions(R.id.container, reqCatlogFragment,
				// "RequestWebinarFragment", null);
				// >>>>>>> 08c30060d0beed0afbb74dd084619cf8212a2226
				drawer.toggle();
			} else {
				showToast("No Internet");
			}
			break;

		case R.id.os:
			if (connectionDetector.isConnectingToInternet()) {
				// <<<<<<< HEAD
				RequestOrderFragment orderFragment = new RequestOrderFragment();
				transactionsWithDrawer(R.id.container, orderFragment,
						"RequestOrderFragment");
				drawer.toggle();
			} else {
				showToast("No Internet");
			}
			break;

		case R.id.ep:
			if (connectionDetector.isConnectingToInternet()) {
				EditProfileMobile editProfileFragment = new EditProfileMobile();
				transactionsWithDrawer(R.id.container, editProfileFragment,
						"EditProfileFragment");
				drawer.toggle();
			} else {
				showToast("No Internet");
			}
			break;

		case R.id.hshu:
			if (connectionDetector.isConnectingToInternet()) {
				HelpUsHelpOutFragment helpUsFragment = new HelpUsHelpOutFragment();
				transactionsWithDrawer(R.id.container, helpUsFragment,
						"HelpUsHelpOutFragment");
				drawer.toggle();
			} else {
				showToast("No Internet");
			}
			break;

		case R.id.mood_products:
			if (connectionDetector.isConnectingToInternet()) {
				DataHolderClass.getInstance().set_filterBy("Mood Products");
				DataHolderClass.getInstance()
						.set_titleCatagory("MOOD PRODUCTS");
				DataHolderClass.getInstance().set_catagoryName("moodproducts");
				MobileProductListing _listing = new MobileProductListing(false);
				letsHaveSomeTransactions(R.id.container, _listing, "mood", null);
				drawer.toggle();
			} else {
				showToast("No Internet");
			}
			break;

		case R.id.new_products:
			if (connectionDetector.isConnectingToInternet()) {
				DataHolderClass.getInstance().set_filterBy("New Products");
				DataHolderClass.getInstance().set_titleCatagory("NEW PRODUCTS");
				DataHolderClass.getInstance().set_catagoryName("newproducts");
				MobileProductListing _listing = new MobileProductListing(false);
				letsHaveSomeTransactions(R.id.container, _listing, "new", null);
				drawer.toggle();
			} else {
				showToast("No Internet");
			}
			break;

		case R.id.best_sellers:
			if (connectionDetector.isConnectingToInternet()) {
				DataHolderClass.getInstance().set_filterBy("Best Seller");
				DataHolderClass.getInstance().set_titleCatagory("BEST SELLERS");
				DataHolderClass.getInstance().set_catagoryName("bestsellers");
				MobileProductListing _listing = new MobileProductListing(false);
				letsHaveSomeTransactions(R.id.container, _listing, "best", null);
				// =======
				// RequestOrderFragment reqCatlogFragment = new
				// RequestOrderFragment();
				// letsHaveSomeTransactions(R.id.container, reqCatlogFragment,
				// "RequestOrderFragment", null);
				// >>>>>>> 08c30060d0beed0afbb74dd084619cf8212a2226
				drawer.toggle();
			} else {
				showToast("No Internet");
			}
			break;

		case R.id.snacks:
			if (connectionDetector.isConnectingToInternet()) {
				DataHolderClass.getInstance().set_filterBy("Snacks");
				DataHolderClass.getInstance().set_titleCatagory("SNACKS");
				DataHolderClass.getInstance().set_catagoryName("snacks");
				MobileProductListing _listing = new MobileProductListing(false);
				letsHaveSomeTransactions(R.id.container, _listing, "snacks",
						null);
				drawer.toggle();
			} else {
				showToast("No Internet");
			}
			break;
			
		case R.id.searchByOrder:
			if (connectionDetector.isConnectingToInternet()) {
				
				Log.e("tag", "search by order");
				
				SearchByOrderFragment searchByOrderFragment = new SearchByOrderFragment();
				letsHaveSomeTransactions(R.id.container, searchByOrderFragment, "searchByOrder",
						null);
				drawer.toggle();
				
				
			} else {
				showToast("No Internet");
			}
			break;
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		try {
			prefs = getApplicationContext()
					.getSharedPreferences("LoginPref", 0);
			emailId = prefs.getString("USER-EMAIL", emailId);
			userId = prefs.getString("USER-ID", userId);

			if (userId == null || userId.equals("")) {
				drawer.setSlidingEnabled(false);
			} else {
				drawer.setSlidingEnabled(true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void letsHaveSomeTransactions(int id, Fragment newFrag, String tag,
			Bundle bundle) {
		// TODO need to fix this
		Fragment prevFrag = getSupportFragmentManager().findFragmentByTag(tag);
		fragmentManager = getSupportFragmentManager();
		// frags = newFrag;
		if (prevFrag == null) {
			// push
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
					android.R.anim.fade_out);
			if (bundle != null) // Null Check
			{
				newFrag.setArguments(bundle);
			}
			fragmentTransaction.replace(id, newFrag, tag);
			fragmentTransaction.addToBackStack(tag);
			fragmentTransaction.commit();
		} else {
			// pop
			fragmentManager.popBackStack(tag, 0);
		}
	}

	public void transactionsWithDrawer(int id, Fragment newFrag, String tag) {
		// TODO need to fix this
		Fragment prevFrag = getSupportFragmentManager().findFragmentByTag(tag);
		// frags = newFrag;
		fragmentManager = getSupportFragmentManager();
		if (prevFrag == null) {
			// push
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
					android.R.anim.fade_out);
			int size = fragmentManager.getBackStackEntryCount();
			fragmentTransaction.add(id, newFrag, tag);
			for (int i = 0; i < size; i++) {
				fragmentManager.popBackStack();
			}
			fragmentTransaction.addToBackStack(tag);

			fragmentTransaction.commit();
		} else {
			// pop
			fragmentManager.popBackStack(tag, 0);
		}
	}

	@Override
	public void onBackPressed() {
		View target = this.getWindow().getDecorView()
				.findViewById(android.R.id.content).findFocus();
		Thread quitThread;
		String fragTag = null;
		boolean justLauncherFragVisible = false;// to check if the launcher
												// fragment just appear from
												// backstack
		if (fragmentManager == null
				|| fragmentManager.getBackStackEntryCount() == 0)
			// do nothing
			System.out.println("blha...");
		else
			fragTag = fragmentManager.getBackStackEntryAt(
					fragmentManager.getBackStackEntryCount() - 1).getName();
		if (fragTag != null) {
			if (fragTag.equals("mood") || fragTag.equals("new")
					|| fragTag.equals("best") || fragTag.equals("snacks")) {
				for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
					fragmentManager.popBackStack();
				}
				justLauncherFragVisible = true;
			} else {
				fragmentManager.popBackStackImmediate();
				justLauncherFragVisible = true;
			}
		}

		try {
			if (launcherFrag.isVisible() && justLauncherFragVisible == false) {
				if (!drawer.isMenuShowing()) {
					if (i >= 0 && i <= 1) {
						Toast.makeText(this, "Press Back Again To Quit",
								Toast.LENGTH_SHORT).show();
						i++;
						quitThread = new Thread() {
							public void run() {
								try {
									sleep(2000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								i = 0;
							}
						};
						quitThread.start();
					}
					if (i > 1) {
						super.onBackPressed();
						i = 0;
					}
				}
			}
		} catch (Exception e) {// just to make sure that user can choose for
								// rescanning
			e.printStackTrace();

			finish();
			SyncMob.syncMob.finish();
			Intent intent = new Intent(HomeMob.this, SyncMob.class);
			startActivity(intent);
		}

		if (target != null) {
			InputMethodManager imm = (InputMethodManager) target.getContext()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(target.getWindowToken(), 0);
		}
		if (drawer.isMenuShowing()) {
			drawer.toggle(true);
		}
	}

	@Override
	public void onDialogSubmit(int requestCode, String result) {
		if(requestCode == 1){
			RequestMeetingFragment meetingfragment = new RequestMeetingFragment();
			transactionsWithDrawer(R.id.container, meetingfragment,
					"RequestMeetingFragment");
			drawer.toggle();
			
		}else {
			RequestWebinarFragment _fragment = new RequestWebinarFragment();
			transactionsWithDrawer(R.id.container, _fragment,
					"RequestWebinarFragment");
			drawer.toggle();
			
		}		
	}
}
