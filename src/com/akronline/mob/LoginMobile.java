package com.akronline.mob;

import java.net.UnknownHostException;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.akronline.HomeTab;
import com.akronline.R;
import com.akronline.views.MyEditText;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.internet.ConnectionDetector;
import com.progressbar.GoogleProgressDialog;

public class LoginMobile extends Activity implements OnClickListener {

	Button btnSignIn;
	com.akronline.views.MyEditText username, password;
	Boolean isConnected = false;
	ConnectionDetector _cd;
	SharedPreferences prefs;
	String emailId;
	String userID;
	int i = 0;
	
	private EasyTracker easyTracker = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("tag", "LoginMobile");
		easyTracker = EasyTracker.getInstance(LoginMobile.this);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mobile_login);
//		getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		setFinishOnTouchOutside(false);
		
		prefs = getApplicationContext().getSharedPreferences("LoginPref", 0);
		emailId = prefs.getString("USER-EMAIL", "");
		userID = prefs.getString("USER-ID", "");

		btnSignIn = (Button) findViewById(R.id.btnSignIn);
		btnSignIn.setOnClickListener(this);

		username = (MyEditText) findViewById(R.id.get_useremail);
		password = (MyEditText) findViewById(R.id.get_password);
		_cd = new ConnectionDetector(getApplicationContext());
		isConnected = _cd.isConnectingToInternet();
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnSignIn:
			try {
				
				easyTracker.send(MapBuilder.createEvent("Login",
						"login", "login btn", null).build());
				
				if (isConnected) {
					String uname = username.getText().toString();
					String passwd = password.getText().toString();

					if (username.length() == 0) {
						Toast.makeText(this, "Enter email", Toast.LENGTH_LONG)
								.show();
					} else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(
							uname).matches()) {
						Toast.makeText(this, "Enter valid email",
								Toast.LENGTH_LONG).show();
					} else if (passwd.length() == 0) {
						Toast.makeText(this, "Enter password",
								Toast.LENGTH_LONG).show();
					} else {
						new CheckValidUser(uname, passwd).execute();
					}
				} else {
					Toast.makeText(this, "No internet Connection",
							Toast.LENGTH_LONG).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		default:
			break;
		}
	}

	private class CheckValidUser extends AsyncTask<String, String, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/GetUserDetails";
		private static final String NAMESPACE = "http://tempuri.org/";
		private static final String METHOD_NAME = "GetUserDetails";
		private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";

		String email, pass;
		GoogleProgressDialog dialog;
		JSONObject data;

		public CheckValidUser(String name, String pass) {
			this.email = name;
			this.pass = pass;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(LoginMobile.this);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				request.addProperty("UserName", email);
				request.addProperty("Password", pass);
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				try {
					HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
					androidHttpTransport.call(SOAP_ACTION, envelope);
					SoapObject result = (SoapObject) envelope.bodyIn;
					if (result != null) {
						String response = result.getProperty(0).toString();
						data = new JSONObject(response);

					} else {
						return "Invalid response from server";
					}
				} catch (UnknownHostException uh) {
					return "No internet connection";
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Error occured! Please try again";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null) {
				Toast.makeText(LoginMobile.this, result, Toast.LENGTH_LONG)
						.show();
			} else {
				try {
					JSONObject ob = data.getJSONArray("Data").getJSONObject(0);
					String responseCode = ob.getString("responseCode");
					if (responseCode.equalsIgnoreCase("0")) {
						userID = ob.getString("UserId");
						emailId = ob.getString("user_email");
						String register = ob.getString("Register");
						Editor prefsEditor = prefs.edit();
						prefsEditor.putString("USER-ID", userID);
						prefsEditor.putString("USER-EMAIL", emailId);
						prefsEditor.commit();
						if (register.equals("1")) {
							Toast.makeText(LoginMobile.this,
									"You have registered successfully",
									Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(LoginMobile.this,
									"Login successful.", Toast.LENGTH_LONG)
									.show();
						}
//						finish();
						Intent i= null;
						if (getString(R.string.screen_type).equals("Tablet")) {
							setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
							i = new Intent(LoginMobile.this, HomeTab.class);
						} else {
							setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//							i = new Intent(LoginMobile.this, HomeMob.class);
							finish();
						}
						if(i != null){
						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(i);
						overridePendingTransition(R.anim.enter_new_screen,
								R.anim.exit_old_screen);
						}
					} else {
						
						Toast.makeText(LoginMobile.this, getResources().getString(R.string.error_password),
								Toast.LENGTH_LONG).show();
					}
				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(LoginMobile.this,
							"Error occured! Please try again",
							Toast.LENGTH_LONG).show();
				}
			}
		}
	}
	
		@Override
	public void onBackPressed() {
		Thread quitThread;
		if(i>=0 && i <=1) {
			Toast.makeText(this, "Press Back Again To Quit", Toast.LENGTH_SHORT).show();
			i++;
			quitThread = new Thread() {
				public void run() {
					try {
						sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					i = 0;
				}
			};
			quitThread.start();
		} if(i > 1) {
			super.onBackPressed();
			HomeMob.homeMob.finish();
			i = 0;
		}
	}
}