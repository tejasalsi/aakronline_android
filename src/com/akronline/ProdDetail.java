package com.akronline;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.adapters.InventoryAdapter;
import com.adapters.ProductDetailAdapter;
import com.adapters.SampleColorAdapter;
import com.akronline.dialogs.SendEmail;
import com.androidquery.AQuery;
import com.dataholder.DataHolderClass;
import com.datamodel.InventoryModel;
import com.datamodel.Product;
import com.datamodel.ProductDetailModel;
import com.moodstocks.android.MoodstocksError;
import com.moodstocks.android.Scanner;
import com.progressbar.GoogleProgressDialog;

public class ProdDetail extends Fragment implements OnClickListener, OnCheckedChangeListener
{

	private static final String NAMESPACE = "http://tempuri.org/";

	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	ArrayList<ProductDetailModel> _chargeList = new ArrayList<ProductDetailModel>();

	ArrayList<ProductDetailModel> _DetailsList = new ArrayList<ProductDetailModel>();

	ArrayList<InventoryModel> _inventoryList = new ArrayList<InventoryModel>();

	ArrayList<ProductDetailModel> _OptionsList = new ArrayList<ProductDetailModel>();

	DecimalFormat df = new DecimalFormat();

	String userID;

	Product product;

	String vidURL;

	LinearLayout detailLayout;

	LinearLayout one, two, three, four, five;

	View scrPD, scrRS, scrRQ, scrCI, scrFE;

	ScrollView bottomView;

	TextView txtProdID, txtProdName, txtProdPrice, txtAsLowAs;

	ImageView imgProd;

	ImageView btnVideo, btnEmail;

	TextView Freight_estimator, Check_inventory, Request_quote, Request_sample, Product_info;

	ListView info_list;

	TextView btnDetail, btnCharges, btnOptions, btnFeatures;

	TextView txtProdDesc;

	// Details Pricing
	RelativeLayout pricingLayout;

	TextView txtPricing, txtStandard, txtExpires;

	ImageView ivUS, ivCA, arrowUS, arrowCA;

	TextView q1, q2, q3, q4, q5;

	TextView st1, st2, st3, st4, st5, dc1;

	TextView sp1, sp2, sp3, sp4, sp5, dc2;

	TextView sq1, sq2, sq3, sq4, sq5;

	TextView p1, p2, p3, p4, p5, dc3;

	TextView note;

	LinearLayout noteslayout, spclLine;

	LinearLayout newPriceLayout;

	String[] stdQuants;

	String[] splQuants;

	String[] newQuants;

	String[] stdUS;

	String[] stdCA;

	String[] splUS;

	String[] splCA;

	String[] newUS;

	String[] newCA;

	String splExpDate, prodNote = "";

	String stdCode, splCode, newCode;

	// Request Sample
	String[] spinnerValues;

	CheckBox cbSpot, cbFCD;

	Spinner spinColors;

	EditText etQuant;

	Button btnReqSamp;

	TextView txtQuant, txtChoosecolor;

	// Req Quote
	RelativeLayout pricingLayoutQ;

	Button btnReqQuote;

	// String[] spinnerValuesQ;
	CheckBox cbSpotQ, cbFCDQ;

	TextView txtChoosecolorQ, txtQuantQ;

	Spinner spinColorsQ;

	EditText etQuantQ, etQuantQ2, etQuantQ3, etQuantQ4, etQuantQ5, etInfo;

	TextView txtPricingQ, txtStandardQ, txtExpiresQ;

	ImageView ivUSQ, ivCAQ, arrowUSQ, arrowCAQ;

	TextView Qq1, Qq2, Qq3, Qq4, Qq5;

	TextView Qst1, Qst2, Qst3, Qst4, Qst5, Qdc1;

	TextView Qsp1, Qsp2, Qsp3, Qsp4, Qsp5, Qdc2;

	TextView Qsq1, Qsq2, Qsq3, Qsq4, Qsq5;

	TextView Qp1, Qp2, Qp3, Qp4, Qp5, Qdc3;

	TextView noteQ;

	LinearLayout noteslayoutQ, spclLineQ;

	LinearLayout newPriceLayoutQ;

	// Check Inventory
	private static String CHECK_INVENTORY_METHOD = "GetInventoryDetails";

	private static String CHECK_INVENTORY_SOAP_ACTION = "http://tempuri.org/GetInventoryDetails";

	ListView invenListView;

	TextView set_part;

	// Freight Estimator
	private static String FRIEGHT_ESTIMATOR_METHOD = "GetFreightEstimator";

	private static String FRIEGHT_ESTIMATOR_SOAP_ACTION = "http://tempuri.org/GetFreightEstimator";

	ProgressBar progress;

	TextView txtNolink;

	LinearLayout invenItems;

	com.webview.TouchyWebView web_view;

	Scanner scanner;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		Log.e("tag", "ProdDetail");
		try
		{
			if (Scanner.isCompatible())
			{
				scanner = Scanner.get();
				scanner.close();
				scanner.destroy();
				scanner = null;
			}
		}
		catch (MoodstocksError e)
		{
			e.printStackTrace();
		}

		userID = getActivity().getApplicationContext().getSharedPreferences("LoginPref", 0).getString("USER-ID", null);

		df.setMinimumFractionDigits(2);
		View v = inflater.inflate(R.layout.detail_frag, container, false);

		imgProd = (ImageView) v.findViewById(R.id.product_image);
		txtProdID = (TextView) v.findViewById(R.id.set_product_id);
		txtProdName = (TextView) v.findViewById(R.id.set_product_name);
		txtProdPrice = (TextView) v.findViewById(R.id.set_price);
		txtAsLowAs = (TextView) v.findViewById(R.id.set_as_low);
		btnVideo = (ImageView) v.findViewById(R.id.btnPlay);
		btnEmail = (ImageView) v.findViewById(R.id.btnEmail);

		detailLayout = (LinearLayout) v.findViewById(R.id.details);
		one = (LinearLayout) v.findViewById(R.id.one);
		two = (LinearLayout) v.findViewById(R.id.two);
		three = (LinearLayout) v.findViewById(R.id.three);
		four = (LinearLayout) v.findViewById(R.id.four);
		five = (LinearLayout) v.findViewById(R.id.five);

		Freight_estimator = (TextView) v.findViewById(R.id.Freight_estimator);
		Check_inventory = (TextView) v.findViewById(R.id.Check_inventory);
		Request_quote = (TextView) v.findViewById(R.id.Request_quote);
		Request_sample = (TextView) v.findViewById(R.id.Request_sample);
		Product_info = (TextView) v.findViewById(R.id.Product_info);

		bottomView = (ScrollView) v.findViewById(R.id.bottomViews);
		scrPD = v.findViewById(R.id.product_details_screen);
		scrRS = v.findViewById(R.id.request_sample_out);
		scrRQ = v.findViewById(R.id.layout_request_quote);
		scrCI = v.findViewById(R.id.check_inventory_screens);
		scrFE = v.findViewById(R.id.freight_estimator_out);

		btnVideo.setOnClickListener(this);
		btnEmail.setOnClickListener(this);
		detailLayout.setOnClickListener(this);
		txtAsLowAs.setOnClickListener(this);
		txtProdID.setOnClickListener(this);
		txtProdName.setOnClickListener(this);
		txtProdPrice.setOnClickListener(this);
		Freight_estimator.setOnClickListener(this);
		Check_inventory.setOnClickListener(this);
		Request_quote.setOnClickListener(this);
		Request_sample.setOnClickListener(this);
		Product_info.setOnClickListener(this);

		// for info screens

		info_list = (ListView) v.findViewById(R.id.info_list);
		btnDetail = (TextView) v.findViewById(R.id.set_detail_btn);
		btnCharges = (TextView) v.findViewById(R.id.set_charges_btn);
		btnOptions = (TextView) v.findViewById(R.id.set_options_btn);
		btnFeatures = (TextView) v.findViewById(R.id.features);
		txtProdDesc = (TextView) v.findViewById(R.id.txtProdDesc);

		btnDetail.setOnClickListener(this);
		btnCharges.setOnClickListener(this);
		btnOptions.setOnClickListener(this);

		info_list.setOnTouchListener(new ListView.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				int action = event.getAction();
				switch (action)
				{
					case MotionEvent.ACTION_DOWN:
						v.getParent().requestDisallowInterceptTouchEvent(true);
						break;

					case MotionEvent.ACTION_UP:
						v.getParent().requestDisallowInterceptTouchEvent(false);
						break;
				}
				v.onTouchEvent(event);
				return true;
			}
		});

		// Detail Pricing
		pricingLayout = (RelativeLayout) v.findViewById(R.id.pricing);
		txtPricing = (TextView) v.findViewById(R.id.txtPricing);
		txtStandard = (TextView) v.findViewById(R.id.txtStandard);
		txtExpires = (TextView) v.findViewById(R.id.txtExpires);
		ivCA = (ImageView) v.findViewById(R.id.imgCA);
		ivUS = (ImageView) v.findViewById(R.id.imgUSA);
		arrowCA = (ImageView) v.findViewById(R.id.arrowCA);
		arrowUS = (ImageView) v.findViewById(R.id.arrowUS);
		noteslayout = (LinearLayout) v.findViewById(R.id.noteLayout);
		note = (TextView) v.findViewById(R.id.txtNote);
		spclLine = (LinearLayout) v.findViewById(R.id.specialPrices);
		newPriceLayout = (LinearLayout) v.findViewById(R.id.newPrices);
		q1 = (TextView) v.findViewById(R.id.quant1);
		q2 = (TextView) v.findViewById(R.id.quant2);
		q3 = (TextView) v.findViewById(R.id.quant3);
		q4 = (TextView) v.findViewById(R.id.quant4);
		q5 = (TextView) v.findViewById(R.id.quant5);
		st1 = (TextView) v.findViewById(R.id.std1);
		st2 = (TextView) v.findViewById(R.id.std2);
		st3 = (TextView) v.findViewById(R.id.std3);
		st4 = (TextView) v.findViewById(R.id.std4);
		st5 = (TextView) v.findViewById(R.id.std5);
		dc1 = (TextView) v.findViewById(R.id.dc1);
		sp1 = (TextView) v.findViewById(R.id.spcl1);
		sp2 = (TextView) v.findViewById(R.id.spcl2);
		sp3 = (TextView) v.findViewById(R.id.spcl3);
		sp4 = (TextView) v.findViewById(R.id.spcl4);
		sp5 = (TextView) v.findViewById(R.id.spcl5);
		dc2 = (TextView) v.findViewById(R.id.dc2);
		sq1 = (TextView) v.findViewById(R.id.pq1);
		sq2 = (TextView) v.findViewById(R.id.pq2);
		sq3 = (TextView) v.findViewById(R.id.pq3);
		sq4 = (TextView) v.findViewById(R.id.pq4);
		sq5 = (TextView) v.findViewById(R.id.pq5);
		p1 = (TextView) v.findViewById(R.id.p1);
		p2 = (TextView) v.findViewById(R.id.p2);
		p3 = (TextView) v.findViewById(R.id.p3);
		p4 = (TextView) v.findViewById(R.id.p4);
		p5 = (TextView) v.findViewById(R.id.p5);
		dc3 = (TextView) v.findViewById(R.id.dc3);

		ivCA.setOnClickListener(this);
		ivUS.setOnClickListener(this);
		new ProductDetails().execute();

		// Request Sample
		cbSpot = (CheckBox) v.findViewById(R.id.spot_checkbox);
		cbFCD = (CheckBox) v.findViewById(R.id.fcd_checkbox);
		spinColors = (Spinner) v.findViewById(R.id.spinColorsSamp);
		etQuant = (EditText) v.findViewById(R.id.etQuantity);
		btnReqSamp = (Button) v.findViewById(R.id.btnReqSample);
		txtQuant = (TextView) v.findViewById(R.id.txtQuantity);
		txtChoosecolor = (TextView) v.findViewById(R.id.txtChooseColor);

		//		txtQuant.setText(Html.fromHtml("Quantity <font color=red>*</font>"));
		//		txtChoosecolor.setText(Html
		//				.fromHtml("Choose color <font color=red>*</font>"));

		cbSpot.setOnCheckedChangeListener(this);
		cbFCD.setOnCheckedChangeListener(this);
		btnReqSamp.setOnClickListener(this);

		// Request Quote
		txtChoosecolorQ = (TextView) v.findViewById(R.id.txtChooseColorQ);
		txtChoosecolorQ.setText(Html.fromHtml("Choose color <font color=red>*</font>"));
		txtQuantQ = (TextView) v.findViewById(R.id.txtQuantityQ);
		txtQuantQ.setText(Html.fromHtml("Quantity <font color=red>*</font>"));
		pricingLayoutQ = (RelativeLayout) v.findViewById(R.id.pricingQ);
		cbSpotQ = (CheckBox) v.findViewById(R.id.spot_checkboxQ);
		cbFCDQ = (CheckBox) v.findViewById(R.id.fcd_checkboxQ);
		spinColorsQ = (Spinner) v.findViewById(R.id.spinColorsQ);
		btnReqQuote = (Button) v.findViewById(R.id.btnReqQuote);
		etQuantQ = (EditText) v.findViewById(R.id.etQuantityQ);
		etQuantQ2 = (EditText) v.findViewById(R.id.etQuantityQ2);
		etQuantQ3 = (EditText) v.findViewById(R.id.etQuantityQ3);
		etQuantQ4 = (EditText) v.findViewById(R.id.etQuantityQ4);
		etQuantQ5 = (EditText) v.findViewById(R.id.etQuantityQ5);
		etInfo = (EditText) v.findViewById(R.id.etInfo);
		txtPricingQ = (TextView) v.findViewById(R.id.txtPricingQ);
		txtStandardQ = (TextView) v.findViewById(R.id.txtStandardQ);
		txtExpiresQ = (TextView) v.findViewById(R.id.txtExpiresQ);
		ivCAQ = (ImageView) v.findViewById(R.id.imgCAQ);
		ivUSQ = (ImageView) v.findViewById(R.id.imgUSAQ);
		arrowCAQ = (ImageView) v.findViewById(R.id.arrowCAQ);
		arrowUSQ = (ImageView) v.findViewById(R.id.arrowUSQ);
		noteQ = (TextView) v.findViewById(R.id.txtNoteQ);
		noteslayoutQ = (LinearLayout) v.findViewById(R.id.noteLayoutQ);
		spclLineQ = (LinearLayout) v.findViewById(R.id.specialPricesQ);
		newPriceLayoutQ = (LinearLayout) v.findViewById(R.id.newPricesQ);
		Qq1 = (TextView) v.findViewById(R.id.Qquant1);
		Qq2 = (TextView) v.findViewById(R.id.Qquant2);
		Qq3 = (TextView) v.findViewById(R.id.Qquant3);
		Qq4 = (TextView) v.findViewById(R.id.Qquant4);
		Qq5 = (TextView) v.findViewById(R.id.Qquant5);
		Qst1 = (TextView) v.findViewById(R.id.Qstd1);
		Qst2 = (TextView) v.findViewById(R.id.Qstd2);
		Qst3 = (TextView) v.findViewById(R.id.Qstd3);
		Qst4 = (TextView) v.findViewById(R.id.Qstd4);
		Qst5 = (TextView) v.findViewById(R.id.Qstd5);
		Qdc1 = (TextView) v.findViewById(R.id.Qdc1);
		Qsp1 = (TextView) v.findViewById(R.id.Qspcl1);
		Qsp2 = (TextView) v.findViewById(R.id.Qspcl2);
		Qsp3 = (TextView) v.findViewById(R.id.Qspcl3);
		Qsp4 = (TextView) v.findViewById(R.id.Qspcl4);
		Qsp5 = (TextView) v.findViewById(R.id.Qspcl5);
		Qdc2 = (TextView) v.findViewById(R.id.Qdc2);
		Qsq1 = (TextView) v.findViewById(R.id.Qpq1);
		Qsq2 = (TextView) v.findViewById(R.id.Qpq2);
		Qsq3 = (TextView) v.findViewById(R.id.Qpq3);
		Qsq4 = (TextView) v.findViewById(R.id.Qpq4);
		Qsq5 = (TextView) v.findViewById(R.id.Qpq5);
		Qp1 = (TextView) v.findViewById(R.id.Qp1);
		Qp2 = (TextView) v.findViewById(R.id.Qp2);
		Qp3 = (TextView) v.findViewById(R.id.Qp3);
		Qp4 = (TextView) v.findViewById(R.id.Qp4);
		Qp5 = (TextView) v.findViewById(R.id.Qp5);
		Qdc3 = (TextView) v.findViewById(R.id.Qdc3);

		ivCAQ.setOnClickListener(this);
		ivUSQ.setOnClickListener(this);
		cbSpotQ.setOnCheckedChangeListener(this);
		cbFCDQ.setOnCheckedChangeListener(this);
		btnReqQuote.setOnClickListener(this);

		// Check inventory
		set_part = (TextView) v.findViewById(R.id.set_part);
		invenListView = (ListView) v.findViewById(R.id.inventoryList);

		invenListView.setOnTouchListener(new OnTouchListener()
		{

			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				int action = event.getAction();
				switch (action)
				{
					case MotionEvent.ACTION_DOWN:
						v.getParent().requestDisallowInterceptTouchEvent(true);
						break;

					case MotionEvent.ACTION_UP:
						v.getParent().requestDisallowInterceptTouchEvent(false);
						break;
				}
				v.onTouchEvent(event);
				return true;
			}
		});

		// Freight estimator
		web_view = (com.webview.TouchyWebView) v.findViewById(R.id.web_view);
		txtNolink = (TextView) v.findViewById(R.id.noLink);
		invenItems = (LinearLayout) v.findViewById(R.id.invenItems);
		progress = (ProgressBar) v.findViewById(R.id.progressBar);
		progress.setVisibility(GONE);

		web_view.setWebViewClient(new AakronlineWebViewClient());
		web_view.getSettings().setJavaScriptEnabled(true);

		web_view.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				v.getParent().requestDisallowInterceptTouchEvent(false);
				return false;
			}
		});

		return v;
	}

	int tabIndex = 0;

	int detailTabPosition = 1;

	@Override
	public void onClick(View v)
	{
		boolean isImgVisible = imgProd.getVisibility() == VISIBLE;
		Log.v("imgDimens", "Top:" + imgProd.getTop() + " Bottom: " + imgProd.getBottom());
		Log.v("scrollDimes", "Top:" + bottomView.getTop() + " Bottom: " + bottomView.getBottom());
		try
		{
			switch (v.getId())
			{
				case R.id.details:
				case R.id.set_as_low:
				case R.id.set_product_id:
				case R.id.set_product_name:
				case R.id.set_price:
					if (!isImgVisible)
						switchTab(0);
					break;
				case R.id.btnReqQuote:
					if (spinnerValues == null || spinnerValues.length == 0)
					{
						showToast("No color available for this product");
						return;
					}
					if (!cbFCDQ.isChecked() && !cbSpotQ.isChecked())
					{
						showToast("Please select Imprint Method");
						return;
					}
					if (etQuantQ.getText().toString().isEmpty() && etQuantQ2.getText().toString().isEmpty()
							&& etQuantQ3.getText().toString().isEmpty() && etQuantQ4.getText().toString().isEmpty()
							&& etQuantQ5.getText().toString().isEmpty())
					{
						showToast("Please enter at least 1 quantity");
						etQuantQ.requestFocus();
						return;
					}
					String quants = "";
					if (!etQuantQ.getText().toString().isEmpty())
					{
						if (Integer.parseInt(etQuantQ.getText().toString()) < 1)
						{
							showToast("Please enter quantity greater than 0 or leave blank");
							return;
						}
						quants = etQuantQ.getText().toString();

					}
					if (!etQuantQ2.getText().toString().isEmpty())
					{
						if (quants.equals(""))
							quants = etQuantQ2.getText().toString();
						else if (Integer.parseInt(etQuantQ2.getText().toString()) < 1)
						{
							showToast("Please enter quantity greater than 0 or leave blank");
							return;
						}
						quants = quants + "|" + etQuantQ2.getText().toString();
					}
					if (!etQuantQ3.getText().toString().isEmpty())
					{
						if (quants.equals(""))
							quants = etQuantQ2.getText().toString();
						else if (Integer.parseInt(etQuantQ3.getText().toString()) < 1)
						{
							showToast("Please enter quantity greater than 0 or leave blank");
							return;
						}
						quants = quants + "|" + etQuantQ3.getText().toString();
					}
					if (!etQuantQ4.getText().toString().isEmpty())
					{
						if (quants.equals(""))
							quants = etQuantQ4.getText().toString();
						else if (Integer.parseInt(etQuantQ4.getText().toString()) < 1)
						{
							showToast("Please enter quantity greater than 0 or leave blank");
							return;
						}
						quants = quants + "|" + etQuantQ2.getText().toString();
					}
					if (!etQuantQ5.getText().toString().isEmpty())
					{
						if (quants.equals(""))
							quants = etQuantQ5.getText().toString();
						else if (Integer.parseInt(etQuantQ5.getText().toString()) < 1)
						{
							showToast("Please enter quantity greater than 0 or leave blank");
							return;
						}
						quants = quants + "|" + etQuantQ5.getText().toString();
					}
					Log.d("quants", quants);
					new RequestQuote().execute(quants);
					break;
				case R.id.btnReqSample:
					if (spinnerValues == null || spinnerValues.length == 0)
					{
						showToast("No color available for this product");
						return;
					}
					if (!cbFCD.isChecked() && !cbSpot.isChecked())
					{
						showToast("Please select Imprint Method");
						return;
					}
					if (etQuant.getText().toString().trim().isEmpty())
					{
						showToast("Please enter quantity");
						etQuant.requestFocus();
						return;
					}
					int quant = 0;
					try
					{
						quant = Integer.valueOf(etQuant.getText().toString().trim());
					}
					catch (NumberFormatException e)
					{
						quant = 0;
					}
					if (quant == 0)
					{
						showToast("Please enter valid quantity");
						return;
					}
					// all ok call service
					new RequestSample().execute();
					break;
				case R.id.btnPlay:
					try
					{
						DataHolderClass.getInstance().set_videoId(vidURL);
						ProductScreen.fromScan = false;
						startActivity(new Intent(getActivity(), PlayVideoScreen.class));
						getActivity().overridePendingTransition(R.anim.enter_new_screen, R.anim.exit_old_screen);

					}
					catch (Exception e)
					{
						e.printStackTrace();
						Toast.makeText(getActivity(), "Error! Can't Play this video", Toast.LENGTH_LONG).show();
					}
					break;
				case R.id.btnEmail:
					new SendEmail(getActivity(), product.getProductCode()).show();
					break;
				case R.id.Product_info:
				case R.id.one:
					switchTab(1);
					break;
				case R.id.Request_sample:
				case R.id.two:
					switchTab(2);
					break;
				case R.id.Request_quote:
				case R.id.three:
					switchTab(3);
					break;
				case R.id.Check_inventory:
				case R.id.four:
					switchTab(4);
					break;
				case R.id.Freight_estimator:
				case R.id.five:
					switchTab(5);
					break;
				case R.id.set_detail_btn:
					if (detailTabPosition == 1)
					{
						if (bottomView.getScrollY() == 0)
							bottomView.fullScroll(ScrollView.FOCUS_DOWN);
						else
							bottomView.fullScroll(ScrollView.FOCUS_UP);
					}
					else
					{
						detailTabPosition = 1;
						bottomView.fullScroll(ScrollView.FOCUS_DOWN);
						setInfoData(_DetailsList);
					}

					break;
				case R.id.set_charges_btn:
					if (detailTabPosition == 2)
					{
						if (bottomView.getScrollY() == 0)
							bottomView.fullScroll(ScrollView.FOCUS_DOWN);
						else
							bottomView.fullScroll(ScrollView.FOCUS_UP);
					}
					else
					{
						detailTabPosition = 2;
						bottomView.fullScroll(ScrollView.FOCUS_DOWN);
						setInfoData(_chargeList);
					}
					break;
				case R.id.set_options_btn:
					if (detailTabPosition == 3)
					{
						if (bottomView.getScrollY() == 0)
							bottomView.fullScroll(ScrollView.FOCUS_DOWN);
						else
							bottomView.fullScroll(ScrollView.FOCUS_UP);
					}
					else
					{
						detailTabPosition = 3;
						bottomView.fullScroll(ScrollView.FOCUS_DOWN);
						setInfoData(_OptionsList);
					}
					break;
				case R.id.features:
					detailTabPosition = 0;
					if (txtProdDesc.getVisibility() == VISIBLE)
					{
						if (bottomView.getScrollY() == 0)
							bottomView.fullScroll(ScrollView.FOCUS_DOWN);
						else
							bottomView.fullScroll(ScrollView.FOCUS_UP);
					}
					else
					{
						bottomView.fullScroll(ScrollView.FOCUS_DOWN);
						info_list.setVisibility(GONE);
						txtProdDesc.setVisibility(VISIBLE);
						txtProdDesc.setText(product.getDesc());
					}
					break;
				case R.id.imgUSA:
					showDetailsPrices(1);
					break;
				case R.id.imgCA:
					showDetailsPrices(2);
				case R.id.imgUSAQ:
					showQuotePrices(1);
					break;
				case R.id.imgCAQ:
					showQuotePrices(2);
					break;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
		switch (buttonView.getId())
		{
			case R.id.spot_checkbox:
				if (isChecked)
					cbFCD.setChecked(false);
				break;
			case R.id.fcd_checkbox:
				if (isChecked)
					cbSpot.setChecked(false);
				break;
			case R.id.spot_checkboxQ:
				if (isChecked)
					cbFCDQ.setChecked(false);
				break;
			case R.id.fcd_checkboxQ:
				if (isChecked)
					cbSpotQ.setChecked(false);
				break;
		}
	}

	// pass 0 to hide and 1-5 to switch
	private void switchTab(int index)
	{
		if (tabIndex != 0 && index == tabIndex)
		{
			index = 0;
			// touched on the same tab so hide all
		}
		switch (index)
		{
			case 0: // Hide all
				imgProd.setVisibility(VISIBLE);
				bottomView.setVisibility(GONE);

				one.setBackgroundResource(R.drawable.black);
				two.setBackgroundResource(R.drawable.black);
				three.setBackgroundResource(R.drawable.black);
				four.setBackgroundResource(R.drawable.black);
				five.setBackgroundResource(R.drawable.black);
				break;
			case 1: // Prod Details
				imgProd.setVisibility(GONE);
				bottomView.setVisibility(VISIBLE);
				scrCI.setVisibility(GONE);
				scrFE.setVisibility(GONE);
				scrPD.setVisibility(VISIBLE);
				scrRQ.setVisibility(GONE);
				scrRS.setVisibility(GONE);
				one.setBackgroundResource(R.drawable.buttonhover);
				two.setBackgroundResource(R.drawable.black);
				three.setBackgroundResource(R.drawable.black);
				four.setBackgroundResource(R.drawable.black);
				five.setBackgroundResource(R.drawable.black);

				setInfoData(_DetailsList);

				break;
			case 2: // Req Sample
				imgProd.setVisibility(GONE);
				bottomView.setVisibility(VISIBLE);
				scrCI.setVisibility(GONE);
				scrFE.setVisibility(GONE);
				scrPD.setVisibility(GONE);
				scrRQ.setVisibility(GONE);
				scrRS.setVisibility(VISIBLE);
				one.setBackgroundResource(R.drawable.black);
				two.setBackgroundResource(R.drawable.buttonhover);
				three.setBackgroundResource(R.drawable.black);
				four.setBackgroundResource(R.drawable.black);
				five.setBackgroundResource(R.drawable.black);
				SampleColorAdapter colorAdapter = new SampleColorAdapter(getActivity(),
						R.layout.request_sample_color_dropdown_inflator, spinnerValues);
				spinColors.setAdapter(colorAdapter);
				break;
			case 3: // Req Quote
				imgProd.setVisibility(GONE);
				bottomView.setVisibility(VISIBLE);
				scrCI.setVisibility(GONE);
				scrFE.setVisibility(GONE);
				scrPD.setVisibility(GONE);
				scrRQ.setVisibility(VISIBLE);
				scrRS.setVisibility(GONE);
				one.setBackgroundResource(R.drawable.black);
				two.setBackgroundResource(R.drawable.black);
				three.setBackgroundResource(R.drawable.buttonhover);
				four.setBackgroundResource(R.drawable.black);
				five.setBackgroundResource(R.drawable.black);
				SampleColorAdapter ca = new SampleColorAdapter(getActivity(),
						R.layout.request_sample_color_dropdown_inflator, spinnerValues);
				spinColorsQ.setAdapter(ca);
				break;
			case 4: // Check inventory
				imgProd.setVisibility(GONE);
				bottomView.setVisibility(VISIBLE);
				scrCI.setVisibility(VISIBLE);
				scrFE.setVisibility(GONE);
				scrPD.setVisibility(GONE);
				scrRQ.setVisibility(GONE);
				scrRS.setVisibility(GONE);
				one.setBackgroundResource(R.drawable.black);
				two.setBackgroundResource(R.drawable.black);
				three.setBackgroundResource(R.drawable.black);
				four.setBackgroundResource(R.drawable.buttonhover);
				five.setBackgroundResource(R.drawable.black);
				new CheckInventory().execute();
				break;
			case 5: // Freight estimator
				imgProd.setVisibility(GONE);
				bottomView.setVisibility(VISIBLE);
				scrCI.setVisibility(GONE);
				scrFE.setVisibility(VISIBLE);
				scrPD.setVisibility(GONE);
				scrRQ.setVisibility(GONE);
				scrRS.setVisibility(GONE);
				one.setBackgroundResource(R.drawable.black);
				two.setBackgroundResource(R.drawable.black);
				three.setBackgroundResource(R.drawable.black);
				four.setBackgroundResource(R.drawable.black);
				five.setBackgroundResource(R.drawable.buttonhover);
				new Estimator().execute();
				break;
		}
		tabIndex = index;
	}

	private void showToast(String msg)
	{
		Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}

	private void setInfoData(ArrayList<ProductDetailModel> data)
	{
		info_list.setVisibility(VISIBLE);
		txtProdDesc.setVisibility(GONE);
		ProductDetailAdapter _adapter = new ProductDetailAdapter(getActivity(), data);
		info_list.setAdapter(_adapter);
	}

	// i= 1 for US 2 for CA
	private void showDetailsPrices(int i)
	{
		if (prodNote.equals(""))
		{
			noteslayout.setVisibility(GONE);
		}
		else
		{
			note.setText(prodNote);
			noteslayout.setVisibility(VISIBLE);
		}
		switch (stdQuants.length)
		{
			case 5:
				q5.setText(stdQuants[4]);
			case 4:
				q4.setText(stdQuants[3]);
			case 3:
				q3.setText(stdQuants[2]);
			case 2:
				q2.setText(stdQuants[1]);
			case 1:
				q1.setText(stdQuants[0]);
		}
		if (splUS != null)
		{
			spclLine.setVisibility(VISIBLE);
			txtExpires.setVisibility(VISIBLE);
			st1.setPaintFlags(st1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			st2.setPaintFlags(st1.getPaintFlags());
			st3.setPaintFlags(st1.getPaintFlags());
			st4.setPaintFlags(st1.getPaintFlags());
			st5.setPaintFlags(st1.getPaintFlags());
			txtStandard.setPaintFlags(st1.getPaintFlags());
			txtExpires.setText("Expires: " + splExpDate);
		}
		else
		{
			spclLine.setVisibility(GONE);
			txtExpires.setVisibility(GONE);
		}
		if (i == 1)
		{ // US pricing
			arrowUS.setVisibility(VISIBLE);
			arrowCA.setVisibility(GONE);
			txtPricing.setText("Pricing, US Dollars");
			dc1.setText(stdCode);
			switch (stdUS.length)
			{
				case 5:
					st5.setText("$" + df.format(Float.valueOf(stdUS[4])));
				case 4:
					st4.setText("$" + df.format(Float.valueOf(stdUS[3])));
				case 3:
					st3.setText("$" + df.format(Float.valueOf(stdUS[2])));
				case 2:
					st2.setText("$" + df.format(Float.valueOf(stdUS[1])));
				case 1:
					st1.setText("$" + df.format(Float.valueOf(stdUS[0])));
			}
			if (splUS != null)
			{
				dc2.setText(splCode);
				switch (splUS.length)
				{
					case 5:
						sp5.setText("$" + df.format(Float.valueOf(splUS[4])));
					case 4:
						sp4.setText("$" + df.format(Float.valueOf(splUS[3])));
					case 3:
						sp3.setText("$" + df.format(Float.valueOf(splUS[2])));
					case 2:
						sp2.setText("$" + df.format(Float.valueOf(splUS[1])));
					case 1:
						sp1.setText("$" + df.format(Float.valueOf(splUS[0])));
				}
			}

			// TODO fix
			// if (newUS != null) {
			// newPriceLayout.setVisibility(VISIBLE);
			// dc3.setText(newCode);
			// switch (newUS.length) {
			// case 5:
			// sq5.setText(newQuants[4]);
			// p5.setText(df.format(Float.valueOf(newUS[4])));
			// case 4:
			// sq4.setText(newQuants[3]);
			// p4.setText(df.format(Float.valueOf(newUS[3])));
			// case 3:
			// sq3.setText(newQuants[2]);
			// p3.setText(df.format(Float.valueOf(newUS[2])));
			// case 2:
			// sq2.setText(newQuants[1]);
			// p2.setText(df.format(Float.valueOf(newUS[1])));
			// case 1:
			// sq1.setText(newQuants[0]);
			// p1.setText(df.format(Float.valueOf(newUS[0])));
			// }
			// } else {
			// newPriceLayout.setVisibility(GONE);
			// }

		}
		else if (i == 2)
		{ // CA pricing
			arrowUS.setVisibility(GONE);
			arrowCA.setVisibility(VISIBLE);
			txtPricing.setText("Pricing, Canadian Dollars");
			switch (stdCA.length)
			{
				case 5:
					st5.setText("$" + df.format(Float.valueOf(stdCA[4])));
				case 4:
					st4.setText("$" + df.format(Float.valueOf(stdCA[3])));
				case 3:
					st3.setText("$" + df.format(Float.valueOf(stdCA[2])));
				case 2:
					st2.setText("$" + df.format(Float.valueOf(stdCA[1])));
				case 1:
					st1.setText("$" + df.format(Float.valueOf(stdCA[0])));
			}
			if (splCA != null)
			{
				switch (splCA.length)
				{
					case 5:
						sp5.setText("$" + df.format(Float.valueOf(splCA[4])));
					case 4:
						sp4.setText("$" + df.format(Float.valueOf(splCA[3])));
					case 3:
						sp3.setText("$" + df.format(Float.valueOf(splCA[2])));
					case 2:
						sp2.setText("$" + df.format(Float.valueOf(splCA[1])));
					case 1:
						sp1.setText("$" + df.format(Float.valueOf(splCA[0])));
				}
			}
			// TODO fix
			// if (newCA != null) {
			// newPriceLayout.setVisibility(VISIBLE);
			// dc3.setText(newCode);
			// switch (newCA.length) {
			// case 5:
			// // TODO
			// sq5.setText(newQuants[4]);
			// p5.setText(df.format(Float.valueOf(newCA[4])));
			// case 4:
			// sq4.setText(newQuants[3]);
			// p4.setText(df.format(Float.valueOf(newCA[3])));
			// case 3:
			// sq3.setText(newQuants[2]);
			// p3.setText(df.format(Float.valueOf(newCA[2])));
			// case 2:
			// sq2.setText(newQuants[1]);
			// p2.setText(df.format(Float.valueOf(newCA[1])));
			// case 1:
			// sq1.setText(newQuants[0]);
			// p1.setText(df.format(Float.valueOf(newCA[0])));
			// }
			// } else {
			// newPriceLayout.setVisibility(GONE);
			// }
		}
	}

	private void showQuotePrices(int i)
	{
		if (prodNote.equals(""))
		{
			noteslayoutQ.setVisibility(GONE);
		}
		else
		{
			noteQ.setText(prodNote);
			noteslayoutQ.setVisibility(VISIBLE);
		}
		switch (stdQuants.length)
		{
			case 5:
				Qq5.setText(stdQuants[4]);
			case 4:
				Qq4.setText(stdQuants[3]);
			case 3:
				Qq3.setText(stdQuants[2]);
			case 2:
				Qq2.setText(stdQuants[1]);
			case 1:
				Qq1.setText(stdQuants[0]);
		}
		if (splUS != null)
		{
			spclLineQ.setVisibility(VISIBLE);
			txtExpiresQ.setVisibility(VISIBLE);
			Qst1.setPaintFlags(Qst1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			Qst2.setPaintFlags(Qst1.getPaintFlags());
			Qst3.setPaintFlags(Qst1.getPaintFlags());
			Qst4.setPaintFlags(Qst1.getPaintFlags());
			Qst5.setPaintFlags(Qst1.getPaintFlags());
			txtStandardQ.setPaintFlags(Qst1.getPaintFlags());
			txtExpiresQ.setText("Expires: " + splExpDate);
		}
		else
		{
			spclLineQ.setVisibility(GONE);
			txtExpiresQ.setVisibility(GONE);
		}

		if (i == 1)
		{ // US pricing
			arrowUSQ.setVisibility(VISIBLE);
			arrowCAQ.setVisibility(GONE);
			txtPricingQ.setText("Pricing, US Dollars");
			Qdc1.setText(stdCode);
			switch (stdUS.length)
			{
				case 5:
					Qst5.setText("$" + df.format(Float.valueOf(stdUS[4])));
				case 4:
					Qst4.setText("$" + df.format(Float.valueOf(stdUS[3])));
				case 3:
					Qst3.setText("$" + df.format(Float.valueOf(stdUS[2])));
				case 2:
					Qst2.setText("$" + df.format(Float.valueOf(stdUS[1])));
				case 1:
					Qst1.setText("$" + df.format(Float.valueOf(stdUS[0])));
			}
			if (splUS != null)
			{
				Qdc2.setText(splCode);
				switch (splUS.length)
				{
					case 5:
						Qsp5.setText("$" + df.format(Float.valueOf(splUS[4])));
					case 4:
						Qsp4.setText("$" + df.format(Float.valueOf(splUS[3])));
					case 3:
						Qsp3.setText("$" + df.format(Float.valueOf(splUS[2])));
					case 2:
						Qsp2.setText("$" + df.format(Float.valueOf(splUS[1])));
					case 1:
						Qsp1.setText("$" + df.format(Float.valueOf(splUS[0])));
				}
			}
			// TODO
			// if (newUS != null) {
			// newPriceLayoutQ.setVisibility(VISIBLE);
			// Qdc3.setText(newCode);
			// switch (newUS.length) {
			// case 5:
			// Qsq5.setText(newQuants[4]);
			// Qp5.setText(df.format(Float.valueOf(newUS[4])));
			// case 4:
			// Qsq4.setText(newQuants[3]);
			// Qp4.setText(df.format(Float.valueOf(newUS[3])));
			// case 3:
			// Qsq3.setText(newQuants[2]);
			// Qp3.setText(df.format(Float.valueOf(newUS[2])));
			// case 2:
			// Qsq2.setText(newQuants[1]);
			// Qp2.setText(df.format(Float.valueOf(newUS[1])));
			// case 1:
			// Qsq1.setText(newQuants[0]);
			// Qp1.setText(df.format(Float.valueOf(newUS[0])));
			// }
			// } else {
			// newPriceLayoutQ.setVisibility(GONE);
			// }

		}
		else if (i == 2)
		{ // CA pricing
			arrowUSQ.setVisibility(GONE);
			arrowCAQ.setVisibility(VISIBLE);
			txtPricingQ.setText("Pricing, Canadian Dollars");
			Qdc1.setText(stdCode);
			switch (stdCA.length)
			{
				case 5:
					Qst5.setText("$" + df.format(Float.valueOf(stdCA[4])));
				case 4:
					Qst4.setText("$" + df.format(Float.valueOf(stdCA[3])));
				case 3:
					Qst3.setText("$" + df.format(Float.valueOf(stdCA[2])));
				case 2:
					Qst2.setText("$" + df.format(Float.valueOf(stdCA[1])));
				case 1:
					Qst1.setText("$" + df.format(Float.valueOf(stdCA[0])));
			}
			if (splCA != null)
			{
				Qdc2.setText(splCode);
				switch (splCA.length)
				{
					case 5:
						Qsp5.setText("$" + df.format(Float.valueOf(splCA[4])));
					case 4:
						Qsp4.setText("$" + df.format(Float.valueOf(splCA[3])));
					case 3:
						Qsp3.setText("$" + df.format(Float.valueOf(splCA[2])));
					case 2:
						Qsp2.setText("$" + df.format(Float.valueOf(splCA[1])));
					case 1:
						Qsp1.setText("$" + df.format(Float.valueOf(splCA[0])));
				}
			}
			// if (newCA != null) {
			// newPriceLayoutQ.setVisibility(VISIBLE);
			// Qdc3.setText(newCode);
			// switch (newCA.length) {
			// case 5:
			// Qsq5.setText(newQuants[4]);
			// Qp5.setText(df.format(Float.valueOf(newCA[4])));
			// case 4:
			// Qsq4.setText(newQuants[3]);
			// Qp4.setText(df.format(Float.valueOf(newCA[3])));
			// case 3:
			// Qsq3.setText(newQuants[2]);
			// Qp3.setText(df.format(Float.valueOf(newCA[2])));
			// case 2:
			// Qsq2.setText(newQuants[1]);
			// Qp2.setText(df.format(Float.valueOf(newCA[1])));
			// case 1:
			// Qsq1.setText(newQuants[0]);
			// Qp1.setText(df.format(Float.valueOf(newCA[0])));
			// }
			// } else {
			// newPriceLayoutQ.setVisibility(GONE);
			// }
		}
	}

	private class Estimator extends AsyncTask<String, String, String>
	{
		GoogleProgressDialog dialog;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params)
		{
			try
			{
				SoapObject request = new SoapObject(NAMESPACE, FRIEGHT_ESTIMATOR_METHOD);
				request.addProperty("ProductCode", product.getProductCode());
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				try
				{
					HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
					androidHttpTransport.call(FRIEGHT_ESTIMATOR_SOAP_ACTION, envelope);
					SoapObject result = (SoapObject) envelope.bodyIn;
					if (result != null)
					{
						return result.getProperty(0).toString();
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			try
			{
				JSONObject _obj = new JSONObject(result);
				if (_obj != null)
				{
					JSONArray _jarray = _obj.optJSONArray("Freight");
					if (_jarray != null & _jarray.length() > 0)
					{
						String _link = _jarray.optJSONObject(0).optString("Link");
						if (_link != null && _link.length() > 0)
						{
							web_view.setVisibility(VISIBLE);
							System.out.println(_link);
							web_view.loadUrl(_link);
						}
						else
						{
							web_view.setVisibility(View.INVISIBLE);
							txtNolink.setVisibility(VISIBLE);
							progress.setVisibility(GONE);
						}
					}
				}
			}
			catch (Exception e)
			{
				web_view.setVisibility(View.INVISIBLE);
				txtNolink.setVisibility(VISIBLE);
				progress.setVisibility(GONE);
				e.printStackTrace();
			}
			dialog.dismiss();
		}
	}

	private class AakronlineWebViewClient extends WebViewClient
	{
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)
		{
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url)
		{
			progress.setVisibility(GONE);
			ProdDetail.this.progress.setProgress(100);
			super.onPageFinished(view, url);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon)
		{
			progress.setVisibility(VISIBLE);
			ProdDetail.this.progress.setProgress(0);
			super.onPageStarted(view, url, favicon);
		}
	}

	private class ProductDetails extends AsyncTask<String, String, String>
	{

		private static final String SOAP_ACTION = "http://tempuri.org/GetProductData";

		private static final String METHOD_NAME = "GetProductData";

		GoogleProgressDialog dialog;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params)
		{
			try
			{
				_chargeList.clear();
				_inventoryList.clear();
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				String pid = DataHolderClass.getInstance().get_pId();
				request.addProperty("ProductCode", pid);
				request.addProperty("userId", userID);
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null && result.getPropertyCount() > 0)
				{
					String _result = result.getProperty(0).toString();
					Log.v("Product Detail", _result);
					JSONObject obj = new JSONObject(_result);
					if (obj != null)
					{
						JSONArray arr = obj.optJSONArray("ProductData");
						if (arr != null && arr.length() > 0)
						{
							product = new Product();
							product.setID(arr.optJSONObject(0).optString("productCode"));
							product.setName(arr.optJSONObject(0).optString("ProductName"));
							product.setPrice(arr.optJSONObject(0).optString("AsLowAsPrice"));
							product.setImageURL(arr.optJSONObject(0).optString("ImageUrl"));
							product.setDesc(arr.optJSONObject(0).optString("productDescription"));
							vidURL = arr.optJSONObject(0).optString("MultimediaContent");
							spinnerValues = arr.optJSONObject(0).optString("ItemColors").split("\\|");
							if (spinnerValues != null)
							{
								// Remove spaces
								for (int i = 0; i < spinnerValues.length; i++)
								{
									spinnerValues[i] = spinnerValues[i].trim();
								}
							}
						}
						JSONArray _ChargesArray = obj.optJSONArray("Charges");
						if (_ChargesArray != null && _ChargesArray.length() > 0)
						{
							for (int i = 0; i < _ChargesArray.length(); i++)
							{
								JSONObject _chargeObj = _ChargesArray.optJSONObject(i);
								ProductDetailModel model = new ProductDetailModel();
								model.setName(_chargeObj.optString("chargeName"));
								model.setText(_chargeObj.optString("chargeText"));
								_chargeList.add(model);
							}
						}
						JSONArray _DetailsArray = obj.optJSONArray("Details");
						if (_DetailsArray != null && _DetailsArray.length() > 0)
						{
							for (int j = 0; j < _DetailsArray.length(); j++)
							{
								JSONObject _detailsObj = _DetailsArray.optJSONObject(j);
								ProductDetailModel _model1 = new ProductDetailModel();
								String _Name = _detailsObj.optString("detailName");
								String _Text = _detailsObj.optString("detailText");
								_model1.setName(_Name);
								_model1.setText(_Text);
								_DetailsList.add(_model1);
							}
						}
						JSONArray _OptionsArray = obj.optJSONArray("Options");
						if (_OptionsArray != null && _OptionsArray.length() > 0)
						{
							for (int k = 0; k < _OptionsArray.length(); k++)
							{
								JSONObject _optionObj = _OptionsArray.optJSONObject(k);
								ProductDetailModel _model2 = new ProductDetailModel();
								String _optionName = _optionObj.optString("optionName");
								String _optiontext = _optionObj.optString("optiontext");
								_model2.setName(_optionName);
								_model2.setText(_optiontext);
								_OptionsList.add(_model2);
							}
						}

						// PRICING DATA
						int stdPrices = 0, splPrices = 0, newPrices = 0;
						JSONArray prices = obj.optJSONArray("Pricing");
						if (prices != null && prices.length() > 0)
						{
							stdPrices = Integer.valueOf(prices.getJSONObject(0).getString("RowCount1"));
							splPrices = Integer.valueOf(prices.getJSONObject(0).getString("RowCount2"));
							newPrices = Integer.valueOf(prices.getJSONObject(0).getString("RowCount3"));
							System.out.println(stdPrices + " " + splPrices + " " + newPrices);
						}
						if (stdPrices > 0)
						{
							stdQuants = new String[stdPrices];
							stdUS = new String[stdPrices];
							stdCA = new String[stdPrices];
							JSONArray stdPrice = obj.optJSONArray("PriceData1");
							for (int i = 0; i < stdPrices; i++)
							{
								JSONObject ob = stdPrice.optJSONObject(i);
								stdCode = ob.getString("discountCode");
								stdQuants[i] = ob.getString("Quantity");
								stdUS[i] = ob.getString("USPrice");
								stdCA[i] = ob.getString("CanadianPrice");
								prodNote = ob.getString("noteText");
							}
							if (splPrices > 0)
							{
								splQuants = new String[splPrices];
								splUS = new String[splPrices];
								splCA = new String[splPrices];
								JSONArray splPrice = obj.getJSONArray("PriceData2");
								for (int i = 0; i < splPrices; i++)
								{
									JSONObject ob = splPrice.optJSONObject(i);
									splCode = ob.getString("discountCode");
									splQuants[i] = ob.getString("Quantity");
									splUS[i] = ob.getString("USPrice");
									splCA[i] = ob.getString("CanadianPrice");
									splExpDate = ob.getString("expirationDate").split(" ")[0];
								}
							}
						}
						if (newPrices > 0)
						{
							newQuants = new String[newPrices];
							newUS = new String[newPrices];
							newCA = new String[newPrices];
							JSONArray newPrice = obj.optJSONArray("PriceData3");
							for (int i = 0; i < newPrices; i++)
							{
								JSONObject ob = newPrice.optJSONObject(i);
								newCode = ob.getString("discountCode");
								newQuants[i] = ob.getString("Quantity");
								newUS[i] = ob.getString("USPrice");
								newCA[i] = ob.getString("CanadianPrice");
							}
						}
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				dialog.dismiss();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			dialog.dismiss();
			try
			{
				final DecimalFormat df = new DecimalFormat();
				df.setMinimumFractionDigits(2);
				df.setMaximumFractionDigits(3);
				AQuery aq = new AQuery(getActivity());
				aq.id(imgProd).image(product.getImageURL());
				txtProdID.setText(product.getProductCode());
				txtProdName.setText(product.getName());
				if (product.getDesc().equals(""))
				{
					btnFeatures.setVisibility(GONE);
				}
				else
				{
					btnFeatures.setVisibility(VISIBLE);
					btnFeatures.setOnClickListener(ProdDetail.this);
				}
				float price = 0f;
				try
				{
					price = Float.valueOf(product.getPrice().substring(1));
				}
				catch (Exception e)
				{
					price = 0f;
				}
				txtProdPrice.setText("$" + df.format(price));
				if (stdQuants != null)
				{
					showDetailsPrices(1);
					showQuotePrices(1);
				}
				else
				{
					pricingLayout.setVisibility(GONE);
					pricingLayoutQ.setVisibility(GONE);
				}

				if (vidURL == null || vidURL.equals(""))
				{
					btnVideo.setVisibility(GONE);
				}
				else
				{
					btnVideo.setVisibility(VISIBLE);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	private class CheckInventory extends AsyncTask<String, String, String>
	{

		GoogleProgressDialog _dialog;

		String _part;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			_dialog = new GoogleProgressDialog(getActivity());
			_dialog.setCancelable(false);
			_dialog.show();
		}

		@Override
		protected String doInBackground(String... params)
		{
			try
			{
				_inventoryList.clear();
				SoapObject request = new SoapObject(NAMESPACE, CHECK_INVENTORY_METHOD);
				String code = product.getProductCode();
				if (code.contains("-"))
				{
					code = code.split("-")[1];
				}
				request.addProperty("ProductCode", code);
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				try
				{
					HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
					androidHttpTransport.call(CHECK_INVENTORY_SOAP_ACTION, envelope);
					SoapObject result = (SoapObject) envelope.bodyIn;
					if (result != null)
					{
						String _data = result.getProperty(0).toString();
						Log.v("data", _data);
						JSONObject _obj = new JSONObject(_data);
						if (_obj != null)
						{
							JSONArray arr = _obj.optJSONArray("Response");
							if (arr != null & arr.length() > 0)
							{
								String respCode = arr.optJSONObject(0).optString("ResponseCode");
								if (respCode.equalsIgnoreCase("0"))
								{
									JSONArray _array = _obj.optJSONArray("Details");
									for (int i = 0; i < _array.length(); i++)
									{
										JSONObject obj = _array.optJSONObject(i);
										InventoryModel _model = new InventoryModel();
										_model.setColorName(obj.optString("ColorName"));
										_model.setInventoryAvailable(obj.optString("InventoryAvailable"));
										_inventoryList.add(_model);
									}
									JSONArray _jrray = _obj.optJSONArray("Inventory");
									_part = _jrray.optJSONObject(0).optString("Part");

								}
								else if (respCode.equalsIgnoreCase("1"))
								{

								}
								else if (respCode.equalsIgnoreCase("2"))
								{
									_part = arr.optJSONObject(0).optString("msg");
								}

							}
						}
					}
					else
					{
						System.out.println("error");
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			try
			{
				set_part.setText(Html.fromHtml("<b>Part:</b> " + _part));
				if (_inventoryList.size() > 0)
				{
					InventoryAdapter _adap = new InventoryAdapter(getActivity(), _inventoryList);
					_adap.notifyDataSetChanged();
					invenListView.setAdapter(_adap);
				}
				else
				{
					invenItems.setVisibility(View.GONE);
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			_dialog.dismiss();
		}
	}

	private class RequestSample extends AsyncTask<Void, Void, String>
	{

		private static final String SOAP_ACTION = "http://tempuri.org/SetSampleRequest";

		private static final String METHOD_NAME = "SetSampleRequest";

		GoogleProgressDialog dialog;

		JSONObject obj;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params)
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("Productcode", product.getProductCode());
			request.addProperty("ImprintMethod", cbFCD.isChecked() ? cbFCD.getText() : cbSpot.getText());
			request.addProperty("Colors", spinColors.getSelectedItem().toString());
			request.addProperty("Quantity", etQuant.getText().toString().trim());
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try
			{
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				Log.v("RESP", envelope.bodyIn.toString());
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null)
				{
					String data = result.getProperty(0).toString();
					Log.d("result", result.toString());
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			}
			catch (UnknownHostException e)
			{
				e.printStackTrace();
				return "No Internet Connection";
			}
			catch (SocketTimeoutException e)
			{
				e.printStackTrace();
				return "Unable to reach server";
			}
			catch (JSONException e)
			{
				e.printStackTrace();
				return "Invalid response from server";
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			dialog.dismiss();
			try
			{
				if (result != null)
				{
					showToast(result);
					return;
				}
				if (obj.getJSONArray("Data").getJSONObject(0).getString("responseCode").equals("0"))
				{
					// success
					showToast("Sample Request posted successfully");
					etQuant.setText("");
				}
				else
				{
					showToast("Error Occured: Request not submitted");
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				showToast("Error Occured: Request not submitted");
			}

		}

	}

	public class RequestQuote extends AsyncTask<String, Void, String>
	{

		private static final String SOAP_ACTION = "http://tempuri.org/SetRequestQuote";

		private static final String METHOD_NAME = "SetRequestQuote";

		GoogleProgressDialog dialog;

		JSONObject obj;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params)
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("Productcode", product.getProductCode());
			request.addProperty("ImprintMethod", cbFCDQ.isChecked() ? cbFCDQ.getText() : cbSpotQ.getText());
			request.addProperty("Colors", spinColorsQ.getSelectedItem().toString());
			request.addProperty("Quantity", params[0]);
			request.addProperty("Info", etInfo.getText().toString());
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try
			{
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null)
				{
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			}
			catch (UnknownHostException e)
			{
				e.printStackTrace();
				return "No Internet Connection";
			}
			catch (SocketTimeoutException e)
			{
				e.printStackTrace();
				return "Unable to reach server";
			}
			catch (JSONException e)
			{
				e.printStackTrace();
				return "Invalid response from server";
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			dialog.dismiss();
			try
			{
				if (result != null)
				{
					showToast(result);
					return;
				}
				if (obj.getJSONArray("Data").getJSONObject(0).getString("responseCode").equals("0"))
				{
					// success
					showToast("Quote Request posted successfully");
					etQuantQ.setText("");
					etQuantQ2.setText("");
					etQuantQ3.setText("");
					etQuantQ4.setText("");
					etQuantQ5.setText("");
				}
				else
				{
					showToast("Error Occured: Request not submitted");
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				showToast("Error Occured: Request not submitted");
			}

		}

	}
}
