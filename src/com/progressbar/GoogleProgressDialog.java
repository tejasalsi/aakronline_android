package com.progressbar;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;

import com.akronline.R;

public class GoogleProgressDialog extends Dialog {

	Context ctx;
	View v;

	public GoogleProgressDialog(Context context) {
		super(context);
		this.ctx = context;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setCancelable(false);
		this.setCanceledOnTouchOutside(false);
		setContentView(R.layout.progress_var);
		v = getWindow().getDecorView();
		v.setBackgroundResource(android.R.color.transparent);
	}

}
