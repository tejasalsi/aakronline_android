package com.akronline.dialogs;

import static com.akronline.dialogs.RepMeeting.salesRepData;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.R;
import com.akronline.interfaces.DialogCallback;
import com.datamodel.RepData;

public class StateChooser extends Dialog implements View.OnClickListener {

	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
	private static final String NAMESPACE = "http://tempuri.org/";

	Context ctx;
	String userID;
	int mode;
	Spinner spinStates;
	Button btnDone;
	TextView txtTitle;
	FrameLayout container;
	private ArrayList<String> states = new ArrayList<String>();
	DialogCallback callback;

	public StateChooser(Context ctx, String userID, int mode) {
		super(ctx);
		this.ctx = ctx;
		this.userID = userID;
		this.mode = mode;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().getDecorView().setBackgroundResource(R.drawable.white_bg);
		setContentView(R.layout.state_dialog);
		txtTitle = (TextView) findViewById(R.id.txtTitle);
		container = (FrameLayout) findViewById(R.id.spinContainer);
		spinStates = (Spinner) findViewById(R.id.spinState);
		btnDone = (Button) findViewById(R.id.btnDone);
		btnDone.setOnClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.e("dTheme", getThemeName());
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		new GetRepData().execute();
	}

	public String getThemeName() {
		PackageInfo packageInfo;
		try {
			packageInfo = this
					.getContext()
					.getPackageManager()
					.getPackageInfo(this.getContext().getPackageName(),
							PackageManager.GET_META_DATA);
			int themeResId = packageInfo.applicationInfo.theme;
			return this.getContext().getResources()
					.getResourceEntryName(themeResId);
		} catch (NameNotFoundException e) {
			return null;
		}
	}

	@Override
	public void onClick(View v) {
		for (RepData rep : salesRepData) {
			if (rep.getStateName().equals(
					spinStates.getSelectedItem().toString())) {
				Log.v("Chooser", spinStates.getSelectedItem().toString()
						+ " matched. storing repID:" + rep.getSalesRepId());
				SharedPreferences.Editor editor = ctx.getApplicationContext()
						.getSharedPreferences("LoginPref", 0).edit();
				editor.putString("REP-ID", rep.getSalesRepId());
				Log.e("REP", "storing repID:" + rep.getSalesRepId());
				editor.commit();
				/*if (mode == 1) {
					showToast("State is selected. Now you can request Rep Meeting");
				} else if (mode == 2) {
					showToast("State is selected. Now you can request Webinar");
				}*/
				if(callback!= null){
					callback.onDialogSubmit(mode, spinStates.getSelectedItem().toString());
				}
				StateChooser.this.dismiss();
				break;
			}
		}
	}

	private void showToast(String msg) {
		Toast.makeText(ctx.getApplicationContext(), msg, Toast.LENGTH_LONG)
				.show();
	}

	private class GetRepData extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "GetSalesRepData";
		private static final String SOAP_ACTION = "http://tempuri.org/GetSalesRepData";
		JSONObject ob;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			txtTitle.setText(Html
					.fromHtml("<font color=red>Please wait...</font>"));
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					ob = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result != null) {
				showToast(result);
				StateChooser.this.onBackPressed();
				return;
			}
			try {
				txtTitle.setText("Select your state");
				container.setVisibility(View.VISIBLE);
				btnDone.setVisibility(View.VISIBLE);
				JSONArray arr = ob.getJSONArray("Data");
				salesRepData.clear();
				states.clear();
				for (int i = 0; i < arr.length(); i++) {
					JSONObject rep = arr.getJSONObject(i);
					RepData repData = new RepData();
					repData.setSalesRepId(rep.getString("SalesRepId"));
					repData.setRegionId(rep.getString("RegionId"));
					repData.setRFName(rep.getString("RFName"));
					repData.setRLName(rep.getString("RLName"));
					repData.setIFName(rep.getString("IFName"));
					repData.setILName(rep.getString("ILName"));
					repData.setREmail(rep.getString("REmail"));
					repData.setIEmail(rep.getString("IEmail"));
					repData.setRPhoneNo(rep.getString("RPhoneNo"));
					repData.setIPhoneNo(rep.getString("IPhoneNo"));
					repData.setRDesignation(rep.getString("RDesignation"));
					repData.setIDesignation(rep.getString("IDesignation"));
					repData.setRimage(rep.getString("Rimage"));
					repData.setIimage(rep.getString("Iimage"));
					repData.setRegionName(rep.getString("RegionName"));
					repData.setStateId(rep.getString("stateId"));
					repData.setStateName(rep.getString("stateName"));
					states.add(repData.getStateName());
					salesRepData.add(repData);
				}

				spinStates.setAdapter(new ArrayAdapter<String>(ctx,
						android.R.layout.simple_list_item_1,
						android.R.id.text1, states));
			} catch (Exception e) {
				e.printStackTrace();
				showToast("Exception: " + e.getMessage());
				StateChooser.this.onBackPressed();
			}
		}

	}
	public void setOnDialogSubmitListner(DialogCallback callback){
		this.callback = callback;
		
	}
}
