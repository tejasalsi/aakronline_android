package com.akronline;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.dialogs.EditProfile;
import com.akronline.dialogs.HelpUs;
import com.akronline.dialogs.OrderSample;
import com.akronline.dialogs.RepMeeting;
import com.akronline.dialogs.ReqWebinar;
import com.akronline.dialogs.RequestCatalog;
import com.akronline.dialogs.SearchByOrderDialoge;
import com.akronline.dialogs.StateChooser;
import com.akronline.interfaces.AdsCallback;
import com.akronline.interfaces.DialogCallback;
import com.akronline.mob.SearchByOrderFragment;
import com.commondata.CommonData;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.dataholder.DataHolderClass;
import com.progressbar.GoogleProgressDialog;

public class HomeTab extends FragmentActivity implements OnClickListener,
		AdsCallback, DialogCallback {

	private static String SOAP_ACTION = "http://tempuri.org/GetUserDetails";
	private static String NAMESPACE = "http://tempuri.org/";
	private static String METHOD_NAME = "GetUserDetails";
	private static String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	LinearLayout loginLayout, optLayout;
	EditText etEmail, etPass;
	TextView txtSample, txtMeet, txtCatalog, txtWebinar, txtHelp,
			txtEditProfile, searchByOrder;
	ImageView ivSnacks, ivNew, ivMood, ivbest, pick, point, exp;
	Button btnLogin;
	boolean isLogin = false;
	SharedPreferences prefs;
	String userID;
	String emaiID;
	String repID;
	EditProfile editProfileDialog;
	SliderLayout slider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("tag", "HomeTab");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.home_tab);

		prefs = getApplicationContext().getSharedPreferences("LoginPref", 0);
		userID = prefs.getString("USER-ID", "");
		emaiID = prefs.getString("USER-EMAIL", "");
		repID = prefs.getString("REP-ID", "0");

		if (userID.equals("") || emaiID.equals(""))
			isLogin = false;
		else
			isLogin = true;

		
		searchByOrder = (TextView) findViewById(R.id.searchByOrder);
		
		loginLayout = (LinearLayout) findViewById(R.id.loginLayout);
		optLayout = (LinearLayout) findViewById(R.id.options);
		txtCatalog = (TextView) findViewById(R.id.txtCatalog);
		txtHelp = (TextView) findViewById(R.id.txtHelp);
		txtMeet = (TextView) findViewById(R.id.txtReqMeet);
		txtSample = (TextView) findViewById(R.id.txtSample);
		txtEditProfile = (TextView) findViewById(R.id.txtEdit);
		txtWebinar = (TextView) findViewById(R.id.txtWebinar);
		ivbest = (ImageView) findViewById(R.id.imgBest);
		ivMood = (ImageView) findViewById(R.id.imgMood);
		ivNew = (ImageView) findViewById(R.id.imgNew);
		ivSnacks = (ImageView) findViewById(R.id.imgSnacks);
		pick = (ImageView) findViewById(R.id.imgPick);
		point = (ImageView) findViewById(R.id.imgPoint);
		exp = (ImageView) findViewById(R.id.imgExp);
		btnLogin = (Button) findViewById(R.id.btnLogin);
		etEmail = (EditText) findViewById(R.id.etEmail);
		etPass = (EditText) findViewById(R.id.etPass);
		slider = (SliderLayout) findViewById(R.id.slider);
		slider.setDuration(6000);
		if (isLogin) {
			loginLayout.setVisibility(View.GONE);
			optLayout.setVisibility(View.VISIBLE);
			txtCatalog.setOnClickListener(this);
			txtHelp.setOnClickListener(this);
			txtMeet.setOnClickListener(this);
			txtSample.setOnClickListener(this);
			txtEditProfile.setOnClickListener(this);
			txtWebinar.setOnClickListener(this);
			new GetAds(this, userID).execute();
		}
		ivbest.setOnClickListener(this);
		ivMood.setOnClickListener(this);
		ivNew.setOnClickListener(this);
		ivSnacks.setOnClickListener(this);
		pick.setOnClickListener(this);
		point.setOnClickListener(this);
		exp.setOnClickListener(this);
		btnLogin.setOnClickListener(this);
		searchByOrder.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		repID = prefs.getString("REP-ID", "0");
		switch (view.getId()) {
		case R.id.btnLogin:
			if (etEmail.getText().toString().isEmpty()) {
				Toast.makeText(getApplicationContext(), "Email is required",
						Toast.LENGTH_LONG).show();
				return;
			}
			if (!etEmail.getText().toString()
					.matches(Patterns.EMAIL_ADDRESS.pattern())) {
				Toast.makeText(getApplicationContext(), "Enter valid email",
						Toast.LENGTH_LONG).show();
				return;
			}
			if (etPass.getText().toString().length() < 5) {
				Toast.makeText(getApplicationContext(),
						"Password length must be atleast 5", Toast.LENGTH_LONG)
						.show();
				return;
			}
			new CheckValidUser()
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			break;
		case R.id.imgExp:
		case R.id.imgPick:
		case R.id.imgPoint:
			if (!isLogin) {
				Toast.makeText(getApplicationContext(), "Please login first",
						Toast.LENGTH_SHORT).show();
				return;
			}
			startActivity(new Intent(this, SyncTab.class));
			overridePendingTransition(R.anim.enter_new_screen,
					R.anim.exit_old_screen);
			break;
		case R.id.imgBest:
			if (!isLogin) {
				Toast.makeText(getApplicationContext(),
						"Please login to view items", Toast.LENGTH_SHORT)
						.show();
				return;
			}
			DataHolderClass.getInstance().set_filterBy("Best Seller");
			DataHolderClass.getInstance().set_titleCatagory("BEST SELLERS");
			DataHolderClass.getInstance().set_catagoryName("bestsellers");
			startActivity(new Intent(this, ProductScreen.class));
			overridePendingTransition(R.anim.enter_new_screen,
					R.anim.exit_old_screen);
			break;
		case R.id.imgMood:
			if (!isLogin) {
				Toast.makeText(getApplicationContext(),
						"Please login to view items", Toast.LENGTH_SHORT)
						.show();
				return;
			}
			DataHolderClass.getInstance().set_filterBy("Mood Products");
			DataHolderClass.getInstance().set_titleCatagory("MOOD PRODUCTS");
			DataHolderClass.getInstance().set_catagoryName("moodproducts");
			startActivity(new Intent(this, ProductScreen.class));
			overridePendingTransition(R.anim.enter_new_screen,
					R.anim.exit_old_screen);
			break;
		case R.id.imgNew:
			if (!isLogin) {
				Toast.makeText(getApplicationContext(),
						"Please login to view items", Toast.LENGTH_SHORT)
						.show();
				return;
			}
			DataHolderClass.getInstance().set_filterBy("New Products");
			DataHolderClass.getInstance().set_titleCatagory("NEW PRODUCTS");
			DataHolderClass.getInstance().set_catagoryName("newproducts");
			startActivity(new Intent(this, ProductScreen.class));
			overridePendingTransition(R.anim.enter_new_screen,
					R.anim.exit_old_screen);
			break;
		case R.id.imgSnacks:
			if (!isLogin) {
				Toast.makeText(getApplicationContext(),
						"Please login to view items", Toast.LENGTH_SHORT)
						.show();
				return;
			}
			DataHolderClass.getInstance().set_filterBy("Snacks");
			DataHolderClass.getInstance().set_titleCatagory("SNACKS");
			DataHolderClass.getInstance().set_catagoryName("snacks");
			startActivity(new Intent(this, ProductScreen.class));
			overridePendingTransition(R.anim.enter_new_screen,
					R.anim.exit_old_screen);
			break;
		case R.id.txtCatalog:
			new RequestCatalog(this).show();
			break;
		case R.id.txtHelp:
			new HelpUs(this, userID).show();
			break;
		case R.id.txtReqMeet:
			if (repID.equals("0")) {
				StateChooser stateDialoge = new StateChooser(this, userID, 1);
				stateDialoge.setOnDialogSubmitListner(this);
				stateDialoge.show();
				//new StateChooser(this, userID, 1).show();
			} else
				new RepMeeting(this, userID, repID).show();
			break;
		case R.id.txtSample:
			new OrderSample(this, userID).show();
			break;
		case R.id.txtEdit:
			new EditProfile(this, userID).show();
			break;
		case R.id.txtWebinar:
			if (repID.equals("0")) {
				StateChooser stateDialoge = new StateChooser(this, userID, 2);
				stateDialoge.setOnDialogSubmitListner(this);
				stateDialoge.show();
				//new StateChooser(this, userID, 2).show();
			} else
				new ReqWebinar(this).show();
			break;
			
		case R.id.searchByOrder:
			Log.e("tag", " clicked");
			new SearchByOrderDialoge(this, userID).show();
			break;
		}

	}

	/*************************** LOGIN WORK *********************/
	private class CheckValidUser extends AsyncTask<String, String, String> {

		GoogleProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(HomeTab.this);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserName", etEmail.getText().toString());
			request.addProperty("Password", etPass.getText().toString());
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				Log.v("RESP", envelope.bodyIn.toString());
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String _data = result.getProperty(0).toString();
					Log.v("data", _data);
					JSONObject _obj = new JSONObject(_data);
					if (_obj != null) {
						JSONArray _jarray = _obj.optJSONArray("Data");
						if (_jarray != null & _jarray.length() > 0) {
							String _responseCode = _jarray.optJSONObject(0)
									.optString("responseCode");
							if (_responseCode.equalsIgnoreCase("0")) {
								userID = _jarray.optJSONObject(0).optString(
										"UserId");
								emaiID = _jarray.optJSONObject(0).optString(
										"user_email");
								// TODO get n set REPID
							} else {
								// TODO invalid password
							}
						}
					}
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (result != null) {
					Toast.makeText(getApplicationContext(), result,
							Toast.LENGTH_SHORT).show();
					return;
				} else if (emaiID != null && userID != null
						&& emaiID.length() > 0 && userID.length() > 0) {
					loginLayout.setVisibility(View.GONE);
					optLayout.setVisibility(View.VISIBLE);
					txtCatalog.setOnClickListener(HomeTab.this);
					txtHelp.setOnClickListener(HomeTab.this);
					txtMeet.setOnClickListener(HomeTab.this);
					txtSample.setOnClickListener(HomeTab.this);
					txtEditProfile.setOnClickListener(HomeTab.this);
					txtWebinar.setOnClickListener(HomeTab.this);

					Editor prefsEditor = prefs.edit();
					prefsEditor.putString("USER-ID", userID);
					prefsEditor.putString("USER-EMAIL", emaiID);
					prefsEditor.putString("REP-ID", repID);
					prefsEditor.commit();
					isLogin = true;
					Toast.makeText(getApplicationContext(), "Login successful",
							Toast.LENGTH_LONG).show();
					if (CommonData.adsList.size() == 0) {
						new GetAds(HomeTab.this, userID).execute();
					} else {
						onAdsLoaded();
					}
				} else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_password),
							Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getApplicationContext(), "Error occured",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onAdsLoaded() {
		for (String ad : CommonData.adsList) {
			DefaultSliderView sv = new DefaultSliderView(this);
			sv.image(ad);
			slider.addSlider(sv);
		}
	}

	@Override
	public void onDialogSubmit(int requestCode, String result) {
		repID = prefs.getString("REP-ID", "0");
		if(requestCode == 1){
			new RepMeeting(this, userID, repID).show();
		}else {
			new ReqWebinar(this).show();
		}			
	}

}
