package com.datamodel;

public class InventoryModel {
	public String getColorName() {
		return ColorName;
	}
	public void setColorName(String colorName) {
		ColorName = colorName;
	}
	public String getInventoryAvailable() {
		return InventoryAvailable;
	}
	public void setInventoryAvailable(String inventoryAvailable) {
		InventoryAvailable = inventoryAvailable;
	}
	String ColorName;
	String InventoryAvailable;
}
