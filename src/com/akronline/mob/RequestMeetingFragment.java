package com.akronline.mob;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.akronline.R;
import com.androidquery.AQuery;
import com.datamodel.RepData;
import com.progressbar.GoogleProgressDialog;

public class RequestMeetingFragment extends BaseFragment implements OnClickListener{
	

	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
	private static final String NAMESPACE = "http://tempuri.org/";

	public static ArrayList<RepData> salesRepData = new ArrayList<RepData>();
	Context ctx;
	String userID, repID;
	ImageView imgRep;
	TextView txtRepName, txtRepName2, txtRepDetails;
	EditText  etNotes;
	Button btnSubmit, btnCancel,etDay, etTime;
	

	boolean isLogin = false;
	String emaiID;
	SharedPreferences prefs;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View _view= inflater.inflate(R.layout.request_meeting_fragment, null); 
		Log.e("tag", "RequestMeetingFragment");
		prefs = getActivity().getSharedPreferences("LoginPref", 0);
		userID = prefs.getString("USER-ID", "");
		emaiID = prefs.getString("USER-EMAIL", "");
		repID = prefs.getString("REP-ID", "0");
		
		// TODO Auto-generated method stub
		imgRep = (ImageView) _view.findViewById(R.id.imgRep);
		txtRepDetails = (TextView) _view.findViewById(R.id.txtRepDetails);
		txtRepName = (TextView) _view.findViewById(R.id.txtRepName);
		txtRepName2 = (TextView) _view.findViewById(R.id.txtRepName2);
		etDay = (Button) _view.findViewById(R.id.etDay);
		etTime = (Button) _view.findViewById(R.id.etTime);
		etNotes = (EditText) _view.findViewById(R.id.etNotes);
		btnSubmit = (Button) _view.findViewById(R.id.btnSubmit);
		btnCancel = (Button) _view.findViewById(R.id.btnCancel);

		btnCancel.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
		etDay.setOnClickListener(this);
		etTime.setOnClickListener(this);
		
		final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(_view.getWindowToken(), 0);
	    
	    
		return _view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		if (salesRepData.isEmpty())
			new GetRepData().execute();
		else
			for (RepData repData : salesRepData){
				if (repData.getSalesRepId().equals(repID)) {
					AQuery aq = new AQuery(getActivity());
					aq.id(imgRep).image(repData.getRimage());
					txtRepName.setText(repData.getRFName() + " "
							+ repData.getRLName());
					txtRepName2.setText(repData.getRFName());
					txtRepDetails.setText(repData.getRDesignation() + "\n"
							+ repData.getREmail() + "\nPh: "
							+ repData.getRPhoneNo());
					break;
				}
				
			}
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnCancel:
				getActivity().onBackPressed();
				break;
			case R.id.btnSubmit:
				if (etDay.getText().toString().isEmpty()) {
					showToast("Select meeting date");
					return;
				}
				if (etTime.getText().toString().isEmpty()) {
					showToast("Select meeting time");
					return;
				}
				new SetRepMeeting().execute();
				break;
			case R.id.etDay:
				Calendar cal = Calendar.getInstance();
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				Log.v("year", "year " + year);
				if (year == 2014) {
					day = 1;
					month = 0;
					year = 2015;
				}
				cal.setTimeInMillis(System.currentTimeMillis());
				DatePickerDialog dpd = new DatePickerDialog(getActivity(), dateSet, day,
						month, year);
				dpd.getDatePicker().setMinDate(System.currentTimeMillis());
				dpd.show();
				break;
			case R.id.etTime:
				new TimePickerDialog(getActivity(), timeSet, Calendar.getInstance().get(
						Calendar.HOUR_OF_DAY), Calendar.getInstance().get(
						Calendar.MINUTE), true).show();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception: " + e.getMessage());
		}
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
//			String title = DataHolderClass.getInstance().get_titleCatagory();
			((HomeMob) getActivity()).setTitle("Request Meeting");
		} catch (Exception e) {
			e.printStackTrace();
		}
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.search.setVisibility(View.GONE);
	}
	
	private static final DecimalFormat df = new DecimalFormat("00");

	private OnDateSetListener dateSet = new OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			etDay.setText((df.format(++monthOfYear)) + "/"
					+ (df.format(dayOfMonth)) + "/" + year);
		}
	};

	private OnTimeSetListener timeSet = new OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			etTime.setText((df.format(hourOfDay)) + ":" + (df.format(minute)));
		}
	};

	private void showToast(String msg) {
		Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG)
				.show();
	}

	private class GetRepData extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "GetSalesRepData";
		private static final String SOAP_ACTION = "http://tempuri.org/GetSalesRepData";
		JSONObject ob;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			txtRepName.setText(Html
					.fromHtml("<font color=red>Please wait...</font>"));
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					ob = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result != null) {
				showToast(result);
				getActivity().onBackPressed();
				return;
			}
			try {
				JSONArray arr = ob.getJSONArray("Data");
				salesRepData.clear();
				for (int i = 0; i < arr.length(); i++) {
					JSONObject rep = arr.getJSONObject(i);
					RepData repData = new RepData();
					repData.setSalesRepId(rep.getString("SalesRepId"));
					repData.setRegionId(rep.getString("RegionId"));
					repData.setRFName(rep.getString("RFName"));
					repData.setRLName(rep.getString("RLName"));
					repData.setIFName(rep.getString("IFName"));
					repData.setILName(rep.getString("ILName"));
					repData.setREmail(rep.getString("REmail"));
					repData.setIEmail(rep.getString("IEmail"));
					repData.setRPhoneNo(rep.getString("RPhoneNo"));
					repData.setIPhoneNo(rep.getString("IPhoneNo"));
					repData.setRDesignation(rep.getString("RDesignation"));
					repData.setIDesignation(rep.getString("IDesignation"));
					repData.setRimage(rep.getString("Rimage"));
					repData.setIimage(rep.getString("Iimage"));
					repData.setRegionName(rep.getString("RegionName"));
					repData.setStateId(rep.getString("stateId"));
					repData.setStateName(rep.getString("stateName"));
					salesRepData.add(repData);
				}
				for (RepData repData : salesRepData) {
					if (repData.getSalesRepId().equals(repID)) {
						AQuery aq = new AQuery(getActivity());
						aq.id(imgRep).image(repData.getRimage());
						txtRepName.setText(repData.getRFName() + " "
								+ repData.getRLName());
						txtRepName2.setText(repData.getRFName());
						txtRepDetails.setText(repData.getRDesignation() + "\n"
								+ repData.getREmail() + "\nPh: "
								+ repData.getRPhoneNo());
						break;
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				showToast("Exception: " + e.getMessage());
				getActivity().onBackPressed();
			}
		}
	}

	private class SetRepMeeting extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "RequestSalesRepMeeting";
		private static final String SOAP_ACTION = "http://tempuri.org/RequestSalesRepMeeting";
		GoogleProgressDialog dialog;
		String response = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("SalesRepId", repID);
			request.addProperty("Day", etDay.getText().toString());
			request.addProperty("Time", etTime.getText().toString());
			request.addProperty("Notes", etNotes.getText().toString());
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					// {"Data":[{"responseCode":"0"}]}
					response = new JSONObject(data).getJSONArray("Data")
							.getJSONObject(0).getString("responseCode");
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null) {
				showToast(result);
				return;
			}
			try {
				if (response.equals("0")) {
					// submitted
					showToast("Rep meeting request posted successfully");
					getActivity().onBackPressed();
				} else
					showToast("Error: Request not posted");
			} catch (Exception e) {
				e.printStackTrace();
				showToast("Exception: " + e.getMessage());
			}
		}

	}

}
