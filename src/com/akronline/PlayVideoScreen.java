package com.akronline;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.mob.HomeMob;
import com.akronline.mob.MobileProductListing;
import com.akronline.mob.ScanMob;
import com.commondata.CommonData;
import com.dataholder.DataHolderClass;
import com.datamodel.Product;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

public class PlayVideoScreen extends YouTubeBaseActivity implements
		YouTubePlayer.OnInitializedListener,
		YouTubePlayer.PlaybackEventListener, OnClickListener {

	public static Product scanProd;
	String vidID;
	YouTubePlayerView youTubePlayerView;
	LinearLayout vdoBtns;
	TextView back, replay, detail;
	YouTubePlayer player;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("tag", "PlayVideoScreen");
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		vidID = DataHolderClass.getInstance().get_videoId();
		setContentView(R.layout.activity_play_video_screen);
		youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
		vdoBtns = (LinearLayout) findViewById(R.id.vdoBtns);
		back = (TextView) findViewById(R.id.btnBack);
		replay = (TextView) findViewById(R.id.btnReplay);
		detail = (TextView) findViewById(R.id.btnDetail);

		back.setOnClickListener(this);
		replay.setOnClickListener(this);
		detail.setOnClickListener(this);

		if (ProductScreen.fromScan) {
			detail.setVisibility(View.VISIBLE);
		} else {
			detail.setVisibility(View.GONE);
		}

		youTubePlayerView.initialize(CommonData.YOUTUBE_API_KEY, this);
	}

	@Override
	public void onInitializationFailure(Provider _youtubeprovider,
			YouTubeInitializationResult _result) {
		Toast.makeText(getApplicationContext(),
				"Failed to initialize YouTube player", Toast.LENGTH_LONG)
				.show();
	}

	@Override
	public void onInitializationSuccess(Provider provider,
			YouTubePlayer player, boolean bool) {
		this.player = player;
		player.setPlayerStyle(PlayerStyle.MINIMAL);
		player.setPlayerStateChangeListener(playerStateChangeListener);
		player.setPlaybackEventListener(this);
		if (!bool) {
			player.loadVideo(vidID);
		}
	}

	private void showControls() {
		vdoBtns.setVisibility(View.VISIBLE);
	}

	private PlayerStateChangeListener playerStateChangeListener = new PlayerStateChangeListener() {

		@Override
		public void onAdStarted() {
		}

		@Override
		public void onError(ErrorReason arg0) {
		}

		@Override
		public void onLoaded(String arg0) {
		}

		@Override
		public void onLoading() {
		}

		@Override
		public void onVideoEnded() {
			showControls();
		}

		@Override
		public void onVideoStarted() {
			vdoBtns.setVisibility(View.GONE);
		}
	};

	@Override
	public void onBuffering(boolean arg0) {
	}

	@Override
	public void onPaused() {
		showControls();
	}

	@Override
	public void onPlaying() {
		vdoBtns.setVisibility(View.GONE);
	}

	@Override
	public void onSeekTo(int arg0) {
	}

	@Override
	public void onStopped() {
		showControls();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnBack:
			finish();
			break;
		case R.id.btnDetail:
			ProductScreen.fromYT = true;
			
			if (getString(R.string.screen_type).equals("Tablet")) {
				startActivity(new Intent(this, ProductScreen.class));
			} else {
				
				ProductScreen.fromScan = true;
				//startActivity(new Intent(ScanMob.this, ProductScreen.class));
				
				SharedPreferences preferences = getSharedPreferences(
						"app", android.content.Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putBoolean("isFromMobileProductList", true);
				editor.commit();
				MobileProductListing _listing = new MobileProductListing(true);
				Intent intent = new Intent(PlayVideoScreen.this, HomeMob.class);
				startActivity(intent);
				
			}
			
			
			
			/*ProductScreen.fromYT = true;
			startActivity(new Intent(this, ProductScreen.class));*/
			
//			MobileProductListing _listing = new MobileProductListing();
//			HomeMob mob = new HomeMob();
//			mob.letsHaveSomeTransactions(
//                     R.id.container, _listing, "Listing",null); 
			 
//			Intent intent = new Intent(PlayVideoScreen.this, HomeMob.class);
//			startActivity(intent);
			
			
			finish();
			break;
		case R.id.btnReplay:
			vdoBtns.setVisibility(View.GONE);
			player.play();
		}
	}
}
