package com.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akronline.R;

public class FilterAdapter extends BaseAdapter {
	Context ctx;
	ArrayList<String> list;
	ArrayList<String> listWithCount;
	HolderClass holder = null;
    boolean flag=false;
	public FilterAdapter(Context _ctx, ArrayList<String> _list, ArrayList<String> _listWithCount) {
		this.ctx = _ctx;
		this.list = _list;
		this.listWithCount = _listWithCount;
		
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if (row == null) {
			LayoutInflater inflater = ((Activity) ctx).getLayoutInflater();
			row = inflater.inflate(
					R.layout.request_sample_color_dropdown_inflator, parent,
					false);
			holder = new HolderClass();
			holder._name = (TextView) row.findViewById(R.id.textone);
			holder._textFilter = (TextView) row.findViewById(R.id.textFilter);

			row.setTag(holder);
		} else {
			holder = (HolderClass) row.getTag();
		}
		try {
			    holder._name.setText(listWithCount.get(position));
			    holder._textFilter.setText(list.get(position));
			    
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return row;
	}
	//*********************************************************************************************************//
	class HolderClass {
		TextView _name;
		TextView _textFilter;
	}
   //***********************************************************************************************************//
	public class ListDataModel {
		String category_name;
		Integer category_count;

		public ListDataModel(String name, Integer cnt) {
			category_name = name;
			category_count = cnt;
		}
	}
}