package com.akronline;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.ImageView;

import com.akronline.mob.HomeMob;

public class Splash extends Activity {

	Thread mThread;
	Intent i;
	ImageView img;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getString(R.string.screen_type).equals("Tablet")) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			i = new Intent(this, HomeTab.class);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			i = new Intent(this, HomeMob.class);
		}
		setContentView(R.layout.splash);
		img = (ImageView) findViewById(R.id.imgSplash);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (getString(R.string.screen_type).equals("Tablet")) {
			img.setBackgroundResource(R.drawable.splash_tab);
		} else
			img.setBackgroundResource(R.drawable.splash_potrait);
		mThread = new Thread() {
			public void run() {
				try {
					sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				startActivity(i);
				finish();
			}
		};
		mThread.start();
	}

	@Override
	public void onBackPressed() {
		// nothing :P
	}

}
