package com.controller;

import android.app.Application;

import com.androidquery.callback.BitmapAjaxCallback;

public class AppController extends Application {
	@Override
	public void onLowMemory() {
		BitmapAjaxCallback.clearCache();
	}
}
