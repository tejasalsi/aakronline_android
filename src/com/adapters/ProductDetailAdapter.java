package com.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akronline.R;
import com.datamodel.ProductDetailModel;

public class ProductDetailAdapter extends BaseAdapter {
	Context _ctx;
	ArrayList<ProductDetailModel> _list;
	HolderClass holder = null;

	public ProductDetailAdapter(Context ctx, ArrayList<ProductDetailModel> list) {
		this._ctx = ctx;
		this._list = list;
	}

	@Override
	public int getCount() {
		return _list.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if (row == null) {
			LayoutInflater inflater = ((Activity) _ctx).getLayoutInflater();
			row = inflater.inflate(R.layout.details_inflator, parent, false);
			holder = new HolderClass();
			holder.name = (TextView) row.findViewById(R.id.name);
			holder.desc = (TextView) row.findViewById(R.id.descriptions);

			row.setTag(holder);
		} else {
			holder = (HolderClass) row.getTag();
		}
		try {
			ProductDetailModel _model = _list.get(position);
			Spanned d = Html.fromHtml(_model.getName());
			Spanned dd = Html.fromHtml(_model.getText());
			holder.name.setText(d);
			holder.desc.setText(dd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return row;
	}

	class HolderClass {
		TextView name;
		TextView desc;

	}
}
