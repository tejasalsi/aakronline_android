package com.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.akronline.R;

public class SampleColorAdapter extends ArrayAdapter<String> {

	Context context;
	String[] spinnerValues;
	public SampleColorAdapter(Context ctx, int txtViewResourceId,
			String[] objects) {
		super(ctx, txtViewResourceId, objects);
		context = ctx;
		spinnerValues = objects;
	}

	@Override
	public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
		return getCustomView(position, cnvtView, prnt);
	}

	@Override
	public View getView(int pos, View cnvtView, ViewGroup prnt) {
		return getCustomView(pos, cnvtView, prnt);
	}

	public View getCustomView(int position, View convertView,
			ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View mySpinner = inflater.inflate(
				R.layout.request_sample_color_dropdown_inflator, parent,
				false);
		TextView main_text = (TextView) mySpinner
				.findViewById(R.id.textone);
		main_text.setText(spinnerValues[position]);
		return mySpinner;
	}
}
