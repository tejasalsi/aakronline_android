package com.dataholder;

import android.view.View;

public class DataHolderClass {

	private static DataHolderClass dataObject = null;
	private String _catagoryName;
	private String _titleCatagory;
	private String _videoId;
	private String _filterBy;
	View view;
	private String _pId;
	private String _productImageUrl;

	public static DataHolderClass getInstance() {
		if (dataObject == null)
			dataObject = new DataHolderClass();
		return dataObject;
	}

	public String get_filterBy() {
		return _filterBy;
	}

	public void set_filterBy(String _filterBy) {
		this._filterBy = _filterBy;
	}

	public String get_videoId() {
		return _videoId;
	}

	public void set_videoId(String _videoId) {
		this._videoId = _videoId;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public String get_titleCatagory() {
		return _titleCatagory;
	}

	public void set_titleCatagory(String _titleCatagory) {
		this._titleCatagory = _titleCatagory;
	}

	public String get_productImageUrl() {
		return _productImageUrl;
	}

	public void set_productImageUrl(String _productImageUrl) {
		this._productImageUrl = _productImageUrl;
	}

	public String get_pId() {
		return _pId;
	}

	public void set_pId(String _pId) {
		this._pId = _pId;
	}

	public String get_catagoryName() {
		return _catagoryName;
	}

	public void set_catagoryName(String _catagoryName) {
		this._catagoryName = _catagoryName;
	}

}
