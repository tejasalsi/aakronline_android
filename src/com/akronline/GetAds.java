package com.akronline;

import static com.commondata.CommonData.adsList;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.AsyncTask;
import android.util.Log;

import com.akronline.interfaces.AdsCallback;

public class GetAds extends AsyncTask<Void, Void, String> {

	private static final String SOAP_ACTION = "http://tempuri.org/GetAds";
	private static final String NAMESPACE = "http://tempuri.org/";
	private static final String METHOD_NAME = "GetAds";
	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	AdsCallback callback;
	String userID;

	public GetAds(AdsCallback callback, String userID) {
		this.callback = callback;
		this.userID = userID;
		Log.v("ads", "getting ads");
	}

	@Override
	protected String doInBackground(Void... params) {
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
		request.addProperty("UserId", userID);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;
		try {
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapObject result = (SoapObject) envelope.bodyIn;
			if (result != null) {
				String data = result.getProperty(0).toString();
				Log.v("data", data);
				JSONArray arr = new JSONObject(data).getJSONArray("Data");
				adsList.clear();
				for (int i = 0; i < arr.length(); i++) {
					adsList.add(arr.getJSONObject(i).getString("Ads"));
				}
				Log.v("AdNum", "Total imgs: " + adsList.size());
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return "No Internet";
		} catch (SocketTimeoutException e) {
			e.printStackTrace();
			return "Unable to reach server";
		} catch (Exception e) {
			e.printStackTrace();
			return "Unexpected Error Occured";
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (result == null)
			callback.onAdsLoaded();
		else
			Log.e("err", result);
	}

}
