package com.akronline.interfaces;

public interface DialogCallback {
	public void onDialogSubmit(int requestCode, String result);
}
