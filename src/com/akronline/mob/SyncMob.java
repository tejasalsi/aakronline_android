package com.akronline.mob;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.GetAds;
import com.akronline.R;
import com.akronline.interfaces.AdsCallback;
import com.commondata.CommonData;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.moodstocks.android.MoodstocksError;
import com.moodstocks.android.Scanner;

public class SyncMob extends Activity implements Scanner.SyncListener,
		AdsCallback {

	private static final String API_KEY = "cv71d5w9bvkadz7nnqp9";
	private static final String API_SECRET = "QzAPOYMWzyQ6z2al";
	private Scanner scanner;
	TextView txtProgress;
	private boolean compatible = false;
	ImageView btnScan;
	SliderLayout slider;
	public static Activity syncMob;
	EasyTracker easyTracker;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("tag", "SyncMob");
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		syncMob = this;
		
		HomeMob.search.setVisibility(View.GONE);
		HomeMob.searchField.setVisibility(View.GONE);
		
		setContentView(R.layout.sync_mob);
		txtProgress = (TextView) findViewById(R.id.txtProgressMob);
		btnScan = (ImageView) findViewById(R.id.btnScanMob);
		slider = (SliderLayout) findViewById(R.id.sliderMob);
		if (CommonData.adsList.size() == 0) {
			new GetAds(this, "0").execute();
		} else {
			onAdsLoaded();
		}
		compatible = Scanner.isCompatible();
		if (compatible) {
			try {
				scanner = Scanner.get();
				scanner.isSyncing();
				String path = Scanner.pathFromFilesDir(this, "scanner.db");
				scanner.open(path, API_KEY, API_SECRET);
				scanner.setSyncListener(this);
				scanner.sync();
			} catch (MoodstocksError e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (compatible) {
			try {
				scanner.close();
				scanner.destroy();
				scanner = null;
			} catch (MoodstocksError e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onSyncStart() {
		txtProgress.setText("Starting SYNC...");
	}

	@Override
	public void onSyncComplete() {
		int count = 0;
		try {
			count = scanner.count();

		} catch (MoodstocksError e) {
			count = 0;
		}
		Log.e("MOOD", "sync completed " + count + " images");
		Toast.makeText(getApplicationContext(), count + " images synced",
				Toast.LENGTH_SHORT).show();
		txtProgress.setVisibility(View.INVISIBLE);
		btnScan.setVisibility(View.VISIBLE);
	}

	@Override
	public void onSyncFailed(MoodstocksError e) {
		Log.e("Moodstocks SDK",
				"Sync error #" + e.getErrorCode() + ": " + e.getMessage());
		txtProgress.setText("Sync Failed!");
	}

	@Override
	public void onSyncProgress(int total, int current) {
		txtProgress.setText("Sync " + current + " / " + total);
		Log.e("SYNC", "Sync " + current + " / " + total);
	}

	public void onScanButtonClicked(View view) {
		easyTracker = EasyTracker.getInstance(SyncMob.this);
		easyTracker.send(MapBuilder.createEvent("Scan",
								"Point and Shoot", "shoot btn", null).build());
		
		if (compatible) {
			startActivity(new Intent(this, ScanMob.class));
		}
	}

	@Override
	public void onAdsLoaded() {
		for (String ad : CommonData.adsList) {
			DefaultSliderView sv = new DefaultSliderView(this);
			sv.image(ad);
			slider.addSlider(sv);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if(HomeMob.searchField.equals(""))
			HomeMob.searchField.setVisibility(View.GONE);
		else {
			HomeMob.searchField.setVisibility(View.VISIBLE);
			HomeMob.title.setVisibility(View.GONE);
		}
		HomeMob.search.setVisibility(View.VISIBLE);
	}
	
	
}