package com.akronline.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

import com.akronline.R;

public class RoundCornerSpinner extends Spinner{

	public RoundCornerSpinner(Context context) {
		super(context);
		init();
	}
	
	public RoundCornerSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
 
    public RoundCornerSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
     
	
	void init(){
		 setBackgroundResource(R.drawable.roundcornertextbox);
	       setPadding(20, 0, 0, 0);
	}

}
