package com.akronline.dialogs;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.Calendar;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.R;
import com.akronline.interfaces.DialogCallback;
import com.progressbar.GoogleProgressDialog;

public class HelpUs extends Dialog implements
		android.view.View.OnClickListener, DialogCallback {

	private static final String NAMESPACE = "http://tempuri.org/";
	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	Context ctx;
	String userID;
	TextView abtMe, howIsell, myCust, whrIwrk;
	Button btnCancel, btnSubmit;
	View viewAboutMe, viewWhereIWork, viewHowISell, viewMyCust;
	private static int tabPos = 0;
	// About Me
	EditText etPhyAddr, etState, etZip, etOfcPhn, etCell;
	TextView txtBday, txtSocial;
	Spinner spinPhone, spinEmail, spinText;
	Spinner spinAnnSalesPers, spinPP, spinCS, spinFS, spinDI, spinSNC, spinDSR;
	// How I sell
	private static final String[] sellingTools = { "Choose One", "Catalog",
			"Supplier Website", "App" };
	private static final String[] often = { "Select", "Daily",
			"1-3 times/ week", "1-3 times/ month", "1-3 times/ year", "Never" };
	private static final String[] PSAs = { "Select", "Monthly", "Quarterly",
			"Semi Annually", "Annually", "Never" };
	private static final String[] directMail = { "Select", "Always",
			"Sometimes", "Never" };
	Spinner spinInPerson, spinEmail2, spinDirect, spinPhone2;
	Spinner spinTools, spinSampAdv, spinPSAs, spinDirectMail;
	RadioButton rbKitsY, rbKitsN, rbWearY, rbWearN;
	TextView txtSocailBusiness;
	// My customers
	RadioButton rbCustArtY, rbCustArtN;
	EditText etMarketNotSelling;
	TextView txtMarketSelling;
	// WhereIWork
	Spinner spinAnnSales, spinBuyDeci;
	EditText etOfcLoc, etNumReps, etVRM, etEmrCon;
	private static final String[] ann_sales = { "Select", "Less than $500,000",
			"$500,001 - $1,000,000", "$1,000,001 - $5,000,000",
			"$5,000,001 - $10,000,000", "Over $10,000,000" };
	private static final String[] buying_deci = { "Select", "Project",
			"Product", "Budget" };

	public HelpUs(Context context, String userID) {
		
		super(context);
		this.ctx = context;
		this.userID = userID;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.help_us);
		getWindow().getDecorView().setBackgroundResource(R.drawable.white_bg);

		abtMe = (TextView) findViewById(R.id.txtAbtMe);
		howIsell = (TextView) findViewById(R.id.txtHowIsell);
		myCust = (TextView) findViewById(R.id.txtMyCust);
		whrIwrk = (TextView) findViewById(R.id.txtWhrIwrk);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		viewAboutMe = findViewById(R.id.viewAboutMe);
		viewWhereIWork = findViewById(R.id.viewWhereIWork);
		viewHowISell = findViewById(R.id.viewHowISell);
		viewMyCust = findViewById(R.id.viewMyCustomers);
		// About Me
		etPhyAddr = (EditText) findViewById(R.id.etPhyAdd);
		etState = (EditText) findViewById(R.id.etState);
		etZip = (EditText) findViewById(R.id.etZip);
		etOfcPhn = (EditText) findViewById(R.id.etPhnOfc);
		etCell = (EditText) findViewById(R.id.etCell);
		txtBday = (TextView) findViewById(R.id.txtBday);
		spinPhone = (Spinner) findViewById(R.id.spinPhone);
		spinEmail = (Spinner) findViewById(R.id.spinEmail);
		spinText = (Spinner) findViewById(R.id.spinText);
		txtSocial = (TextView) findViewById(R.id.txtSocialPers);
		spinAnnSalesPers = (Spinner) findViewById(R.id.spinAnnSalesPers);
		spinPP = (Spinner) findViewById(R.id.spinPP);
		spinCS = (Spinner) findViewById(R.id.spinCS);
		spinFS = (Spinner) findViewById(R.id.spinFS);
		spinDI = (Spinner) findViewById(R.id.spinDI);
		spinSNC = (Spinner) findViewById(R.id.spinSNC);
		spinDSR = (Spinner) findViewById(R.id.spinDSR);

		spinAnnSalesPers.setAdapter(new ArrayAdapter<String>(ctx,
				android.R.layout.simple_list_item_1,
				android.R.id.text1, ann_sales));
		// How I sell
		spinInPerson = (Spinner) findViewById(R.id.spinInPerson);
		spinEmail2 = (Spinner) findViewById(R.id.spinEmail2);
		spinDirect = (Spinner) findViewById(R.id.spinDirect);
		spinPhone2 = (Spinner) findViewById(R.id.spinPhone2);
		spinTools = (Spinner) findViewById(R.id.spinSellTools);
		spinSampAdv = (Spinner) findViewById(R.id.spinOften);
		spinPSAs = (Spinner) findViewById(R.id.spinPSAs);
		spinDirectMail = (Spinner) findViewById(R.id.spinDirectMail);
		rbKitsY = (RadioButton) findViewById(R.id.rbKitsY);
		rbKitsN = (RadioButton) findViewById(R.id.rbKitsN);
		rbWearY = (RadioButton) findViewById(R.id.rbWearY);
		rbWearN = (RadioButton) findViewById(R.id.rbWearN);
		txtSocailBusiness = (TextView) findViewById(R.id.txtSocialBusiness);

		spinTools.setAdapter(new ArrayAdapter<String>(ctx,
				android.R.layout.simple_list_item_1,
				android.R.id.text1, sellingTools));
		spinSampAdv.setAdapter(new ArrayAdapter<String>(ctx,
				android.R.layout.simple_list_item_1,
				android.R.id.text1, often));
		spinPSAs.setAdapter(new ArrayAdapter<String>(ctx,
				android.R.layout.simple_list_item_1,
				android.R.id.text1, PSAs));
		spinDirectMail.setAdapter(new ArrayAdapter<String>(ctx,
				android.R.layout.simple_list_item_1,
				android.R.id.text1, directMail));

		// My customers
		txtMarketSelling = (TextView) findViewById(R.id.txtMarkets);
		etMarketNotSelling = (EditText) findViewById(R.id.etWhatMarkets);
		rbCustArtY = (RadioButton) findViewById(R.id.rbCustArtY);
		rbCustArtN = (RadioButton) findViewById(R.id.rbCustArtN);
		// where I work
		spinAnnSales = (Spinner) findViewById(R.id.spinAnnSales);
		spinBuyDeci = (Spinner) findViewById(R.id.spinBuyDeci);
		etOfcLoc = (EditText) findViewById(R.id.etOfcLoc);
		etNumReps = (EditText) findViewById(R.id.etNumReps);
		etVRM = (EditText) findViewById(R.id.etVRM);
		etEmrCon = (EditText) findViewById(R.id.etEmerCon);

		spinAnnSales.setAdapter(new ArrayAdapter<String>(ctx,
				android.R.layout.simple_list_item_1,
				android.R.id.text1, ann_sales));
		spinBuyDeci.setAdapter(new ArrayAdapter<String>(ctx,
				android.R.layout.simple_list_item_1,
				android.R.id.text1, buying_deci));

		btnCancel.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
		abtMe.setOnClickListener(this);
		howIsell.setOnClickListener(this);
		myCust.setOnClickListener(this);
		whrIwrk.setOnClickListener(this);
		txtSocial.setOnClickListener(this);
		txtSocailBusiness.setOnClickListener(this);
		txtMarketSelling.setOnClickListener(this);
		txtBday.setOnClickListener(this);

		switchTab(0);
	}

	private void showToast(String msg) {
		Toast.makeText(ctx.getApplicationContext(), msg, Toast.LENGTH_LONG)
				.show();
	}

	private void switchTab(int position) {
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		tabPos = position;
		switch (position) {
		case 0: // About me
			abtMe.setBackgroundResource(R.drawable.buttonhover);
			howIsell.setBackgroundResource(R.drawable.black);
			myCust.setBackgroundResource(R.drawable.black);
			whrIwrk.setBackgroundResource(R.drawable.black);
			viewAboutMe.setVisibility(VISIBLE);
			viewHowISell.setVisibility(GONE);
			viewMyCust.setVisibility(GONE);
			viewWhereIWork.setVisibility(GONE);
			break;
		case 1: // How i sell
			abtMe.setBackgroundResource(R.drawable.black);
			howIsell.setBackgroundResource(R.drawable.buttonhover);
			myCust.setBackgroundResource(R.drawable.black);
			whrIwrk.setBackgroundResource(R.drawable.black);
			viewAboutMe.setVisibility(GONE);
			viewHowISell.setVisibility(VISIBLE);
			viewMyCust.setVisibility(GONE);
			viewWhereIWork.setVisibility(GONE);
			break;
		case 2: // My Customers
			abtMe.setBackgroundResource(R.drawable.black);
			howIsell.setBackgroundResource(R.drawable.black);
			myCust.setBackgroundResource(R.drawable.buttonhover);
			whrIwrk.setBackgroundResource(R.drawable.black);
			viewAboutMe.setVisibility(GONE);
			viewHowISell.setVisibility(GONE);
			viewMyCust.setVisibility(VISIBLE);
			viewWhereIWork.setVisibility(GONE);
			break;
		case 3: // Where I work
			abtMe.setBackgroundResource(R.drawable.black);
			howIsell.setBackgroundResource(R.drawable.black);
			myCust.setBackgroundResource(R.drawable.black);
			whrIwrk.setBackgroundResource(R.drawable.buttonhover);
			viewAboutMe.setVisibility(GONE);
			viewHowISell.setVisibility(GONE);
			viewMyCust.setVisibility(GONE);
			viewWhereIWork.setVisibility(VISIBLE);
			break;
		}
	}

	private static final DecimalFormat df = new DecimalFormat("00");
	private OnDateSetListener dateSet = new OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			txtBday.setText((df.format(++monthOfYear)) + "/"
					+ (df.format(dayOfMonth)) + "/" + year);
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txtAbtMe:
			switchTab(0);
			break;
		case R.id.txtHowIsell:
			switchTab(1);
			break;
		case R.id.txtMyCust:
			switchTab(2);
			break;
		case R.id.txtWhrIwrk:
			switchTab(3);
			break;
		case R.id.btnCancel:
			dismiss();
			break;
		case R.id.btnSubmit:
			submitForm();
			break;
		case R.id.txtSocialPers:
			new SocialChooser(ctx, this, REQ_SOCIAL).show();
			break;
		case R.id.txtSocialBusiness:
			new SocialChooser(ctx, this, REQ_SOCIAL_BUSINESS).show();
			break;
		case R.id.txtMarkets:
			new MarketChooser(ctx, this, REQ_MARKET).show();
			break;
		case R.id.txtBday:
//			Calendar cal = Calendar.getInstance();
//			cal.setTimeInMillis(System.currentTimeMillis());
//			DatePickerDialog dpd = new DatePickerDialog(ctx, dateSet,
//					cal.get(Calendar.DAY_OF_WEEK), cal.get(Calendar.MONTH),
//					cal.get(Calendar.YEAR));
//			dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
//			dpd.show();
			
			Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			Log.v("year", "year " + year);
			if (year == 2014) {
				day = 1;
				month = 0;
				year = 2015;
			}
			DatePickerDialog dialog = new DatePickerDialog(v.getContext(),
					dateSet, year, month, day);

			dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
			dialog.updateDate(year, month, day);
			dialog.getDatePicker().setCalendarViewShown(false);
			dialog.show();
			
			break;
		}
	}

	private void submitForm() {
		switch (tabPos) {
		case 0:
			String PhysicalAddress = etPhyAddr.getText().toString(),
			State = etState.getText().toString(),
			ZipCode = etZip.getText().toString(),
			OfficePhone = etOfcPhn.getText().toString(),
			CellPhone = etCell.getText().toString(),
			Birthday = txtBday.getText().toString(),
			BestWayContactMe = "",
			SocialNetworkingg = txtSocial.getText().toString(),
			AnnualSale = "",
			SupplierAttributes = "";
			if (spinPhone.getSelectedItemPosition() > 0)
				BestWayContactMe = "Phone;"
						+ spinPhone.getSelectedItem().toString();
			if (spinEmail.getSelectedItemPosition() > 0)
				BestWayContactMe = BestWayContactMe + "|Email;"
						+ spinEmail.getSelectedItem().toString();
			if (spinText.getSelectedItemPosition() > 0)
				BestWayContactMe = BestWayContactMe + "|Text;"
						+ spinText.getSelectedItem().toString();
			if (BestWayContactMe.startsWith("|")) {
				BestWayContactMe = BestWayContactMe.substring(1);
			}

			if (spinAnnSalesPers.getSelectedItemPosition() > 0)
				AnnualSale = spinAnnSalesPers.getSelectedItem().toString();
			if (spinPP.getSelectedItemPosition() > 0)
				SupplierAttributes = "Product prices;"
						+ spinPP.getSelectedItem().toString();
			if (spinCS.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes + "|Customer service;"
						+ spinCS.getSelectedItem().toString();
			if (spinFS.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes + "|Fast shipping;"
						+ spinFS.getSelectedItem().toString();
			if (spinDI.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes + "|Deep inventory;"
						+ spinDI.getSelectedItem().toString();
			if (spinSNC.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes
						+ "|Safety & compliance;"
						+ spinSNC.getSelectedItem().toString();
			if (spinDSR.getSelectedItemPosition() > 0)
				SupplierAttributes = SupplierAttributes
						+ "|Dedicated sales representatives;"
						+ spinDSR.getSelectedItem().toString();

			if (SupplierAttributes.startsWith("|")) {
				SupplierAttributes = SupplierAttributes.substring(1);
			}
			final String[] aboutMeParams = { PhysicalAddress, State, ZipCode,
					OfficePhone, CellPhone, Birthday, BestWayContactMe,
					SocialNetworkingg, AnnualSale, SupplierAttributes };
			new AboutMe().execute(aboutMeParams);
			break;
		case 1:
			String sellingTool = "",
			freeVirtualSample = "",
			aakronSampleKit = "",
			sampleKit = "",
			PSA = "",
			SocialNetworking = "",
			CommunicationMethod = "",
			SendDirectMailClient = "";
			if (spinTools.getSelectedItemPosition() > 0) {
				sellingTool = spinTools.getSelectedItem().toString();
			}
			if (spinSampAdv.getSelectedItemPosition() > 0) {
				freeVirtualSample = spinSampAdv.getSelectedItem().toString();
			}
			if (rbKitsY.isChecked() || rbKitsN.isChecked()) {
				aakronSampleKit = rbKitsY.isChecked() ? "Yes" : "No";
			}
			if (rbWearY.isChecked() || rbWearN.isChecked()) {
				sampleKit = rbWearY.isChecked() ? "Yes" : "No";
			}
			if (spinPSAs.getSelectedItemPosition() > 0) {
				PSA = spinPSAs.getSelectedItem().toString();
			}
			SocialNetworking = txtSocailBusiness.getText().toString();
			// TODO pref comm fix CommunicationMethod
			if (spinInPerson.getSelectedItemPosition() > 0)
				CommunicationMethod = "In Person;"
						+ spinInPerson.getSelectedItem().toString();
			if (spinEmail2.getSelectedItemPosition() > 0)
				CommunicationMethod = CommunicationMethod + "|Email;"
						+ spinEmail2.getSelectedItem().toString();
			if (spinDirect.getSelectedItemPosition() > 0)
				CommunicationMethod = CommunicationMethod + "|Direct Mail;"
						+ spinDirect.getSelectedItem().toString();
			if (spinPhone2.getSelectedItemPosition() > 0)
				CommunicationMethod = CommunicationMethod + "|Phone;"
						+ spinPhone2.getSelectedItem().toString();

			if (CommunicationMethod.startsWith("|")) {
				CommunicationMethod = CommunicationMethod.substring(1);
			}

			if (spinDirectMail.getSelectedItemPosition() > 0) {
				SendDirectMailClient = spinDirectMail.getSelectedItem()
						.toString();
			}
			final String[] howISellParams = { sellingTool, freeVirtualSample,
					aakronSampleKit, sampleKit, PSA, SocialNetworking,
					CommunicationMethod, SendDirectMailClient };
			new HowISell().execute(howISellParams);
			break;
		case 2:
			String MarketSell = "",
			MarketNotSelling = "",
			FreeVirtualSample = "";
			MarketSell = txtMarketSelling.getText().toString();
			MarketNotSelling = etMarketNotSelling.getText().toString();
			if (rbCustArtY.isChecked() || rbCustArtN.isChecked()) {
				FreeVirtualSample = rbCustArtY.isChecked() ? "Yes" : "No";
			}
			final String[] myCustParams = { MarketSell, MarketNotSelling,
					FreeVirtualSample };
			new MyCustomers().execute(myCustParams);
			break;
		case 3:
			new WhereIWork().execute();
			break;
		}
	}

	private class AboutMe extends AsyncTask<String, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/HelpUsHelpYou_AboutMe";
		private static final String METHOD_NAME = "HelpUsHelpYou_AboutMe";

		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(ctx);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... p) {

			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("PhysicalAddress", p[0]);
			request.addProperty("State", p[1]);
			request.addProperty("ZipCode", p[2]);
			request.addProperty("OfficePhone", p[3]);
			request.addProperty("CellPhone", p[4]);
			request.addProperty("Birthday", p[5]);
			request.addProperty("BestWayContactMe", p[6]);
			request.addProperty("SocialNetworking", p[7]);
			request.addProperty("AnnualSale", p[8]);
			request.addProperty("SupplierAttributes", p[9]);
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				// HelpUs.this.dismiss();
				showToast("Feedback submitted successfully");
				return;
			} else
				showToast("Server error: Feedback not sent");
		}

	}

	private class HowISell extends AsyncTask<String, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/HelpUsHelpYou_HowISell";
		private static final String METHOD_NAME = "HelpUsHelpYou_HowISell";

		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(ctx);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... p) {

			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("SellingTool", p[0]);
			request.addProperty("FreeVirtualSample", p[1]);
			request.addProperty("AakronSampleKit", p[2]);
			request.addProperty("SampleKit", p[3]);
			request.addProperty("PSA", p[4]);
			request.addProperty("SocialNetworking", p[5]);
			request.addProperty("CommunicationMethod", p[6]);
			request.addProperty("SendDirectMailClient", p[7]);
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				// HelpUs.this.dismiss();
				showToast("Feedback submitted successfully");
				return;
			} else
				showToast("Server error: Feedback not sent");
		}

	}

	private class MyCustomers extends AsyncTask<String, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/HelpUsHelpYou_MyCustomers";
		private static final String METHOD_NAME = "HelpUsHelpYou_MyCustomers";

		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(ctx);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... p) {

			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("MarketSell", p[0]);
			request.addProperty("MarketNotSelling", p[1]);
			request.addProperty("FreeVirtualSample", p[2]);
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				// HelpUs.this.dismiss();
				showToast("Feedback submitted successfully");
				return;
			} else
				showToast("Server error: Feedback not sent");
		}

	}

	private class WhereIWork extends AsyncTask<Void, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/HelpUsHelpYou_WhereIWork";
		private static final String METHOD_NAME = "HelpUsHelpYou_WhereIWork";

		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(ctx);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {

			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("AnnualSale", spinAnnSales.getSelectedItem()
					.toString());
			request.addProperty("Decision", spinBuyDeci.getSelectedItem()
					.toString());
			request.addProperty("OfficeLocations", etOfcLoc.getText()
					.toString());
			request.addProperty("SalesRep", etNumReps.getText().toString());
			request.addProperty("VendorRelationManager", etVRM.getText()
					.toString());
			request.addProperty("Emergency", etEmrCon.getText().toString());
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				// HelpUs.this.dismiss();
				showToast("Feedback submitted successfully");
				return;
			} else
				showToast("Server error: Feedback not sent");
		}

	}

	private static final int REQ_SOCIAL = 100;
	private static final int REQ_SOCIAL_BUSINESS = 200;
	private static final int REQ_MARKET = 300;

	@Override
	public void onDialogSubmit(int requestCode, String result) {
		switch (requestCode) {
		case REQ_SOCIAL:
			txtSocial.setText(result);
			break;
		case REQ_MARKET:
			txtMarketSelling.setText(result);
			break;
		case REQ_SOCIAL_BUSINESS:
			txtSocailBusiness.setText(result);
			break;
		}

	}

}
