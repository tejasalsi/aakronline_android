package com.akronline.dialogs;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.R;
import com.progressbar.GoogleProgressDialog;

public class SendEmail extends Dialog implements
		android.view.View.OnClickListener {

	Context ctx;
	String userID;
	String prodCode;
	TextView name, email;
	EditText etEmail, etName, etNotes;
	Button btnSend, btnCancel;

	public SendEmail(Context ctx, String prodCode) {
		super(ctx);
		this.ctx = ctx;
		this.prodCode = prodCode;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.email_layout);
		getWindow().getDecorView().setBackgroundResource(R.drawable.white_bg);
		userID = ctx.getSharedPreferences("LoginPref", 0).getString("USER-ID",
				null);

		name = (TextView) findViewById(R.id.name);
		email = (TextView) findViewById(R.id.email);
		etName = (EditText) findViewById(R.id.etName);
		etEmail = (EditText) findViewById(R.id.etEmails);
		etNotes = (EditText) findViewById(R.id.etNotes);
		btnSend = (Button) findViewById(R.id.btnSend);
		btnCancel = (Button) findViewById(R.id.btnCancel);

		if(name!=null || email!=null){
		name.setText(Html.fromHtml("Recipient Name <font color=red>*</font>"));
		email.setText(Html.fromHtml("Recipient Email <font color=red>*</font>"));
		}
		btnSend.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnCancel:
				this.dismiss();
				break;
			case R.id.btnSend:
				if (etName.getText().toString().trim().isEmpty()) {
					etName.requestFocus();
					showToast("Name is required");
					return;
				}
				if (etEmail.getText().toString().trim().isEmpty()) {
					etEmail.requestFocus();
					showToast("Email is required");
					return;
				}
				if (!etEmail.getText().toString()
						.matches(Patterns.EMAIL_ADDRESS.pattern())) {
					etEmail.requestFocus();
					showToast("Email is invalid");
					return;
				}
				new SendMail().execute();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception :" + e.getMessage());
		}

	}

	private void showToast(String msg) {
		Toast.makeText(ctx.getApplicationContext(), msg, Toast.LENGTH_LONG)
				.show();
	}

	private static String SOAP_ACTION = "http://tempuri.org/EmailProductInfo";
	private static String NAMESPACE = "http://tempuri.org/";
	private static String METHOD_NAME = "EmailProductInfo";
	private static String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	private class SendMail extends AsyncTask<Void, Void, String> {

		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(ctx);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("ProductCode", prodCode);
			request.addProperty("RecepientEmail", etEmail.getText().toString()
					.trim());
			request.addProperty("RecepientName", etName.getText().toString()
					.trim());
			request.addProperty("Notes", etNotes.getText().toString());
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				SendEmail.this.dismiss();
				showToast("Email sent successfully");
				return;
			} else
				showToast("Server Error: email not sent");
		}
	}
}
