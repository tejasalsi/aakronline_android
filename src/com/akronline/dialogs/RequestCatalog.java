package com.akronline.dialogs;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.akronline.R;
import com.progressbar.GoogleProgressDialog;

public class RequestCatalog extends Dialog implements
		android.view.View.OnClickListener {

	Context ctx;
	String userID;
	Spinner spinCatalogs, spinFlyers;
	EditText notes;
	Button btnSubmit, btnCancel;
	private static final String[] items = {"0", "1", "2", "3" };

	public RequestCatalog(Context ctx) {
		super(ctx);
		this.ctx = ctx;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.request_catalog);
		getWindow().getDecorView().setBackgroundResource(R.drawable.white_bg);
		
		userID = ctx.getSharedPreferences("LoginPref", 0).getString("USER-ID",
				null);

		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		notes = (EditText) findViewById(R.id.etNotes);
		spinCatalogs = (Spinner) findViewById(R.id.spinCatalogs);
		spinFlyers = (Spinner) findViewById(R.id.spinFlyers);

		spinCatalogs
				.setAdapter(new ArrayAdapter<String>(ctx,
						android.R.layout.simple_list_item_1,
						android.R.id.text1, items));
		spinFlyers
				.setAdapter(new ArrayAdapter<String>(ctx,
						android.R.layout.simple_list_item_1,
						android.R.id.text1, items));

		spinCatalogs.setSelection(0);
		spinFlyers.setSelection(0);

		btnSubmit.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnCancel:
				this.dismiss();
				break;
			case R.id.btnSubmit:
//				if (spinCatalogs.getSelectedItemPosition() == 0
//						&& spinFlyers.getSelectedItemPosition() == 0) {
//					showToast("Please select at least one item");
//					return;
//				}
				new Submit().execute();
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception: " + e.getMessage());
		}

	}

	private void showToast(String msg) {
		Toast.makeText(ctx.getApplicationContext(), msg, Toast.LENGTH_LONG)
				.show();
	}

	private class Submit extends AsyncTask<Void, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/RequestACatalog";
		private static final String NAMESPACE = "http://tempuri.org/";
		private static final String METHOD_NAME = "RequestACatalog";
		private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
		GoogleProgressDialog dialog;
		String code = "1";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(ctx);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("NumberOfCatalogs", spinCatalogs
					.getSelectedItem().toString());
			request.addProperty("NumberOfFlyers", spinFlyers.getSelectedItem()
					.toString());
			request.addProperty("AdditionalInfo", notes.getText().toString()
					.trim());
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					JSONObject obj = new JSONObject(data);
					code = obj.getJSONArray("Data").getJSONObject(0)
							.getString("responseCode");
					Log.v("Code", "Code:" + code);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null)
				showToast(result);
			else if (code.equals("0")) {
				showToast("Request sent successfully");
				RequestCatalog.this.dismiss();
			} else
				showToast("Server Error: Request not sent");

		}
	}

}
