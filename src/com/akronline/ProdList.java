package com.akronline;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.adapters.ProductListingAdapter;
import com.akronline.mob.MobileProductListing;
import com.akronline.views.MyEditText;
import com.commondata.CommonData;
import com.dataholder.DataHolderClass;
import com.datamodel.Product;
import com.progressbar.GoogleProgressDialog;

public class ProdList extends Fragment implements OnItemClickListener
{

	private static final String NAMESPACE = "http://tempuri.org/";

	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	private static ListView lvProducts;

	public static ArrayList<Product> productList = new ArrayList<Product>();

	public static ArrayList<Product> tempList = new ArrayList<Product>();

	public static com.akronline.views.MyEditText search_keywords;

	static ProductListingAdapter adapter;

	Button up;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.e("tag", "ProdList");
		View v = inflater.inflate(R.layout.list_item_fragment, container, false);
		lvProducts = (ListView) v.findViewById(R.id.productList);
		up = (Button) v.findViewById(R.id.scrolldown);
		search_keywords = (MyEditText) v.findViewById(R.id.search_keywords);

		lvProducts.setOnItemClickListener(this);

		search_keywords.setOnEditorActionListener(new TextView.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if (actionId == EditorInfo.IME_ACTION_SEARCH)
				{
					if (search_keywords.getText().toString().isEmpty())
					{
						showToast("enter search keywords");
					}
					else
					{
						new Search().execute(search_keywords.getText().toString());
					}
					ProdList.this.getActivity().getWindow().setSoftInputMode(SOFT_INPUT_STATE_HIDDEN);
					return true;
				}
				return false;
			}
		});

		lvProducts.setOnScrollListener(new OnScrollListener()
		{
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState)
			{
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
			{
				if (firstVisibleItem > 4)
				{
					up.setVisibility(View.VISIBLE);
				}
				else
				{
					up.setVisibility(View.GONE);
				}
			}
		});

		up.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				lvProducts.setSelection(0);
				up.setVisibility(View.GONE);
			}
		});

		if (ProductScreen.fromYT)
		{
			new FetchSubProducts().execute();
			ProductScreen.fromYT = false;
		}
		else
		{
			new FetchProducts().execute();
		}
		return v;
	}

	private void showToast(String msg)
	{
		Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}

	public static float[] getMinMax()
	{
		float[] values = { 0f, 0f };
		ArrayList<Product> temp = productList;
		Collections.sort(temp, new Ascending());
		try
		{
			values[0] = Float.valueOf(temp.get(0).getPrice().substring(1));
		}
		catch (Exception e)
		{
			values[0] = 0f;
		}
		try
		{
			values[1] = Float.valueOf(temp.get(temp.size() - 1).getPrice().substring(1));
		}
		catch (Exception e)
		{
			values[1] = 0f;
		}
		return values;
	}

	public static void sortAsc(Context ctx)
	{
//		tempList = productList;
		Collections.sort(tempList, new Ascending());
		filterData(ctx, tempList);
	}

	public static void sortDesc(Context ctx)
	{
//		tempList = productList;
		Collections.sort(tempList, new Ascending());
		Collections.reverse(tempList);
		filterData(ctx, tempList);
	}

	public static void setRange(final Context ctx, float min, float max)
	{
		tempList = new ArrayList<Product>();
		for (Product p : productList)
		{
			float price = 0f;
			try
			{
				price = Float.valueOf(p.getPrice().substring(1));
			}
			catch (Exception e)
			{
				price = 0f;
			}
			if (price >= min && price < max)
			{
				tempList.add(p);
				Log.d("price", min + " >= " + price + " < " + max);
			}

		}
		filterData(ctx, tempList);
	}

	private static class Ascending implements Comparator<Product>
	{
		@Override
		public int compare(Product lhs, Product rhs)
		{
			float first = 0f;
			try
			{
				first = Float.valueOf(lhs.getPrice().substring(1));
			}
			catch (Exception e)
			{
				first = 0f;
			}
			float second = 0f;
			try
			{
				second = Float.valueOf(rhs.getPrice().substring(1));
			}
			catch (Exception e)
			{
				second = 0f;
			}
			if (first == second)
				return 0;
			else if (first < second)
				return -1;
			else
				return 1;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		try
		{
			String _id = tempList.get(position).getProductCode();
			DataHolderClass.getInstance().set_pId(_id);
			Fragment detailFrag = new ProdDetail();
			((ProductScreen) getActivity()).doTransaction("details", detailFrag);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void _checkView()
	{
		// TODO fix keyboard
		if (search_keywords.getVisibility() == View.VISIBLE)
		{
			search_keywords.setVisibility(View.GONE);
		}
		else
		{
			search_keywords.setVisibility(View.VISIBLE);
		}
	}

	public static void filterData(Context ctx, ArrayList<Product> list)
	{
		try
		{
			adapter = new ProductListingAdapter(ctx, list);
			MobileProductListing._adapter = new ProductListingAdapter(ctx, list);
			MobileProductListing._adapter.notifyDataSetChanged();
			adapter.notifyDataSetChanged();
			if (lvProducts != null)
				lvProducts.setAdapter(adapter);
			MobileProductListing.productList.setAdapter(MobileProductListing._adapter);
			String id = list.get(0).getProductCode();
			DataHolderClass.getInstance().set_pId(id);
			Fragment detailFrag = new ProdDetail();
			((ProductScreen) ctx).doTransaction("details", detailFrag);
			((ProductScreen) ctx).setProductCount();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private class FetchProducts extends AsyncTask<String, String, String>
	{

		private static final String SOAP_ACTION = "http://tempuri.org/GetProductListing";

		private static final String METHOD_NAME = "GetProductListing";

		GoogleProgressDialog dialog;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params)
		{
			try
			{
				productList.clear();
				tempList.clear();
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				request.addProperty("Type", DataHolderClass.getInstance().get_catagoryName());
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				try
				{
					HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
					androidHttpTransport.call(SOAP_ACTION, envelope);
					if (envelope.bodyIn instanceof SoapFault)
					{
						String str = ((SoapFault) envelope.bodyIn).faultstring;
						Log.i("adddddddd", str);

					}
					else
					{
						SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
						String _result = resultsRequestSOAP.getProperty(0).toString();
						System.out.println("data" + _result);
						JSONObject ob = new JSONObject(_result);
						if (ob != null)
						{
							JSONArray _jarray = ob.optJSONArray("Products");
							for (int i = 0; i < _jarray.length(); i++)
							{
								JSONObject p = _jarray.optJSONObject(i);
								Product prod = new Product();

								prod.setName(p.optString("productname"));
								prod.setID(p.optString("productCode"));
								prod.setImageURL(p.optString("ImageUrl"));
								prod.setPrice(p.optString("price"));
								prod.setCatagory(p.getString("Category"));
								prod.setSubCatagory(p.getString("Subcategory"));

								productList.add(prod);
							}
						}
						Collections.sort(productList, new Ascending());
						tempList = productList;
					}
				}
				catch (NullPointerException e)
				{
					e.printStackTrace();
				}
				catch (ClassCastException cc)
				{
					cc.printStackTrace();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			try
			{
				if (tempList.size() > 0)
				{
					adapter = new ProductListingAdapter(getActivity(), tempList);
					lvProducts.setAdapter(adapter);
					adapter.notifyDataSetChanged();
					DataHolderClass.getInstance().set_pId(tempList.get(0).getProductCode());
					ProdDetail detailFrag = new ProdDetail();
					((ProductScreen) getActivity()).doTransaction("details", detailFrag);
					Iterator<Product> productIterator = tempList.iterator();
					int i = 0;
					while (productIterator.hasNext())
					{

						Product product = productIterator.next();

						if (product.getProductCode().equals(DataHolderClass.getInstance().get_pId()))
						{
							break;
						}
						i++;

					}

					lvProducts.setSelection(i);

				}
				else
				{
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				dialog.dismiss();
				((ProductScreen) getActivity()).setProductCount();
				((ProductScreen) getActivity()).setFilterData();
			}
		}
	}

	private class Search extends AsyncTask<String, Void, String>
	{

		private static final String SOAP_ACTION = "http://tempuri.org/GetProductSearch";

		private static final String METHOD_NAME = "GetProductSearch";

		GoogleProgressDialog dialog;

		JSONObject obj;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params)
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("keyword", params[0]);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try
			{
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null)
				{
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			}
			catch (UnknownHostException e)
			{
				e.printStackTrace();
				return "No Internet Connection";
			}
			catch (SocketTimeoutException e)
			{
				e.printStackTrace();
				return "Unable to reach server";
			}
			catch (JSONException e)
			{
				e.printStackTrace();
				return "Invalid response from server";
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			try
			{
				if (result != null)
					throw new IOException();
				JSONArray arr = obj.getJSONArray("Products");
				if (arr.length() == 1 && arr.getJSONObject(0).getString("productCode").equals(""))
				{
					Toast.makeText(getActivity().getApplicationContext(), "No results found", Toast.LENGTH_LONG).show();
				}
				else
				{
					productList.clear();
					for (int i = 0; i < arr.length(); i++)
					{
						JSONObject prod = arr.getJSONObject(i);
						Product p = new Product();
						p.setID(prod.getString("productCode"));
						p.setName(prod.getString("productname"));
						p.setCatagory(prod.getString("Category"));
						p.setSubCatagory(prod.getString("Subcategory"));
						p.setImageURL(prod.getString("ImageUrl"));
						p.setPrice(prod.getString("price"));
						productList.add(p);
					}
					filterData(getActivity(), productList);
					((ProductScreen) getActivity()).setFilterData();
				}

			}
			catch (IOException e)
			{
				showToast(result);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				showToast("Invalid response from server");
			}
			finally
			{
				dialog.dismiss();
				((ProductScreen) getActivity()).setProductCount();
				((ProductScreen) getActivity()).setFilterData();
			}

		}
	}

	private class FetchSubProducts extends AsyncTask<Void, Void, String>
	{

		private static final String SOAP_ACTION = "http://tempuri.org/GetProductListing";

		private static final String METHOD_NAME = "GetProductListing";

		GoogleProgressDialog dialog;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params)
		{
			try
			{
				productList.clear();
				tempList.clear();
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				// TODO filter category
				String category = "";
				if (PlayVideoScreen.scanProd.getCatagory().equals("Mood Products"))
				{
					category = "moodproducts";
				}
				else if (PlayVideoScreen.scanProd.getCatagory().equals("New Products"))
				{
					category = "newproducts";
				}
				else if (PlayVideoScreen.scanProd.getCatagory().equals("Snacks"))
				{
					category = "snacks";
				}
				else if (PlayVideoScreen.scanProd.getCatagory().equals("Best Sellers"))
				{
					category = "bestsellers";
				}
				DataHolderClass.getInstance().set_titleCatagory("");
				request.addProperty("Type", category);
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				try
				{
					HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
					androidHttpTransport.call(SOAP_ACTION, envelope);
					if (envelope.bodyIn instanceof SoapFault)
					{
						String str = ((SoapFault) envelope.bodyIn).faultstring;
						Log.i("adddddddd", str);

					}
					else
					{
						SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
						String _result = resultsRequestSOAP.getProperty(0).toString();
						System.out.println("data" + _result);
						JSONObject _obj = new JSONObject(_result);
						if (_obj != null)
						{
							JSONArray arr = _obj.optJSONArray("Products");
							for (int i = 0; i < arr.length(); i++)
							{
								JSONObject obj = arr.optJSONObject(i);
								if (obj.getString("Subcategory").equals(PlayVideoScreen.scanProd.getSubCatagory()))
								{
									Product p = new Product();
									p.setName(obj.optString("productname"));
									p.setID(obj.optString("productCode"));
									p.setImageURL(obj.optString("ImageUrl"));
									p.setPrice(obj.optString("price"));
									p.setCatagory(obj.getString("Category"));
									p.setSubCatagory(obj.getString("Subcategory"));
									productList.add(p);
								}
							}
						}
						tempList = productList;
					}
				}
				catch (NullPointerException e)
				{
					e.printStackTrace();
				}
				catch (ClassCastException cc)
				{
					cc.printStackTrace();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			try
			{
				if (tempList.size() > 0)
				{
					adapter = new ProductListingAdapter(getActivity(), tempList);
					adapter.notifyDataSetChanged();
					lvProducts.setAdapter(adapter);
					DataHolderClass.getInstance().set_pId(PlayVideoScreen.scanProd.getProductCode());
					ProdDetail detailFrag = new ProdDetail();
					((ProductScreen) getActivity()).doTransaction("details", detailFrag);
					Iterator<Product> productIterator = tempList.iterator();
					int i = 0;
					while (productIterator.hasNext())
					{

						Product product = productIterator.next();

						if (product.getProductCode().equals(DataHolderClass.getInstance().get_pId()))
						{
							break;
						}
						i++;

					}

					lvProducts.setSelection(i);
				}
				else
				{
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				dialog.dismiss();
				((ProductScreen) getActivity()).setProductCount();
				((ProductScreen) getActivity()).setFilterData();
			}
		}
	}
}