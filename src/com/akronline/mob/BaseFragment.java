package com.akronline.mob;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.akronline.R;

public class BaseFragment extends Fragment {

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			ImageView logo = (ImageView) getActivity().getActionBar()
					.getCustomView().findViewById(R.id.app_icon);
			TextView txtTitle = (TextView) getActivity().getActionBar()
					.getCustomView().findViewById(R.id.title);
			Button btnBack = (Button) getActivity().getActionBar()
					.getCustomView().findViewById(R.id.back_mobile);
			txtTitle.setVisibility(View.GONE);
			btnBack.setVisibility(View.GONE);
			logo.setVisibility(View.VISIBLE);
			
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
