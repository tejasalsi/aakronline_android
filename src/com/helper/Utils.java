package com.helper;

import android.annotation.SuppressLint;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

public class Utils {

	@SuppressLint("NewApi")
	public static int getDeviceWidth(WindowManager windowManager) {
		int deviceWidth = 0;

		Point size = new Point();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			windowManager.getDefaultDisplay().getSize(size);
			deviceWidth = size.x;
		} else {
			Display display = windowManager.getDefaultDisplay();
			deviceWidth = display.getWidth();
		}
		return deviceWidth;
	}

}
