package com.akronline.mob;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.Window;

import com.akronline.PlayVideoScreen;
import com.akronline.ProductScreen;
import com.akronline.R;
import com.commondata.CommonData;
import com.dataholder.DataHolderClass;
import com.datamodel.Product;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.moodstocks.android.AutoScannerSession;
import com.moodstocks.android.MoodstocksError;
import com.moodstocks.android.Result;
import com.moodstocks.android.Scanner;
import com.progressbar.GoogleProgressDialog;

public class ScanMob extends Activity implements AutoScannerSession.Listener {
	private AutoScannerSession session = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("tag", "ScanMob");
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_scanner);
		SurfaceView preview = (SurfaceView) findViewById(R.id.preview);
		try {
			session = new AutoScannerSession(this, Scanner.get(), this, preview);
		} catch (MoodstocksError e) {
			e.printStackTrace();
			showDialog(e.getMessage());
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (session != null)
			session.start();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (session != null)
			session.stop();
	}

	private void showDialog(String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false);
		builder.setNeutralButton("OK", null);
		builder.setTitle("AakronLine");
		builder.setMessage(msg);
		builder.show();
	}

	@Override
	public void onCameraOpenFailed(Exception arg0) {
		System.out.println("failed to open camera");
		showDialog("failed to open camera");
	}

	@Override
	public void onResult(Result result) {
		Log.v("ID", result.getValue());
		new GetDetails().execute(new String[] { result.getValue() });
	}

	@Override
	public void onWarning(String message) {
		System.out.println(message);
		showDialog(message);
	}

	private class GetDetails extends AsyncTask<String, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/GetProductData";
		private static final String NAMESPACE = "http://tempuri.org/";
		private static final String METHOD_NAME = "GetProductData";
		private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
		GoogleProgressDialog dialog;
		JSONObject obj;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(ScanMob.this);
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				request.addProperty("ProductCode", params[0]);
				// request.addProperty("userid", "0");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null && result.getPropertyCount() > 0) {
					String response = result.getProperty(0).toString();
					obj = new JSONObject(response);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Exception: " + e.getMessage();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null) {
				showDialog(result);
			}
			
			try {
				JSONObject data = obj.getJSONArray("ProductData")
						.getJSONObject(0);
				String vdoID = data.getString("MultimediaContent");
				Product scanProduct = new Product();
				scanProduct.setID(data.getString("productCode"));
				scanProduct.setCatagory(data.getString("Category"));
				scanProduct.setSubCatagory(data.getString("Subcategory"));
				scanProduct.setName(data.getString("ProductName"));
				CommonData.scannedProduct = scanProduct;
				ProductScreen.fromScan = true;
				if (scanProduct.getProductCode().equals("")) {
					showDialog("The scanned product is Invalid.\nPlease press back and scan again");
					return;
				} else if (vdoID.equals("")) {
					PlayVideoScreen.scanProd = scanProduct;
					ProductScreen.fromYT = true;
					//startActivity(new Intent(ScanMob.this, ProductScreen.class));
					SharedPreferences preferences = getSharedPreferences(
							"app", android.content.Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = preferences.edit();
					editor.putBoolean("isFromMobileProductList", true);
					editor.commit();
					MobileProductListing _listing = new MobileProductListing(true);
					Intent intent = new Intent(ScanMob.this, HomeMob.class);
					startActivity(intent);
					finish();
				} else {
					DataHolderClass.getInstance().set_videoId(vdoID);
					PlayVideoScreen.scanProd = scanProduct;
					/*startActivity(new Intent(ScanMob.this,
							PlayVideoScreen.class));*/
					if (android.os.Build.VERSION.SDK_INT >= 21)
					{
						Intent intent = YouTubeStandalonePlayer.createVideoIntent(ScanMob.this,
								CommonData.YOUTUBE_API_KEY, vdoID);
						startActivity(intent);
					}
					else
					{

						startActivity(new Intent(ScanMob.this, PlayVideoScreen.class));
					}
					finish();
				}
			} catch (Exception e) {
				e.printStackTrace();
				showDialog(e.getMessage());
			}
				
			/*try {
				DataHolderClass.getInstance().set_videoId(
						obj.getJSONArray("ProductData").getJSONObject(0)
								.getString("MultimediaContent"));
				PlayVideoScreen.fromScan = true;
				startActivity(new Intent(ScanMob.this, PlayVideoScreen.class));
				finish();
			} catch (Exception e) {
				e.printStackTrace();
				showDialog(e.getMessage());
			}*/
		}
	}
}
