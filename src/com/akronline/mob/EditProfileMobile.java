package com.akronline.mob;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.R;
import com.progressbar.GoogleProgressDialog;

public class EditProfileMobile extends BaseFragment implements OnClickListener {

	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
	private static final String NAMESPACE = "http://tempuri.org/";

	Context ctx;
	Button btnSubmit, btnCancel;
	CheckBox cbMail;
	TextView email, fname, last, comp, phone, addr, city, state, cntry, zip;
	EditText etEmail, etFName, etMName, etLName, etComp, etPhone, etFax,
			etAddress, etCity, etState, etZip, etASI, etUPIC, etPPI, etSAGE;
	Spinner spinCountry, spinState;
	FrameLayout stateContainer;
	String userID, repID;
	ScrollView scrollView;
	TextView txtLoading;
	private ArrayList<String> countries = new ArrayList<String>();
	private ArrayList<String> countryIds = new ArrayList<String>();
	private Map<String, String> countryIdMap = new HashMap<String, String> ();
	private ArrayList<String> states = new ArrayList<String>();
	private ArrayList<String> stateIds = new ArrayList<String>();
	private Map<String, String> stateIdMap = new HashMap<String, String> ();
	

	SharedPreferences prefs;
	boolean isLogin = false;
	String emaiID;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View _view = inflater.inflate(R.layout.edit_profile_fragment, null);
		
		Log.e("tag", "EditProfileMobile");

		prefs = getActivity().getSharedPreferences("LoginPref", 0);
		userID = prefs.getString("USER-ID", "");
		emaiID = prefs.getString("USER-EMAIL", "");
		repID = prefs.getString("REP-ID", "0");

		scrollView = (ScrollView) _view.findViewById(R.id.scroll);
		email = (TextView) _view.findViewById(R.id.email);
		fname = (TextView) _view.findViewById(R.id.fName);
		last = (TextView) _view.findViewById(R.id.lName);
		comp = (TextView) _view.findViewById(R.id.compName);
		phone = (TextView) _view.findViewById(R.id.phone);
		addr = (TextView) _view.findViewById(R.id.address);
		city = (TextView) _view.findViewById(R.id.city);
		state = (TextView) _view.findViewById(R.id.state);
		cntry = (TextView) _view.findViewById(R.id.country);
		zip = (TextView) _view.findViewById(R.id.zip);
		txtLoading = (TextView) _view.findViewById(R.id.txtWait);
		btnCancel = (Button) _view.findViewById(R.id.btnCancel);
		btnSubmit = (Button) _view.findViewById(R.id.btnSubmit);
		cbMail = (CheckBox) _view.findViewById(R.id.cbMail);
		etEmail = (EditText) _view.findViewById(R.id.etEmail);
		etFName = (EditText) _view.findViewById(R.id.etFName);
		etLName = (EditText) _view.findViewById(R.id.etLName);
		etMName = (EditText) _view.findViewById(R.id.etMname);
		etComp = (EditText) _view.findViewById(R.id.etComp);
		etPhone = (EditText) _view.findViewById(R.id.etPhone);
		etFax = (EditText) _view.findViewById(R.id.etFax);
		etAddress = (EditText) _view.findViewById(R.id.etAddress);
		etCity = (EditText) _view.findViewById(R.id.etCity);
		etState = (EditText) _view.findViewById(R.id.etState);
		spinState = (Spinner) _view.findViewById(R.id.spinState);
		stateContainer = (FrameLayout) _view.findViewById(R.id.stateContainer);
		etZip = (EditText) _view.findViewById(R.id.etZip);
		etASI = (EditText) _view.findViewById(R.id.etASI);
		etUPIC = (EditText) _view.findViewById(R.id.etUPIC);
		etPPI = (EditText) _view.findViewById(R.id.etPPI);
		etSAGE = (EditText) _view.findViewById(R.id.etSAGE);
		spinCountry = (Spinner) _view.findViewById(R.id.spinCountry);

		email.setText(Html.fromHtml("Email Address <font color=red>*</font>"));
		fname.setText(Html.fromHtml("First Name <font color=red>*</font>"));
		last.setText(Html.fromHtml("Last Name <font color=red>*</font>"));
		comp.setText(Html.fromHtml("Company Name <font color=red>*</font>"));
		phone.setText(Html.fromHtml("Phone Number <font color=red>*</font>"));
		addr.setText(Html.fromHtml("Address <font color=red>*</font>"));
		city.setText(Html.fromHtml("City <font color=red>*</font>"));
		state.setText(Html.fromHtml("State <font color=red>*</font>"));
		cntry.setText(Html.fromHtml("Country <font color=red>*</font>"));
		zip.setText(Html.fromHtml("Zip Code <font color=red>*</font>"));

		spinCountry.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if (position == 0) {
					etState.setVisibility(View.GONE);
					stateContainer.setVisibility(View.VISIBLE);
				} else {
					etState.setVisibility(View.VISIBLE);
					stateContainer.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		btnSubmit.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		
		final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(_view.getWindowToken(), 0);
	    
		return _view;
	}

	@Override
	public void onStart() {
		super.onStart();
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		new GetProfileData().execute();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			// String title = DataHolderClass.getInstance().get_titleCatagory();
			((HomeMob) getActivity()).setTitle("Edit Profile");
		} catch (Exception e) {
			e.printStackTrace();
		}
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.search.setVisibility(View.GONE);
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnSubmit:
				if (!etEmail.getText().toString()
						.matches(Patterns.EMAIL_ADDRESS.pattern())) {
					showToast("Please enter valid Email");
					return;
				}
				if (etFName.getText().toString().isEmpty()) {
					showToast("First Name is required");
					return;
				}
				if (etLName.getText().toString().isEmpty()) {
					showToast("Last Name is required");
					return;
				}
				if (etComp.getText().toString().isEmpty()) {
					showToast("Company Name is required");
					return;
				}
				if (etPhone.getText().toString().isEmpty()) {
					showToast("Phone number is required");
					return;
				}
				if (!etPhone.getText().toString()
						.matches(Patterns.PHONE.pattern())) {
					showToast("Phone number is invalid");
					return;
				}
				if (!etFax.getText().toString().isEmpty()
						&& !etFax.getText().toString()
								.matches(Patterns.PHONE.pattern())) {
					showToast("Fax number is invalid");
					return;
				}
				if (etAddress.getText().toString().isEmpty()) {
					showToast("Address is required");
					return;
				}
				if (etCity.getText().toString().isEmpty()) {
					showToast("City is required");
					return;
				}
				if (etState.getVisibility() == View.VISIBLE
						&& etState.getText().toString().isEmpty()) {
					showToast("State is required");
					return;
				}
				if (etZip.getText().toString().isEmpty()) {
					showToast("Zip code is required");
					return;
				}
				if (etZip.getText().toString().length() < 5) {
					etZip.requestFocus();
					showToast("Zip code is invalid");
					return;
				}
				// all OK call service
				new StoreData().execute();

				break;
			case R.id.btnCancel:
				getActivity().onBackPressed();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception :" + e.getLocalizedMessage());
		}
	}

	// private void scrollTo(EditText editText) {
	// // TODO fix scrolling
	// this.getWindow().setSoftInputMode(
	// WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	// // scrollView.scroll
	// }

	private void showToast(String msg) {
		Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
	}

	
	private class GetProfileData1 extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "FetchProfileData";
		private static final String SOAP_ACTION = "http://tempuri.org/FetchProfileData";
		JSONObject obj;

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				Log.v("RESP", envelope.bodyIn.toString());
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					
//					Log.e("result", result.toString());
//					Log.e("data", data);
					obj = new JSONObject(data);
					
					Log.e("data", obj.getJSONArray("ProfileData")
							.getJSONObject(0)+"");
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (result != null) {
				} else {
					JSONObject profile = obj.getJSONArray("ProfileData")
							.getJSONObject(0);
					if (profile.getString("ResponseCode").equals("0")) {
						repID = profile.getString("SalesRepId");
						SharedPreferences.Editor editor = getActivity()
								.getApplicationContext()
								.getSharedPreferences("LoginPref", 0).edit();
						editor.putString("REP-ID", repID);
						editor.commit();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				showToast("Error Occured: Unable to fetch details");
				txtLoading.setText("Error: Unable to fetch details!");
			}
			getActivity().onBackPressed();
		}
	}
		
	private class GetProfileData extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "FetchProfileData";
		private static final String SOAP_ACTION = "http://tempuri.org/FetchProfileData";
		JSONObject obj;

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				Log.v("RESP", envelope.bodyIn.toString());
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.d("result", result.toString());
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (result != null) {
					showToast(result);
					txtLoading.setText(result);
				} else {
					JSONArray arr = obj.getJSONArray("Country");
					countries.clear();
					countryIds.clear();
					countryIdMap.clear();
					
					for (int i = 0; i < arr.length(); i++) {
						countries.add(arr.getJSONObject(i).getString("countryName"));
						countryIds.add(arr.getJSONObject(i).getString("countryId"));
					}
					Collections.reverse(countries);
					Collections.reverse(countryIds);
					int temp = 0;
					for(String countryID : countryIds){
						countryIdMap.put(countryID, temp+"");
						temp++;
						
					}
					
					arr = obj.getJSONArray("State");
					states.clear();
					stateIds.clear();
					stateIdMap.clear();
					
					for (int i = 0; i < arr.length(); i++) {
						
						stateIds.add(arr.getJSONObject(i).getString("stateId"));
						stateIdMap.put(arr.getJSONObject(i).getString("stateId"), i+"");
						states.add(arr.getJSONObject(i).getString("stateName"));
					}
					spinCountry.setAdapter(new ArrayAdapter<String>(
							getActivity(), android.R.layout.simple_list_item_1,
							android.R.id.text1, countries));
					spinState.setAdapter(new ArrayAdapter<String>(
							getActivity(), android.R.layout.simple_list_item_1,
							android.R.id.text1, states));

					JSONObject profile = obj.getJSONArray("ProfileData")
							.getJSONObject(0);
					if (profile.getString("ResponseCode").equals("0")) {
						etEmail.setText(profile.getString("email"));
						etFName.setText(profile.getString("firstName"));
						etMName.setText(profile.getString("middleName"));
						etLName.setText(profile.getString("lastName"));
						etComp.setText(profile.getString("companyName"));
						etPhone.setText(profile.getString("phone"));
						etFax.setText(profile.getString("fax"));
						etAddress.setText(profile.getString("address"));
						etCity.setText(profile.getString("city"));
						etState.setText(profile.getString("state"));
						etZip.setText(profile.getString("zip"));
						etASI.setText(profile.getString("asi"));
						etUPIC.setText(profile.getString("upic"));
						etPPI.setText(profile.getString("ppai"));
						etSAGE.setText(profile.getString("sage"));
						cbMail.setChecked(profile.getString("onlySubscription")
								.equals("1"));
						
						String stateID = "0";
						try {
							String _countryId = profile.getString("countryId");
							spinCountry.setSelection(Integer.parseInt(countryIdMap.get(_countryId)));
							stateID = profile.getString("StateId");
							
						} catch (NumberFormatException e) {
							e.printStackTrace();
							spinCountry.setSelection(0);
							stateID = "0";
						}
						
						if (Integer.parseInt(stateID) != 0) {
							etState.setVisibility(View.GONE);
							stateContainer.setVisibility(View.VISIBLE);
							spinState.setSelection(Integer.parseInt(stateIdMap.get(stateID)));
							
						} else {
							etState.setVisibility(View.VISIBLE);
							stateContainer.setVisibility(View.GONE);
						}

						repID = profile.getString("SalesRepId");
						SharedPreferences.Editor editor = getActivity()
								.getApplicationContext()
								.getSharedPreferences("LoginPref", 0).edit();
						editor.putString("REP-ID", repID);
						editor.commit();

						txtLoading.setVisibility(View.GONE);

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				showToast("Error Occured: Unable to fetch details");
				txtLoading.setText("Error: Unable to fetch details!");
			}
		}
	}

	private class StoreData extends AsyncTask<String, String, String> {

		private static final String METHOD_NAME = "SaveProfileData";
		private static final String SOAP_ACTION = "http://tempuri.org/SaveProfileData";
		GoogleProgressDialog dialog;
		String response;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... p) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("EmailId", etEmail.getText().toString());
			request.addProperty("FirstName", etFName.getText().toString()
					.trim());
			request.addProperty("MiddleName", etMName.getText().toString()
					.trim());
			request.addProperty("LastName", etLName.getText().toString().trim());
			request.addProperty("CompanyName", etComp.getText().toString()
					.trim());
			request.addProperty("Phone", etPhone.getText().toString().trim());
			request.addProperty("fax", etFax.getText().toString().trim());
			request.addProperty("address", etAddress.getText().toString().trim());
			request.addProperty("city", etCity.getText().toString().trim());
			request.addProperty("countryId", countryIds.get(spinCountry.getSelectedItemPosition()));
			
			if (spinCountry.getSelectedItemPosition() == 0) {
				request.addProperty("state", spinState.getSelectedItem().toString());
				request.addProperty("stateid", stateIds.get(spinState.getSelectedItemPosition()));
				
			} else {
				request.addProperty("state", etState.getText().toString());
				request.addProperty("stateid", "0");
			}
			request.addProperty("zip", etZip.getText().toString().trim());
			request.addProperty("asi", etASI.getText().toString().trim());
			request.addProperty("upic", etUPIC.getText().toString().trim());
			request.addProperty("ppai", etPPI.getText().toString().trim());
			request.addProperty("sage", etSAGE.getText().toString().trim());
			request.addProperty("onlySubscription", cbMail.isChecked() ? "1": "0");

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					JSONObject obj = new JSONObject(data);
					if (obj.getJSONArray("ProfileData").getJSONObject(0)
							.getString("ResponseCode").equals("0")) {
						response = "0";
					} else {
						return "Error: Changes not saved";
					}
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Server responded an error";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null) {
				showToast(result);
			} else if (response.equals("0")) {
				// success try to close dialog
				new GetProfileData1().execute();
				showToast("Changes saved successfully");
			}
		}
	}

}
