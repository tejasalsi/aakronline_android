package com.akronline.mob;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akronline.R;
import com.datamodel.Product;
import com.progressbar.GoogleProgressDialog;

public class RequestOrderFragment extends BaseFragment implements OnClickListener, OnCheckedChangeListener, OnItemSelectedListener{
	
	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";
	private static final String NAMESPACE = "http://tempuri.org/";
	private static final String[] categories = { "New Products",
			"Mood Products", "Best Sellers", "Snacks" };
	private static final String[] compnies = { "FedEx", "UPS", "Others" };
	private static final String KEY_PREF_NAME = "CART";
	private ArrayList<Product> productList = new ArrayList<Product>();
	private String category;
	private String subCategory;
	private String productID;
	private Context ctx;
	private String userID;
	private Button btnAdd, btnSubmit, btnCancel;
	private EditText etQuant, etAcc, etMethod;
	private Spinner spinCategories, spinSubCat, spinProduct, spinColor,
			spinComp;
	private CheckBox cbSpot, cbFCD;
	Set<String> prods;
	SharedPreferences prefs;
	// String cartdata;
	LinearLayout items;
	ArrayList<LinearLayout> viewsList = new ArrayList<LinearLayout>();
	
	boolean isLogin = false;
	String emaiID;
	String repID;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View _view = inflater.inflate(R.layout.order_sample_fragment, null);

		Log.e("tag", "RequestOrderFragment");
		prefs =getActivity().getSharedPreferences("LoginPref", 0);
		
		userID = prefs.getString("USER-ID", "");
		emaiID = prefs.getString("USER-EMAIL", "");
		repID = prefs.getString("REP-ID", "0");
		
		prefs = ((HomeMob)getActivity()).getSharedPreferences(KEY_PREF_NAME,
				0);
		// cartdata = prefs.getString(KEY_PREF_NAME, "");
		prods = prefs.getStringSet(KEY_PREF_NAME, null);
		
		btnAdd = (Button) _view.findViewById(R.id.btnAdd);
		btnSubmit = (Button) _view.findViewById(R.id.btnSubmit);
		btnCancel = (Button) _view.findViewById(R.id.btnCancel);

		spinCategories = (Spinner) _view.findViewById(R.id.spinCategory);
		spinSubCat = (Spinner) _view.findViewById(R.id.spinSubCat);
		spinProduct = (Spinner) _view.findViewById(R.id.spinProduct);
		spinColor = (Spinner) _view.findViewById(R.id.spinColors);
		spinComp = (Spinner) _view.findViewById(R.id.spinShipComp);
		etQuant = (EditText) _view.findViewById(R.id.etQuantity);
		etAcc = (EditText) _view.findViewById(R.id.etShipAc);
		etMethod = (EditText) _view.findViewById(R.id.etShipMethod);
		cbSpot = (CheckBox) _view.findViewById(R.id.spot_checkbox);
		cbFCD = (CheckBox) _view.findViewById(R.id.fcd_checkbox);
		items = (LinearLayout) _view.findViewById(R.id.items);

		btnAdd.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);

		spinCategories.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, categories));
		
		spinComp.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, compnies));

		cbSpot.setOnCheckedChangeListener(this);
		cbFCD.setOnCheckedChangeListener(this);

		spinCategories.setOnItemSelectedListener(this);
		spinSubCat.setOnItemSelectedListener(this);
		spinProduct.setOnItemSelectedListener(this);
		
		return _view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		inflateCart();

	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
//			String title = DataHolderClass.getInstance().get_titleCatagory();
			((HomeMob) getActivity()).setTitle("Order Sample");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.search.setVisibility(View.GONE);
	}
	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnCancel:
				getActivity().onBackPressed();
				break;
			case R.id.btnAdd:
				if (productID == null) {
					showToast("Select a product");
					return;
				}
				if (etQuant.getText().toString().isEmpty()) {
					showToast("Enter Quantity");
					return;
				}
				int quantity = 0;
				try {
					quantity = Integer.valueOf(etQuant.getText().toString());
					if (quantity == 0)
						throw new NumberFormatException();
				} catch (NumberFormatException e) {
					e.printStackTrace();
					showToast("Please enter valid Quantity");
					return;
				}
				// add to cart
				String item = spinProduct.getSelectedItem().toString() + ":"
						+ etQuant.getText().toString() + ":"
						+ spinColor.getSelectedItem().toString();
				if (prods == null) {
					prods = new HashSet<String>();
				}
				prods.add(item);
				Editor editor = prefs.edit();
				editor.putStringSet(KEY_PREF_NAME, prods);
				editor.commit();
				etQuant.setText("");
				showToast("Item added");
				inflateCart();
				break;
			case R.id.btnSubmit:
				if ((prods == null || prods.size() == 0) && productID == null) {
					showToast("Select products first");
					return;
				}
				if (!cbFCD.isChecked() && !cbSpot.isChecked()) {
					showToast("Choose Imprint Method");
					return;
				}
				// if (etAcc.getText().toString().isEmpty()) {
				// showToast("Enter Shipping Account");
				// return;
				// }
				// if (etMethod.getText().toString().isEmpty()) {
				// showToast("Enter Shipping Method");
				// return;
				// }
				int q = 0;
				try {
					q = Integer.valueOf(etQuant.getText().toString());
				} catch (NumberFormatException e) {
					q = 0;
				}
				if (prods == null) {
					if (q == 0) {
						showToast("Enter valid Quantity");
						return;
					} else {
						prods = new HashSet<String>();
						prods.add(spinProduct.getSelectedItem().toString()
								+ ":" + etQuant.getText().toString() + ":"
								+ spinColor.getSelectedItem().toString());
					}
				}
				new Submit().execute();
				break;
			case R.id.btnDel:
				String prodToDel = (String) v.getTag();
				prods.remove(prodToDel);
				Editor ed = prefs.edit();
				ed.putStringSet(KEY_PREF_NAME, prods);
				ed.commit();
				etQuant.setText("");
				showToast("Item removed");
				inflateCart();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception: " + e.getMessage());
		}
	}

	private void showToast(String msg) {
		Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_LONG)
				.show();
	}

	@SuppressLint("InflateParams")
	private void inflateCart() {
		if (prods == null || prods.size() == 0) {
			items.setVisibility(View.GONE);
			return;
		}
		try {
			for (LinearLayout l : viewsList)
				items.removeView(l);

			viewsList.clear();
			items.setVisibility(View.VISIBLE);
			LayoutInflater inflater = LayoutInflater.from(getActivity());
			int i = 0;
			for (String p : prods) {
				LinearLayout view = (LinearLayout) inflater.inflate(
						R.layout.sample_item, null);
				TextView txtSR, txtItem, txtQuant, txtColor;
				ImageView btnDel;
				txtColor = (TextView) view.findViewById(R.id.txtColor);
				txtItem = (TextView) view.findViewById(R.id.txtItem);
				txtQuant = (TextView) view.findViewById(R.id.txtQuantity);
				txtSR = (TextView) view.findViewById(R.id.txtSR);
				btnDel = (ImageView) view.findViewById(R.id.btnDel);
				btnDel.setTag(p);
				btnDel.setOnClickListener(this);
				txtSR.setText("" + (i + 1));
				String[] prod = p.split(":");
				txtItem.setText(prod[0]);
				txtQuant.setText(prod[1]);
				txtColor.setText(prod[2]);
				items.addView(view, i + 1);
				viewsList.add(view);
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception: " + e.getMessage());
		}

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked && buttonView.getId() == R.id.spot_checkbox) {
			cbFCD.setChecked(false);
		} else if (isChecked && buttonView.getId() == R.id.fcd_checkbox) {
			cbSpot.setChecked(false);
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position,
			long id) {
		try {
			switch (parent.getId()) {
			case R.id.spinCategory:
				switch (position) {
				case 0:
					category = "newproducts";
					break;
				case 1:
					category = "moodproducts";
					break;
				case 2:
					category = "bestsellers";
					break;
				case 3:
					category = "snacks";
					break;
				}
				Log.v("SVD", "callign SVG for " + category);
				new GetSubCategories().execute();
				break;
			case R.id.spinSubCat:
				subCategory = spinSubCat.getSelectedItem().toString();
				productID = null;
				ArrayList<String> temp = new ArrayList<String>();
				for (Product p : productList) {
					if (p.getSubCatagory().equals(subCategory)) {
						temp.add(p.toString());
					}
				}
				spinProduct.setAdapter(new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1, temp));
				break;
			case R.id.spinProduct:
				productID = spinProduct.getSelectedItem().toString().split(" ")[0];
				new GetColors().execute();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Exception: " + e.getMessage());
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		showToast("Select an item");
	}

	private class GetSubCategories extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "GetProductListing";
		private static final String SOAP_ACTION = "http://tempuri.org/GetProductListing";
		GoogleProgressDialog dialog;
		JSONObject obj;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("Type", category);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			productList.clear();
			if (result != null) {
				showToast(result);
				dialog.dismiss();
				return;
			}
			try {
				JSONArray prods = obj.getJSONArray("Products");
				for (int i = 0; i < prods.length(); i++) {
					JSONObject prod = prods.getJSONObject(i);
					Product p = new Product();
					p.setID(prod.getString("productCode"));
					p.setCatagory(prod.getString("Category"));
					p.setSubCatagory(prod.getString("Subcategory"));
					p.setName(prod.getString("productname"));
					productList.add(p);
				}
				HashSet<String> temp = new HashSet<String>();
				for (Product prod : productList) {
					temp.add(prod.getSubCatagory());
				}
				String[] subCategories = new String[temp.size()];
				temp.toArray(subCategories);
				spinSubCat.setAdapter(new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1, subCategories));
			} catch (Exception e) {
				e.printStackTrace();
				showToast("No items under this category");
			} finally {
				dialog.dismiss();
			}
		}
	}
	

//	@Override
//	public void onPause() {
//		// TODO Auto-generated method stub
//		super.onPause();
//		getActivity().getFragmentManager().popBackStackImmediate();
//		onDestroy();
//	}
	
	
	private class GetColors extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "GetProductData";
		private static final String SOAP_ACTION = "http://tempuri.org/GetProductData";
		GoogleProgressDialog dialog;
		JSONObject obj;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("ProductCode", productID);
			request.addProperty("userId", userID);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result != null) {
				showToast(result);
				dialog.dismiss();
				return;
			}
			try {
				String[] colors = obj.optJSONArray("ProductData")
						.optJSONObject(0).optString("ItemColors").split("\\|");
				if (colors != null) {
					// Remove spaces
					for (int i = 0; i < colors.length; i++) {
						colors[i] = colors[i].trim();
					}
					spinColor.setAdapter(new ArrayAdapter<String>(getActivity(),
							android.R.layout.simple_list_item_1,
							android.R.id.text1, colors));
				}

			} catch (Exception e) {
				e.printStackTrace();
				showToast("Exception: " + e.getMessage());
			} finally {
				dialog.dismiss();
			}
		}

	}

	private class Submit extends AsyncTask<Void, Void, String> {

		private static final String METHOD_NAME = "SetOrderSample";
		private static final String SOAP_ACTION = "http://tempuri.org/SetOrderSample";
		GoogleProgressDialog dialog;
		JSONObject obj;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			String CodeQtyColor = "";
			for (String p : prods) {
				String[] temp = p.split(":");
				String id = temp[0].split(" ")[0];
				String qty = temp[1];
				String color = temp[2];
				CodeQtyColor = CodeQtyColor + id + "|" + qty + "|" + color
						+ ";";
			}
			Log.e("CodeQtyColor", CodeQtyColor);
			request.addProperty("CodeQtyColor", CodeQtyColor);
			request.addProperty("ImprintMethod", cbSpot.isChecked() ? "Spot"
					: "FCD");
			request.addProperty("ShippingCompany",
					compnies[spinComp.getSelectedItemPosition()]);
			request.addProperty("ShippingAccount", etAcc.getText().toString()
					.trim());
			request.addProperty("ShippingMethod", etMethod.getText().toString()
					.trim());

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			if (result != null) {
				showToast(result);
				return;
			}
			try {
				String code = obj.getJSONArray("Data").getJSONObject(0)
						.getString("responseCode");
				if (code.equals("0")) {
					Editor editor = prefs.edit();
					editor.putStringSet(KEY_PREF_NAME, null);
					editor.commit();
					showToast("Order Submitted");
					getActivity().onBackPressed();
				} else {
					showToast("Server Responded an error");
				}
			} catch (Exception e) {
				e.printStackTrace();
				showToast("Invalid Response from server");
			}
		}

	}

}
