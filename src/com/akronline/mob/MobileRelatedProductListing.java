package com.akronline.mob;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.adapters.FilterAdapter;
import com.adapters.ProductListingAdapter;
import com.akronline.PlayVideoScreen;
import com.akronline.ProdList;
import com.akronline.R;
import com.commondata.CommonData;
import com.dataholder.DataHolderClass;
import com.datamodel.Product;
import com.progressbar.GoogleProgressDialog;

public class MobileRelatedProductListing extends Fragment implements
		OnItemClickListener, OnClickListener {
	private static final String SOAP_ACTION = "http://tempuri.org/GetProductListing";
	private static final String NAMESPACE = "http://tempuri.org/";
	private static final String METHOD_NAME = "GetProductListing";
	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	float min, mid1, mid2, mid3, max;
	TextView pr1, pr2, pr3, pr4;
	// LinearLayout catItems;
	// LinearLayout subItems;
	// ImageView imgCatArrow;
	// ImageView imgSubArrow;
	// TextView txtCurrntCat;
	// TextView txtCurretSub;
	HashSet<String> catList = new HashSet<String>();
	TextView titleCurrentCategory;

	GoogleProgressDialog _dialog;
	public static ArrayList<Product> _productList = new ArrayList<Product>();
	public static ListView productList;
	public static ProductListingAdapter _adapter;
	LinearLayout ll1, extra_layout;
	TextView txtCount;
	Button filter;
	Button _scrollup;
	private PopupWindow _dataWindow;
	ScrollView suv_layout;
	// Button catagory_header;
	ListView lsy;
	ArrayList<String> _list = new ArrayList<String>();
	ArrayList<String> values = new ArrayList<String>();
	ArrayList<String> valuesWithCount = new ArrayList<String>();
	static ArrayList<Product> tempList = new ArrayList<Product>();

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.mobile_listing, null);
		Log.e("tag", "MobileRelatedProductListing");
		try {
			productList = (ListView) v.findViewById(R.id.productList);
			productList.setOnItemClickListener(this);
			iniPopupWindow();
			txtCount = (TextView) v.findViewById(R.id.set_product_count);
			filter = (Button) v.findViewById(R.id.filter);
			filter.setOnClickListener(this);

			_scrollup = (Button) v.findViewById(R.id.scrolldown);
			_scrollup.setOnClickListener(this);
			HomeMob.search.setVisibility(View.VISIBLE);

			HomeMob.search.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (HomeMob.searchField.getVisibility() == View.GONE) {
						HomeMob.title.setVisibility(View.GONE);
						HomeMob.searchField.setVisibility(View.VISIBLE);
					} else if (HomeMob.searchField.getVisibility() == View.VISIBLE) {
						HomeMob.title.setVisibility(View.VISIBLE);
						HomeMob.searchField.setVisibility(View.GONE);
					}
				}
			});
			productList.setOnScrollListener(new OnScrollListener() {
				@Override
				public void onScrollStateChanged(AbsListView view,
						int scrollState) {
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,
						int visibleItemCount, int totalItemCount) {
					if (firstVisibleItem > 4) {
						_scrollup.setVisibility(View.VISIBLE);
					} else {
						_scrollup.setVisibility(View.GONE);
					}
				}
			});

			new Sample().execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

		HomeMob.searchField
				.setOnEditorActionListener(new OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							if (HomeMob.searchField.getText().toString()
									.isEmpty()) {
								Toast.makeText(getActivity(),
										"enter search keyword",
										Toast.LENGTH_SHORT).show();
							} else {
								Log.d("search", HomeMob.searchField.getText()
										.toString());
								new Search().execute(HomeMob.searchField
										.getText().toString());
							}
							getActivity().getWindow().setSoftInputMode(
									SOFT_INPUT_STATE_HIDDEN);
							return true;
						}
						return false;
					}
				});
		return v;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		HomeMob.search.setVisibility(View.GONE);
		HomeMob.searchField.setVisibility(View.GONE);
		HomeMob.title.setVisibility(View.VISIBLE);
	}

	private class Sample extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			_dialog = new GoogleProgressDialog(getActivity());
			_dialog.setCancelable(false);
			_dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				_productList.clear();
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				request.addProperty("Type", DataHolderClass.getInstance()
						.get_catagoryName());
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.setOutputSoapObject(request);
				envelope.dotNet = true;
				try {
					HttpTransportSE androidHttpTransport = new HttpTransportSE(
							URL);
					androidHttpTransport.call(SOAP_ACTION, envelope);
					
					if (envelope.bodyIn instanceof SoapFault) {
						String str = ((SoapFault) envelope.bodyIn).faultstring;
						Log.i("adddddddd", str);

					} else {
						SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
						String _result = resultsRequestSOAP.getProperty(0)
								.toString();
						System.out.println("data" + _result);
						Log.e("MobilePructList", "" + _result);
						JSONObject _obj = new JSONObject(_result);
						
						
						if (_obj != null) {
							JSONArray arr = _obj.optJSONArray("Products");
							for (int i = 0; i < arr.length(); i++) {
								JSONObject obj = arr.optJSONObject(i);
								if (obj.getString("Subcategory").equals(
										PlayVideoScreen.scanProd
												.getSubCatagory())) {
									Product p = new Product();
									p.setName(obj.optString("productname"));
									p.setID(obj.optString("productCode"));
									p.setImageURL(obj.optString("ImageUrl"));
									p.setPrice(obj.optString("price"));
									p.setCatagory(obj.getString("Category"));
									p.setSubCatagory(obj
											.getString("Subcategory"));
									_productList.add(p);
								}
							}
						}
					}
					
				} catch (NullPointerException e) {
					e.printStackTrace();
				} catch (ClassCastException cc) {
					cc.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (_productList.size() > 0) {
					_adapter = new ProductListingAdapter(getActivity(),
							_productList);
					_adapter.notifyDataSetChanged();
					productList.setAdapter(_adapter);
					int count = _productList.size();
					Iterator<Product> productIterator = _productList.iterator();
					if (CommonData.scannedProduct != null) {
						int i = 0;
						while (productIterator.hasNext()) {

							Product product = productIterator.next();

							if (product.getProductCode().equals(CommonData.scannedProduct.getProductCode())) {
								break;
							}
							i++;

						}
						productList.setSelection(i-3);
						CommonData.scannedProduct = null;
					}
					txtCount.setText(count + " Products");
					setFilterData();// /////////////////filter the
									// data////////////////
					_dialog.dismiss();
				} else {
					_dialog.dismiss();
				}
				onResume();
			} catch (Exception e) {
				e.printStackTrace();
				if (_dialog.isShowing()) {
					_dialog.dismiss();
				}
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		try {
			String title = DataHolderClass.getInstance().get_titleCatagory();
			((HomeMob) getActivity()).setTitle(title);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		try {
			String _id = _productList.get(position).getProductCode();
			DataHolderClass.getInstance().set_pId(_id);
			ProductDetailMobile _pdMobile = new ProductDetailMobile();
			((HomeMob) getActivity()).letsHaveSomeTransactions(R.id.container,
					_pdMobile, "MobileDetails", null);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.filter:
			try {
				if (_dataWindow.isShowing()
						&& filter.getText().equals("Filter")) {
					_dataWindow.dismiss();
				} else if (!_dataWindow.isShowing()
						&& filter.getText().equals("Filter")) {
					if (lsy.getVisibility() == View.VISIBLE)
						lsy.setVisibility(View.INVISIBLE);
					suv_layout.setVisibility(View.VISIBLE);
					_dataWindow.showAtLocation(filter, Gravity.END
							| Gravity.TOP, 25, 215);
				}
				/*
				 * else if(filter.getText().equals("Back")) {
				 * lsy.setVisibility(View.INVISIBLE);
				 * suv_layout.setVisibility(View.VISIBLE);
				 * _dataWindow.showAtLocation(filter, Gravity.END | Gravity.TOP,
				 * 25, 215); filter.setText("Filter"); }
				 */
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case R.id.scrolldown:
			try {
				productList.setSelection(0);
				_scrollup.setVisibility(View.GONE);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		case R.id.set_current_catagory:
			try {
				// filter.setText("Back");
				_list.clear();
				suv_layout.setVisibility(View.INVISIBLE);
				// catagory_header.setText("Select Catagory");
				if (lsy.getVisibility() == View.VISIBLE)
					lsy.setVisibility(View.INVISIBLE);
				else {
					lsy.setVisibility(View.VISIBLE);
					String _abc = DataHolderClass.getInstance().get_filterBy();
					for (int i = 0; i < _productList.size(); i++) {
						if (_productList.get(i).getCatagory().contains(_abc)) {
							String _d = _productList.get(i).getSubCatagory();
							System.out.println("abc" + _d);
							_list.add(_d);
						}
					}
					try {
						Set<String> unique = new HashSet<String>(_list);
						for (String key : unique) {
							System.out.println("in loop");
							System.out.println(key + ": "
									+ Collections.frequency(_list, key));
							
							valuesWithCount.add(key+ " (" +Collections.frequency(_list,key)+ ")");
							values.add(key);// + " (" +
											// Collections.frequency(_list,
											// key)+ ")");

						}
						Set<String> set = new TreeSet<String>(
								String.CASE_INSENSITIVE_ORDER);
						set.addAll(values);
						values = new ArrayList<String>(set);
						FilterAdapter _adapter = new FilterAdapter(
								getActivity(), values, valuesWithCount);
						_adapter.notifyDataSetChanged();
						lsy.setAdapter(_adapter);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		default:
			break;
		}
	}

	@SuppressLint("InflateParams")
	private void iniPopupWindow() {
		try {
			TextView txtAsc, txtDesc;
			getActivity();
			LayoutInflater inflater = LayoutInflater.from(getActivity());
			View layout = inflater.inflate(R.layout.data_window_new, null);
			_dataWindow = new PopupWindow(layout);
			_dataWindow.setFocusable(true);

			_dataWindow.setWidth(380);
			_dataWindow.setHeight(LayoutParams.WRAP_CONTENT);

			_dataWindow.setBackgroundDrawable(this.getResources().getDrawable(
					R.drawable.box));
			_dataWindow.setOutsideTouchable(true);

			suv_layout = (ScrollView) layout.findViewById(R.id.suv_layout);
			// subItems = (LinearLayout) layout.findViewById(R.id.subItems);
			// imgSubArrow = (ImageView) layout.findViewById(R.id.imgSubArrow);
			// txtCurretSub = (TextView)
			// layout.findViewById(R.id.txtCurrentSub);
			txtAsc = (TextView) layout.findViewById(R.id.txtAscOrder);
			txtDesc = (TextView) layout.findViewById(R.id.txtDescOrder);
			pr1 = (TextView) layout.findViewById(R.id.priceRange1);
			pr2 = (TextView) layout.findViewById(R.id.priceRange2);
			pr3 = (TextView) layout.findViewById(R.id.priceRange3);
			pr4 = (TextView) layout.findViewById(R.id.priceRange4);
			lsy = (ListView) layout.findViewById(R.id.lsy);
			lsy.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					TextView _v = (TextView) view.findViewById(R.id.textFilter);

					ArrayList<Product> _lists = new ArrayList<Product>();
					_lists.clear();
					for (int i = 0; i < _productList.size(); i++) {
						if (_productList.get(i).getSubCatagory()
								.contains(_v.getText().toString())) {
							_lists.add(_productList.get(i));
						}
					}
					_dataWindow.dismiss();
					_adapter = new ProductListingAdapter(getActivity(), _lists);
					_adapter.notifyDataSetChanged();
					productList.setAdapter(_adapter);
					int count = _lists.size();
					txtCount.setText(count + " Products");
					filter.setText("Filter");
				}
			});
			/*
			 * txtCurretSub.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) { if
			 * (subItems.getVisibility() == View.VISIBLE) {
			 * subItems.setVisibility(View.GONE);
			 * imgSubArrow.setImageResource(R.drawable.right_black); } else {
			 * subItems.setVisibility(View.VISIBLE);
			 * imgSubArrow.setImageResource(R.drawable.down_black); } } });
			 */
			pr1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setRange(getActivity(), min, mid1);
					_dataWindow.dismiss();
				}
			});
			pr2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setRange(getActivity(), mid1, mid2);
					_dataWindow.dismiss();
				}
			});
			pr3.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setRange(getActivity(), mid2, mid3);
					_dataWindow.dismiss();
				}
			});
			pr4.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setRange(getActivity(), mid3, max);
					_dataWindow.dismiss();
				}
			});
			txtAsc.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sortAsc(getActivity());
					_dataWindow.dismiss();
				}
			});
			txtDesc.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sortDesc(getActivity());
					_dataWindow.dismiss();
				}
			});

			// catagory_header = (Button) layout
			// .findViewById(R.id.catagory_header);
			// catagory_header.setText("Filter");

			TextView set_current_catagory = (TextView) layout
					.findViewById(R.id.set_current_catagory);
			set_current_catagory.setText(DataHolderClass.getInstance()
					.get_titleCatagory());
			set_current_catagory.setOnClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class Search extends AsyncTask<String, Void, String> {

		private static final String SOAP_ACTION = "http://tempuri.org/GetProductSearch";
		private static final String METHOD_NAME = "GetProductSearch";
		GoogleProgressDialog dialog;
		JSONObject obj;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("keyword", params[0]);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try {
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null) {
					String data = result.getProperty(0).toString();
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "No Internet Connection";
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return "Unable to reach server";
			} catch (JSONException e) {
				e.printStackTrace();
				return "Invalid response from server";
			} catch (Exception e) {
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (result != null)
					throw new IOException();
				JSONArray arr = obj.getJSONArray("Products");
				if (arr.length() == 1
						&& arr.getJSONObject(0).getString("productCode")
								.equals("")) {
					Toast.makeText(getActivity().getApplicationContext(),
							"No results found", Toast.LENGTH_LONG).show();
				} else {
					_productList.clear();
					_adapter.notifyDataSetChanged();
					for (int i = 0; i < arr.length(); i++) {
						JSONObject prod = arr.getJSONObject(i);
						Product p = new Product();
						p.setID(prod.getString("productCode"));
						p.setName(prod.getString("productname"));
						p.setCatagory(prod.getString("Category"));
						p.setSubCatagory(prod.getString("Subcategory"));
						p.setImageURL(prod.getString("ImageUrl"));
						p.setPrice(prod.getString("price"));
						_productList.add(p);
					}
					ProdList.filterData(getActivity(), _productList);
					setProductCount(_productList);
					setFilterData();
					// ((ProductScreen) getActivity()).setFilterData();
				}

			} catch (IOException e) {
				Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT)
						.show();
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(), "Invalid response from server",
						Toast.LENGTH_SHORT).show();
			} finally {
				dialog.dismiss();
				// ((ProductScreen) getActivity()).setProductCount();
				// ((ProductScreen) getActivity()).setFilterData();
			}

		}
	}

	public void setFilterData() {
		float[] arr = getMinMax();
		final DecimalFormat df = new DecimalFormat();
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(3);
		final LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 1);
		final LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		min = arr[0];
		max = arr[1];
		if (min == max || max == 0) {
			// TODO hide ranges
		} else {
			mid2 = (min + max) / 2;
			mid1 = (min + mid2) / 2;
			mid3 = (mid2 + max) / 2;
			pr1.setText("$ " + df.format(min) + " - $ " + df.format(mid1));
			pr2.setText("$ " + df.format(mid1) + " - $ " + df.format(mid2));
			pr3.setText("$ " + df.format(mid2) + " - $ " + df.format(mid3));
			pr4.setText("$ " + df.format(mid3) + " - $ " + df.format(max));
		}
		// catItems.removeAllViews();
		// subItems.removeAllViews();
		int i = 0;
		catList.clear();
		HashSet<String> subList = new HashSet<String>();
		for (Product product : _productList) {
			subList.add(product.getSubCatagory());
			catList.add(product.getCatagory());
		}
		Log.e("cats", "Total cats:" + catList.size());
		Log.e("subs", "Total subs:" + subList.size());
		if (catList.size() > 1) {
			TextView txtCatAll = new TextView(getActivity());
			View line = new View(getActivity());
			txtCatAll.setText("ALL (" + _productList.size() + ")");
			txtCatAll.setTag("ALL");
			txtCatAll.setLayoutParams(txtParams);
			txtCatAll.setPadding(10, 10, 0, 10);
			txtCatAll.setTextSize(18f);
			// txtCatAll.setOnClickListener(onCategoryClick);
			line.setLayoutParams(lineParams);
			line.setBackgroundColor(Color.GRAY);
			// catItems.addView(line, i++);
			// catItems.addView(txtCatAll, i++);
			// txtCurrntCat.setText("ALL");
			// imgCatArrow.setVisibility(View.VISIBLE);
			for (String cat : catList) {
				View v = new View(getActivity());
				v.setLayoutParams(lineParams);
				v.setBackgroundColor(Color.GRAY);
				v.setLayoutParams(lineParams);
				v.setBackgroundColor(Color.GRAY);
				int count = 0;
				for (Product p : _productList)
					if (p.getCatagory().equals(cat))
						count++;
				TextView tv = new TextView(getActivity());
				tv.setText(cat + " (" + count + ")");
				tv.setTag(cat);
				tv.setLayoutParams(txtParams);
				tv.setPadding(10, 10, 0, 10);
				tv.setTextSize(18f);
				tv.setTag(cat);
				// tv.setOnClickListener(onCategoryClick);
				// catItems.addView(v, i++);
				// catItems.addView(tv, i++);
			}
		} else {
			// imgCatArrow.setVisibility(View.GONE);
			// catItems.setVisibility(View.GONE);
			// txtCurrntCat.setText(DataHolderClass.getInstance()
			// .get_titleCatagory());
		}
		i = 0;
		if (subList.size() == 1) {
			// txtCurretSub.setText("ALL");
			// imgSubArrow.setVisibility(View.GONE);
		} else {
			// imgSubArrow.setVisibility(View.VISIBLE);
			TextView txtSubAll = new TextView(getActivity());
			txtSubAll.setText("ALL (" + ProdList.productList.size() + ")");
			txtSubAll.setTag("ALL");
			txtSubAll.setLayoutParams(txtParams);
			txtSubAll.setPadding(10, 10, 0, 10);
			txtSubAll.setTextSize(18f);
			// txtSubAll.setOnClickListener(onSubcategotyClick);
			View line = new View(getActivity());
			line.setLayoutParams(lineParams);
			line.setBackgroundColor(Color.GRAY);
			// subItems.addView(line, i++);
			// subItems.addView(txtSubAll, i++);
			for (String subcat : subList) {
				View v = new View(getActivity());
				v.setLayoutParams(lineParams);
				v.setBackgroundColor(Color.GRAY);
				v.setLayoutParams(lineParams);
				v.setBackgroundColor(Color.GRAY);
				int count = 0;
				for (Product p : ProdList.productList)
					if (p.getSubCatagory().equals(subcat))
						count++;
				TextView tv = new TextView(getActivity());
				tv.setText(subcat + " (" + count + ")");
				tv.setTag(subcat);
				tv.setLayoutParams(txtParams);
				tv.setPadding(10, 10, 0, 10);
				tv.setTextSize(18f);
				tv.setTag(subcat);
				// tv.setOnClickListener(onSubcategotyClick);
				// subItems.addView(v, i++);
				// subItems.addView(tv, i++);
			}
		}
		setProductCount(_productList);

	}

	public void setProductCount(ArrayList<Product> prodList) {
		if (prodList.size() > 0) {
			// titleCurrentCategory.setText(_productList.get(0).getCatagory());
			txtCount.setText(prodList.size() + " Products");
		}
	}

	public static float[] getMinMax() {
		float[] values = { 0f, 0f };
		ArrayList<Product> temp = _productList;
		Collections.sort(temp, new Ascending());
		try {
			values[0] = Float.valueOf(temp.get(0).getPrice().substring(1));
		} catch (Exception e) {
			values[0] = 0f;
		}
		try {
			values[1] = Float.valueOf(temp.get(temp.size() - 1).getPrice()
					.substring(1));
		} catch (Exception e) {
			values[1] = 0f;
		}
		return values;
	}

	public void sortAsc(Context ctx) {
		tempList = _productList;
		Collections.sort(tempList, new Ascending());
		ProdList.filterData(ctx, tempList);
		setProductCount(tempList);
	}

	public void sortDesc(Context ctx) {
		tempList = _productList;
		Collections.sort(tempList, new Ascending());
		Collections.reverse(tempList);
		ProdList.filterData(ctx, tempList);
		setProductCount(tempList);
	}

	public void setRange(final Context ctx, float min, float max) {
		tempList = new ArrayList<Product>();
		for (Product p : _productList) {
			float price = 0f;
			try {
				price = Float.valueOf(p.getPrice().substring(1));
			} catch (Exception e) {
				price = 0f;
			}
			if (price >= min && price <= max) {
				tempList.add(p);
				Log.d("price", min + " >= " + price + " < " + max);
			}

		}
		ProdList.filterData(ctx, tempList);
		setProductCount(tempList);
	}

	private static class Ascending implements Comparator<Product> {
		@Override
		public int compare(Product lhs, Product rhs) {
			float first = 0f;
			try {
				first = Float.valueOf(lhs.getPrice().substring(1));
			} catch (Exception e) {
				first = 0f;
			}
			float second = 0f;
			try {
				second = Float.valueOf(rhs.getPrice().substring(1));
			} catch (Exception e) {
				second = 0f;
			}
			if (first == second)
				return 0;
			else if (first < second)
				return -1;
			else
				return 1;
		}
	}

}
