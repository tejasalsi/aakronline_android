package com.datamodel;

public class Product {

	private String productCode;
	private String productname;
	private String imageUrl;
	private String desc;
	private String price;
	private String catagory;
	private String subCatagory;

	public String getProductCode() {
		return productCode;
	}

	public void setID(String productCode) {
		this.productCode = productCode;
	}

	public String getName() {
		return productname;
	}

	public void setName(String productname) {
		this.productname = productname;
	}

	public String getImageURL() {
		return imageUrl;
	}

	public void setImageURL(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCatagory() {
		return catagory;
	}

	public void setCatagory(String _catagory) {
		this.catagory = _catagory;
	}

	public String getSubCatagory() {
		return subCatagory;
	}

	public void setSubCatagory(String _subCatagory) {
		this.subCatagory = _subCatagory;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return productCode + " " + productname;
	}

}
