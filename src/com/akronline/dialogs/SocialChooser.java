package com.akronline.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.akronline.R;
import com.akronline.interfaces.DialogCallback;

public class SocialChooser extends Dialog {

	Button btnDone;
	CheckBox cbFB, cbTWT, cbIG, cbPI, cbCS, cbGP, cbLI;
	EditText etOther;

	public SocialChooser(Context ctx, final DialogCallback callback,
			final int reqCode) {
		super(ctx);
		
		final StringBuilder sb = new StringBuilder();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.social_choose);
		getWindow().getDecorView().setBackgroundResource(R.drawable.white_bg);
		cbFB = (CheckBox) findViewById(R.id.cbFB);
		cbTWT = (CheckBox) findViewById(R.id.cbTWT);
		cbIG = (CheckBox) findViewById(R.id.cbIG);
		cbPI = (CheckBox) findViewById(R.id.cbPI);
		cbCS = (CheckBox) findViewById(R.id.cbCS);
		cbGP = (CheckBox) findViewById(R.id.cbGP);
		cbLI = (CheckBox) findViewById(R.id.cbLI);
		etOther = (EditText) findViewById(R.id.etOther);
		btnDone = (Button) findViewById(R.id.btnDone);

		btnDone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (cbFB.isChecked())
					sb.append("Facebook,");
				if (cbTWT.isChecked())
					sb.append("Twitter,");
				if (cbIG.isChecked())
					sb.append("Instagram,");
				if (cbPI.isChecked())
					sb.append("Pintrest,");
				if (cbCS.isChecked())
					sb.append("CommonSku,");
				if (cbGP.isChecked())
					sb.append("Google+,");
				if (cbLI.isChecked())
					sb.append("LinkedIn,");
				if (!etOther.getText().toString().isEmpty()) {
					sb.append(etOther.getText().toString());
				}

				String result = sb.toString();
				if (result.endsWith(","))
					result = result.substring(0, result.length() - 1);
				callback.onDialogSubmit(reqCode, result);
				SocialChooser.this.dismiss();
			}
		});
		
		AlertDialog.Builder adb = new AlertDialog.Builder(ctx);
		Dialog d = adb.setView(new View(ctx)).create();
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(d.getWindow().getAttributes());
	    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
	    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
	    getWindow().setAttributes(lp);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		
/*
		 AlertDialog.Builder adb = new AlertDialog.Builder(this);
		    Dialog d = adb.setView(new View(this)).create();
		    // (That new View is just there to have something inside the dialog that can grow big enough to cover the whole screen.)

		    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		    lp.copyFrom(d.getWindow().getAttributes());
		    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		    lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		    d.show();
		    d.getWindow().setAttributes(lp);*/
	}
}
