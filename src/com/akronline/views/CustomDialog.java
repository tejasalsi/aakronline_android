package com.akronline.views;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;

public class CustomDialog extends Dialog {
	Context mContext;
	View v = null;

	public CustomDialog(Context context, int layoutresId) {
		super(context);
		mContext = context;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(layoutresId);
		v = getWindow().getDecorView();
		v.setBackgroundResource(android.R.color.transparent);
	}
}
