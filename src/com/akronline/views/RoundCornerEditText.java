package com.akronline.views;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.akronline.R;

public class RoundCornerEditText extends EditText{
     
    public RoundCornerEditText(Context context) {
        super(context);
        init();
    }
 
    public RoundCornerEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
 
    public RoundCornerEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
     
    void init() {
         setBackgroundResource(R.drawable.roundcornertextbox);
       setPadding(20, 0, 0, 0);
        }
     
     
    
}
