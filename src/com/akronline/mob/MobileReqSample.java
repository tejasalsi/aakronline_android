package com.akronline.mob;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.adapters.SampleColorAdapter;
import com.akronline.R;
import com.progressbar.GoogleProgressDialog;

public class MobileReqSample extends Fragment implements OnClickListener, OnCheckedChangeListener
{

	Spinner spinner;

	String[] spinnerValues;

	CheckBox cbSpot, cbFCD;

	TextView etQuant;

	private String userID;

	Button submitReqSample;

	private static final String NAMESPACE = "http://tempuri.org/";

	private static final String URL = "http://aakronlineapi.cwwws.com/api.asmx";

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		Log.e("tag", "MobileReqSample");
		userID = getActivity().getApplicationContext().getSharedPreferences("LoginPref", 0).getString("USER-ID", null);
		View v = inflater.inflate(R.layout.request_sample_layout, container, false);
		spinnerValues = getArguments().getStringArray("SPINNER_VALUE");
		spinner = (Spinner) v.findViewById(R.id.spinColorsSample);

		SampleColorAdapter ca = new SampleColorAdapter(getActivity(), R.layout.request_sample_color_dropdown_inflator,
				spinnerValues);
		spinner.setAdapter(ca);

		cbSpot = (CheckBox) v.findViewById(R.id.cbSpotSample);
		cbFCD = (CheckBox) v.findViewById(R.id.cbFCDSample);
		etQuant = (TextView) v.findViewById(R.id.etQuantitySample);
		submitReqSample = (Button) v.findViewById(R.id.btnReqSample);
		submitReqSample.setOnClickListener(this);
		
		cbSpot.setOnCheckedChangeListener(this);
		cbFCD.setOnCheckedChangeListener(this);
		
		final float scale = this.getResources().getDisplayMetrics().density;
//		cbSpot.setPadding(cbSpot.getPaddingLeft() + (int)(25.0f * scale + 0.5f),
//				cbSpot.getPaddingTop(),
//				cbSpot.getPaddingRight(),
//				cbSpot.getPaddingBottom());
//		cbFCD.setPadding(cbFCD.getPaddingLeft() + (int)(25.0f * scale + 0.5f),
//				cbFCD.getPaddingTop(),
//				cbFCD.getPaddingRight(),
//				cbFCD.getPaddingBottom());
		
		return v;
	}

	private class RequestSample extends AsyncTask<Void, Void, String>
	{

		private static final String SOAP_ACTION = "http://tempuri.org/SetSampleRequest";

		private static final String METHOD_NAME = "SetSampleRequest";

		GoogleProgressDialog dialog;

		JSONObject obj;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialog = new GoogleProgressDialog(getActivity());
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params)
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("UserId", userID);
			request.addProperty("Productcode", getArguments().get("PRODUCT_CODE"));
			request.addProperty("ImprintMethod", cbFCD.isChecked() ? cbFCD.getText() : cbSpot.getText());
			request.addProperty("Colors", spinner.getSelectedItem().toString());
			request.addProperty("Quantity", etQuant.getText().toString().trim());
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;
			try
			{
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				Log.v("RESP", envelope.bodyIn.toString());
				SoapObject result = (SoapObject) envelope.bodyIn;
				if (result != null)
				{
					String data = result.getProperty(0).toString();
					Log.d("result", result.toString());
					Log.v("data", data);
					obj = new JSONObject(data);
				}
			}
			catch (UnknownHostException e)
			{
				e.printStackTrace();
				return "No Internet Connection";
			}
			catch (SocketTimeoutException e)
			{
				e.printStackTrace();
				return "Unable to reach server";
			}
			catch (JSONException e)
			{
				e.printStackTrace();
				return "Invalid response from server";
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return "Unexpected Error Occured";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			dialog.dismiss();
			try
			{
				if (result != null)
				{
					showToast(result);
					return;
				}
				if (obj.getJSONArray("Data").getJSONObject(0).getString("responseCode").equals("0"))
				{
					// success
					showToast("Sample Request posted successfully");
					etQuant.setText("");
				}
				else
				{
					showToast("Error Occured: Request not submitted");
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				showToast("Error Occured: Request not submitted");
			}

		}

	}
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
		switch (buttonView.getId())
		{
			case R.id.cbSpotSample:
				if (isChecked)
				{
					//cbFCD.setOnCheckedChangeListener(null);
					cbFCD.setChecked(false);
					//cbFCD.setOnCheckedChangeListener(this);
				}
				break;
			case R.id.cbFCDSample:
				if (isChecked)
				{
					//cbSpot.setOnCheckedChangeListener(null);
					cbSpot.setChecked(false);
					//cbSpot.setOnCheckedChangeListener(this);
				}
				break;
			default:
				break;
		}
	}

	private void showToast(String msg)
	{
		Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.btnReqSample:
				if (spinnerValues == null || spinnerValues.length == 0)
				{
					showToast("No color available for this product");
					return;
				}
				if (!cbFCD.isChecked() && !cbSpot.isChecked())
				{
					showToast("Please select Imprint Method");
					return;
				}
				if (etQuant.getText().toString().trim().isEmpty())
				{
					showToast("Please enter quantity");
					etQuant.requestFocus();
					return;
				}
				int quant = 0;
				try
				{
					quant = Integer.valueOf(etQuant.getText().toString().trim());
				}
				catch (NumberFormatException e)
				{
					quant = 0;
				}
				if (quant == 0)
				{
					showToast("Please enter valid quantity");
					return;
				}
				// all ok call service
				new RequestSample().execute();
				break;

		}
	}
}
